@extends('admin.layout.main')

@section('title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-lg-6 col-12">
        <div class="card">
            <span class="card-title pt-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">Activity User </span>
            </span>
            <div class="card-body pt-0" style="align-items: center">
                <canvas class="mt-2" id="chart-user" style="width:100%" height="125">
            </div>
        </div>
    </div>
    <!-- column -->
    <div class="col-lg-3 col-12">
        <div class="card">
            <span class="card-title pt-3" style="border-bottom: none;text-align: center"> 
                <span style="font-size: 18px;">Course Educational </span>
            </span>
            <div class="card-body pt-0" style="align-self: center">
                <canvas class="mt-2" id="chart-educational" width="300" height="250" style="display: inline-block;height: auto;width: inherit;"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-12" >
        <div id="el-total-users">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title m-0" style="border: none">Students</h4>
                    <h3 class="font-light">{{ $user['student_count'] }} <span class="font-16 text-success font-medium">+{{ number_format($user['student_percent'], 2, '.', '') }}%</span></h3>
                    <div class="m-t-30">
                        <div class="row text-center">
                            <div class="col-6 border-right">
                                <h4 class="m-b-0">{{ $user['new_student'] }}</h4>
                                <span class="font-14 text-muted">New this Week</span>
                            </div>
                            <div class="col-6">
                                <h4 class="m-b-0">{{ $user['active_student'] }}</h4>
                                <span class="font-14 text-muted">Active this week</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title m-0" style="border: none">Teachers</h4>
                    <h3 class="font-light">{{ $user['teacher_count'] }} <span class="font-16 text-danger font-medium">+{{ number_format($user['teacher_percent'], 2, '.', '') }}%</span></h3>
                    <div class="m-t-30">
                        <div class="row text-center">
                            <div class="col-6 border-right">
                                <h4 class="m-b-0">{{ $user['new_teacher'] }}</h4>
                                <span class="font-14 text-muted">New this Week</span>
                            </div>
                            <div class="col-6">
                                <h4 class="m-b-0">{{ $user['active_teacher'] }}</h4>
                                <span class="font-14 text-muted">Active this week</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-center pt-2">
                        <div class="card-icon" style="color:#00a000;">
                            <i class="mdi mdi-webhook"></i>
                            <span>1</span>
                        </div>
                        <h5 style="font-size:22px">867</h5>
                        <h4 style="color: #9e9e9e;">Amount of Students</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-center pt-2">
                        <div class="card-icon" style="color: rgb(255 118 103);">
                            <i class="mdi mdi-webhook"></i>
                            <span>7</span>
                        </div>
                        <h5 style="font-size:22px">867</h5>
                        <h4 style="color: #9e9e9e;">Amount of Teachers</h4>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>

<div class="row">
    <div class="col-12 col-md-9">
        <div class="card">
            <div class="card-body text-center p-0" style="width: 500px; height: 550px;min-width:100%;min-height:100%">
                {!! Mapper::render() !!}
                {{-- <div class="card-icon" style="color:#00a000;">
                    <i class="mdi mdi-webhook"></i>
                    <span>1</span>
                </div>
                <h5 style="font-size:22px">867</h5>
                <h4 style="color: #9e9e9e;">Products Today</h4> --}}
            </div>
        </div>
    </div>
    <div class="col-12 col-md-3">
        <div class="card">
            <div class="card-title pt-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">Statistik </span>
            </div>
            <div class="card-body p-1" style="align-items: center">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>Kunjungan hari ini</th>
                            <td>:</td>
                            <td>{{ $visit['today'] }}</td>
                        </tr>
                        <tr>
                            <th>Kunjungan minggu </th>
                            <td>:</td>
                            <td>{{ $visit['week'] }}</td>

                        </tr>
                        <tr>
                            <th>Kunjungan tahun ini</th>
                            <td>:</td>
                            <td>{{ $visit['year'] }}</td>
                        </tr>
                        <tr>
                            <th>Total kunjungan</th>
                            <td>:</td>
                            <td>{{ $visit['total'] }}</td>

                        </tr>
                        <tr>
                            <th>Pengunjung online</th>
                            <td>:</td>
                            <td>{{ $visit['online'] }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <!-- column -->
    <div class="col-12">
        <div class="card">
            <span class="card-title pt-3" style="border-bottom: none"> 
                <span style="font-size: 18px;">Recent Courses Teacher</span>
            </span>
            <div class="col-12 px-4">
                <div class="table-responsive" style="height: 600px">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="border-top-0" style="width: 5px">NO.</th>
                                <th class="border-top-0" style="width: 40px"></th>
                                <th class="border-top-0">TITLE</th>
                                <th class="border-top-0">TUTOR</th>
                                <th class="border-top-0"><center>TOPIC</center></th>
                                <th class="border-top-0"><center>EDUCATIONAL</center></th>
                                <th class="border-top-0"><center>STATUS</center></th>
                                <th class="border-top-0"><center>CREATED AT</center></th>
                                <th class="border-top-0" style="width: 70px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($recents as $recent)
                            <tr>
                                <td><span class="font-medium"><center>{{ $loop->iteration }}</center></span></td>
                                <td><img class="rounded-circle" src="{{ $recent['teacher']['someDetail']['photo'] }}" width="50" height="50"></td>
                                <td>{{ $recent['title'] }}</td>
                                <td>{{ $recent['teacher']['someDetail']['fullname'] }}</td>
                                <td><center>{{ $recent['topic']['topic'] }}</center></td>
                                <td><center>{{ $recent['educational']['stage'] }}</center></td>
                                <td><center>
                                    @if ($recent['is_active'] == '1')
                                        <label class="label label-rounded label-megna">Active</label>
                                    @else
                                        <label class="label label-rounded label-warning">Deactive</label>
                                    @endif
                                </center></td>
                                <td><center>{{ $recent['created_at'] }}</center></td>
                                <td><center><a href="{{ route('admin-course.detail', $recent['id']) }}" class="btn btn-sm btn-primary"><i class="mdi mdi-magnify"></i> Detail</a></center></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="9"><center>Recent Course Not Found</center></td>
                            </tr>
                            @endforelse
                            {{-- @forelse ($data['histories'] as $history)
                            <tr>
                                <td><span class="font-medium"><center>{{ $loop->iteration }}</center></span></td>
                                <td><img class="rounded-circle" src="{{ $history['user']['image'] }}" width="50" height="50"></td>
                                <td>{{ $history['user']['username'] }}</td>
                                <td>{{ $history['user']['email'] }}</td>
                                <td><center>
                                    @if ($history['status_payment'] == '1')
                                        <label class="label label-rounded label-megna">Selesai</label>
                                    @elseif($history['status_payment'] == '0')
                                        <label class="label label-rounded label-warning">Menunggu</label>
                                    @else
                                        <label class="label label-rounded label-purple">Dibatalkan</label>
                                    @endif
                                </center></td>
                                <td><center>
                                    @if (count($history['histories']) > 0)
                                        {{ end($history['histories'])['status']['name'] }}
                                    @elseif ($history['status_payment'] == '1')
                                        Pengiriman Tertunda
                                    @else 
                                        Menunggu Pembayaran
                                    @endif
                                </center></td>
                                <td><center>{{ $history['created_at'] }}</center></td>
                                <td><center><a href="{{ url('payment').'/'.$history['id'] }}" class="btn btn-sm btn-primary"><i class="mdi mdi-magnify"></i> Detail</a></center></td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="4"><center>Order Activity Not Found</center></td>
                            </tr>
                            @endforelse --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('/assets/admin/libs/chart/Chart.min.js') }}"></script>
<script src="{{ asset('/assets/admin/libs/chart/dist/Chart.min.js') }}"></script>
<script src="{{ asset('/assets/admin/libs/chart/dist/chartjs-plugin-datalabels@0.7.0') }}"></script>
{{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-sbUI3YC24VMuJYKOcJ6KAMwoKkH3O5k"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.js"></script> --}}

<script>
$(function() {
    var sum = function(a, b) { return a + b; };

    var ctxUser = $("#chart-user")[0].getContext("2d");
    var chartUser = new Chart(ctxUser, {
        type: 'line',
        data: {
            datasets: [{
                data: {!! json_encode($activity['value']) !!},
                backgroundColor: 'rgba(41, 182, 246,0.2)',
                borderColor: 'rgba(41, 182, 246, 1)',
                hoverBackgroundColor : 'white',
                borderWidth: 2,
                label : 'Jumlah Siswa Aktif'
            }],
            labels: {!! json_encode($activity['label']) !!}
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: true,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem) {
                            return tooltipItem.yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    var ctxLearning = $("#chart-educational")[0].getContext("2d");
    var chartLearning = new Chart(ctxLearning, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: {!! json_encode($chart['educational_value']) !!},
                backgroundColor: ['#20d413', '#f1c21a', '#1a98f1','#ea2a16', '#8a13d4'],
                borderWidth: 1
            }],
            labels: {!! json_encode($chart['educational_label']) !!},
        },
        options: {
            animation: {
                animateScale: true,
                animateRotate: true
            },
            responsive: false,
            maintainAspectRatio: true,
            cutoutPercentage: 1,
            plugins: {
                datalabels: {
                    color: 'black',
                    formatter: function(value, context) {
                        return Math.round(value / context.chart.data.datasets[0].data.reduce(sum) * 100) + '%';
                    }
                }
            },
            legend: {
                display: true, 
                position:'bottom',
                labels: {
                    padding : 12,
                    fontColor: 'black',
                    fontSize : 12,
                    boxWidth: 10,
                    boxHeight: 2,
                },
            }
        }
    });

    setTimeout( function() {
        let height = $('#el-total-users').height(); 
        // alert(height);
        $('#chart-user').css('height',height-80);
        $('#chart-educational').css('height',height-90);
    }, 100);
});

</script>
@endsection