<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="17x16" href="{{ asset('/assets/images/logo-browser.png') }}">
    <title>ADMIN MY BUDDY SCHOOL</title>
    <!-- Custom CSS -->
    <link href="{{ asset('/assets/admin/dist/css/style.css') }}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <script type="text/javascript" charset="utf8" src="{{ asset('/assets/admin/libs/jquery/jquery-3.4.1.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/admin/libs/datatable/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('/assets/admin/libs/datatable/datatables.min.js') }}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">
{{-- <body> --}}
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin6"  style="position: fixed;width:100%">
            <nav class="navbar top-navbar navbar-expand-md navbar-light" >
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-brand" style="justify-content: center;">
                        <a href="{{ url('/')}}" class="logo">
                            <!-- Logo icon -->
                            <b class="logo-icon">
                                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                <!-- Dark Logo icon -->
                                <img src="{{ asset('assets/img/logo-head.png') }}" alt="homepage" class="dark-logo" width="50"/>
                                <!-- Light Logo icon -->
                                <img src="{{ asset('assets/img/logo-head.png') }}" alt="homepage" class="light-logo" width="130" height="40">
                            </b>
                            <!--End Logo icon -->
                            <!-- Logo text -->
                            <!-- <span class="logo-text"> -->
                                <!-- dark Logo text -->
                                <!-- <label style="color: white;cursor: pointer"><h4><i><b>PRASAJA</b></i></h4></label> -->
                            <!-- </span> -->
                        </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6" style="background-color: white;box-shadow: #c3c3c3ad 0px 1px 9px 0px">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <div class="container">
                            <h4 style="padding-left:12px; padding-top:5px"><b>Website Management</b></h4>
                        </div>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{-- <i class="mdi mdi-account-circle" style="font-size:3em;vertical-align: middle;"> </i> --}}
                                <img class="rounded-circle" src="{{ asset('assets/img/harley.png') }}" alt="" srcset="" width="45px">
                                <span style="color:blue">{{ auth()->guard('admin')->user()->email }}</span>
                                <i class="mdi mdi-chevron-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"><i class="ti-power-off m-r-5 m-l-5"></i> Log Out</a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5" style="position: fixed">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                <ul id="sidebarnav">
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link " href="{{ route('admin-dashboard') }}" aria-expanded="false">
                                <i class="mdi mdi-chart-bar"></i>
                                <span class="hide-menu">Dashboard</span>
                            </a>
                        </li>
                        <li class="sidebar-item {{ request()->is('admin/course/*') ? 'selected' : '' }}">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link {{ request()->is('admin/course/*') ? 'active' : '' }}" href="{{ route('admin-course') }}" aria-expanded="false">
                                <i class="mdi mdi-food-variant"></i>
                                <span class="hide-menu">Course</span>
                            </a>
                        </li>
                        {{-- <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('admin-leaderboard') }}" aria-expanded="false">
                                <i class="mdi mdi-star"></i>
                                <span class="hide-menu">Leaderboard</span>
                            </a>
                        </li> --}}
                        <li class="sidebar-item {{ request()->is('admin/mitra/*') ? 'selected' : '' }}">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link {{ request()->is('admin/mitra/*') ? 'active' : '' }}" href="{{ route('admin-mitra') }}" aria-expanded="false">
                                <i class="mdi mdi-star"></i>
                                <span class="hide-menu">Data Mitra</span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" data-toggle="collapse" href="#sidenavManajemen" aria-expanded="false">
                                <i class="mdi mdi-face"></i>
                                <span class="hide-menu">Management Account</span>
                                <i class="mdi mdi-menu-down mdi-collapse"></i>
                            </a>
                            <div class="collapse {{ request()->is('admin/studen*') || request()->is('admin/teache*') ? 'show' : '' }}" id="sidenavManajemen">
                                <ul class="first-level">
                                    <li class="sidebar-item {{ request()->is('admin/studen*') ? 'selected' : '' }}">
                                        <a class="sidebar-link {{ request()->is('admin/studen*') ? 'active' : '' }}" href="{{ route('admin-student') }}">
                                            <i class="mdi mdi-arrow-right-bold"></i>
                                            <span class="sidebar-normal">Student </span>
                                        </a>
                                    </li>
                                    <li class="sidebar-item {{ request()->is('admin/teache*') ? 'selected' : '' }}">
                                        <a class="sidebar-link {{ request()->is('admin/teache*') ? 'active' : '' }}" href="{{ route('admin-teacher') }}">
                                            <i class="mdi mdi-arrow-right-bold"></i>
                                            <span class="sidebar-normal">Tutor/Teacher </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" data-toggle="collapse" href="#sidenavSetting" aria-expanded="false">
                                <i class="mdi mdi-settings"></i>
                                <span class="hide-menu">Settings</span>
                                <i class="mdi mdi-menu-down mdi-collapse"></i>
                            </a>
                            <div class="collapse {{ request()->is('setting*') ? 'show' : '' }}" id="sidenavSetting">
                                <ul class="first-level">
                                    <li class="sidebar-item {{ request()->is('setting/soc*') ? 'selected' : '' }}">
                                        <a class="sidebar-link {{ request()->is('setting/soc*') ? 'active' : '' }}" href="{{ route('admin-setting.social') }}">
                                            <i class="mdi mdi-arrow-right-bold"></i>
                                            <span class="sidebar-normal">Social Media </span>
                                        </a>
                                    </li>
                                    <li class="sidebar-item">
                                        <a class="sidebar-link" href="#">
                                            <i class="mdi mdi-arrow-right-bold"></i>
                                            <span class="sidebar-normal">Others </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <!-- <ul id="sidebarnav">
                        <?php
                            // $main_menu = $this->db->get('tb_menu')->result();
                            // foreach ($main_menu as $main) {
                            //     echo '<li class="sidebar-item">' . anchor($main->link, "<i class='".$main->icon."'></i></i><span class='hide-menu'>" . strtoupper($main->nama), "class='sidebar-link waves-effect waves-dark sidebar-link'  aria-expanded='fals'") . "</span></li>";
                            // };
                        ?>
                    </ul> -->
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper" style="margin-top: 60px">
        <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">@yield('title')</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="active">
                                    <?php
                                        $tgl_sekarang = Date('Y')."-" .Date('m') ."-" .Date('d');
                                        echo date('l, d F Y', strtotime($tgl_sekarang)).' -  <span id="clock"></span>'; 
                                    ?>
                                    </li>
                                    
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                @yield('content')
            </div>
            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Krakatio Studio. Designed and Developed by
                <a href="https://krakatio.com">Krakatio</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('/assets/admin/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('/assets/admin/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!--Wave Effects -->
    <script src="{{ asset('/assets/admin/dist/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('/assets/admin/dist/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('/assets/admin/dist/js/custom.min.js') }}"></script>
    <!-- Sweetalert JS -->
    {{-- <script src="{{ asset('/assets/libs/sweetalert/sweetalert2.all.min.js') }}"></script> --}}
    <!--This page JavaScript -->
    
    <script>
        function tampilkanwaktu(){
            var waktu = new Date();        
            var sh = waktu.getHours() + "";
            var sm = waktu.getMinutes() + "";
            var ss = waktu.getSeconds() + "";
            document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
        }

        $('.btn-reset').click(function (e) { 
            e.preventDefault();
            $('input').val('');
            $('textarea').val('');
            $('.help-block').html('');
        });
        
        function clearError() {
            $('input').removeClass('is-invalid');
            $('textarea').removeClass('is-invalid');
            $('.help-block').html('');
        }
        

        // function markAsRead(notification_id) {
        //     $.post('/NotifMarkAsRead', {'notif_id': notification_id}, function (data) {
        //         data.success ? alert('berhasil') : alert('gagal');
        //     }, 'json');

        //     return false;
        // };

    </script>
</body>

</html>