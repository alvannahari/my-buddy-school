@extends('admin.layout.main')

@section('title', 'List Student')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {{-- <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh siswa yang telah terdaftar dalam sistem. Data yang ditampilkan berisi info pribadi dan status siswa. </h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <a class="btn btn-primary m-b-10" style="float: right;color:#fff" onclick="test()"><i class="mdi mdi-plus"></i> Add New Student</a>
                    </div>
                </div> --}}
                <div class="table-responsive">
                    <table id="table-customer" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th style="text-align: center">PHOTO</th>
                                <th>FULLNAME</th>
                                <th>EMAIL</th>
                                <th>GRADE</th>
                                <th style="text-align: center">STATUS</th>
                                <th style="text-align: center">CREATED</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" id="modal-delete" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Are you sure to delete this student ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="{{ route('admin-student.destroy', 1) }}" id="form-delete" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" id="btn-delete" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-customer').DataTable( {
        "pageLength": 50,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    return '<img class="rounded-circle" src="'+row.detail.photo+'" width="50" height="50">';
                }
            },
            { "data": "detail.fullname"},
            { "data": "email"},
            { "data": "detail.grade"},
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.email_verified_at == null)
                        return '<label class="label label-rounded label-warning">Suspend</label>';
                    else 
                        return '<label class="label label-rounded label-primary">Active</label>';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    let url = "{{ route('admin-student.detail', ':id') }}";
                    return `
                        <a href="`+url.replace(':id', row.id)+`" class="btn btn-sm btn-success" ><i class="mdi mdi-magnify"></i> Detail</a>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteStudent(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                        `;
                },"bSortable": false
            }
        ],
        // "createdRow": function (row, data, index) {
            // $('td', row).eq(1).addClass('truncated');
            // $('td', row).eq(2).css('width', '50px');
            // $('td', row).eq(3).css('width', '40px');
            // $('td', row).eq(6).css('width', '80px');
            // $('td', row).eq(5).css('width', '150px');
        // },
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function deleteStudent(id) {
    let url = "{{ route('admin-student.destroy', ':id') }}";
    $('#form-delete').attr('action', url.replace(':id', id));
    $('#modal-delete').modal('show');
}

function test() {
    alert('features are still under repair')
}

</script>

@endsection