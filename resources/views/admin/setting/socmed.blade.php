@extends('admin.layout.main')

@section('title', 'Update Social Media')

@section('content')
<div class="card">
    <div class="card-body">
        <form id="form-socmed">
            @csrf
            <div class="form-group">
                <label>Youtube</label>
                <input type="text" class="form-control" name="youtube" value="{{ $setting['youtube'] }}">
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label>Facebook</label>
                <input type="text" class="form-control" name="facebook" value="{{ $setting['facebook'] }}">
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label>Twitter</label>
                <input type="text" class="form-control" name="twitter" value="{{ $setting['twitter'] }}">
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label>Instagram</label>
                <input type="text" class="form-control" name="instagram" value="{{ $setting['instagram'] }}">
                <span class="help-block"></span>
            </div>
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-success" style="float: right;width:120px" id="btn-submit-socmed"><i class="mdi mdi-plus"></i> Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$('#btn-submit-socmed').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ route('admin-setting.social.update') }}",
        data: $('#form-socmed').serialize(),
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('<i class="mdi mdi-plus"></i> Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('<i class="mdi mdi-plus"></i> Save');
            btn.prop('disabled', false);
        }
    });
});
</script>

@endsection