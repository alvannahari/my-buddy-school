@extends('admin.layout.main')

@section('title', 'List Course')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                {{-- <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh kursus yang telah terdaftar dalam sistem. Data yang ditampilkan berisi info tentang kursus yang dibuka oleh pengajar beserta status kursus. </h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <a class="btn btn-primary m-b-10" style="float: right;color:#fff" onclick="test()"><i class="mdi mdi-plus"></i> Add New Course</a>
                    </div>
                </div> --}}
                <div class="table-responsive">
                    <table id="table-courses" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>TITLE</th>
                                <th>TOPIC</th>
                                <th>EDUCATIONAL</th>
                                <th>TUTOR</th>
                                <th>STATUS</th>
                                <th>CREATED</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Are you sure to delete this course ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="{{ route('admin-course.destroy', 1) }}" id="form-delete" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" id="btn-delete" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-suspend">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to suspend this student ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-suspend">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-courses').DataTable( {
        "pageLength": 50,
        "processing": true,
        // "serverSide": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "title"},
            { "data": "topic.topic", "sClass": "text-center"},
            { "data": "educational.stage", "sClass": "text-center"},
            { "data": "teacher.some_detail.fullname"},
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.is_active == '1') 
                        return '<label class="label label-rounded label-primary">Active</label>';
                    else 
                        return '<label class="label label-rounded label-warning">Suspend</label>';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    let url = "{{ route('admin-course.detail', ':id') }}";
                    return `
                        <a href="`+url.replace(':id', row.id)+`" class="btn btn-sm btn-success" ><i class="mdi mdi-magnify"></i> Detail</a>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteCourse(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                        `;
                },"bSortable": false
            }
        ],
        "createdRow": function (row, data, index) {
            $('td', row).eq(7).css('width', '150px');
        }
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function deleteCourse(id) {
    let url = "{{ route('admin-course.destroy', ':id') }}";
    $('#form-delete').attr('action', url.replace(':id', id));
    $('#modal-delete').modal('show');
}

function test() {
    alert('features are still under repair')
}

</script>

@endsection