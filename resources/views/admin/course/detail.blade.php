@extends('admin.layout.main')

@section('title', 'Detail Course')

@section('content')
<div class="row">
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
                <div class="row">
                    <div class="col-4">
                        <span style="font-size: 18px;" >Information </span>
                    </div>
                    <div class="col-8">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-course" ><i class="mdi mdi-pen mr-2"></i> Update Course</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-delete-course"><i class="mdi mdi-delete mr-2"></i> Delete Course</a>
                        </div>
                        @if ($course['is_active'] == '0')
                            <button class="btn btn-sm btn-warning mr-3" data-toggle="modal" data-target="#modal-state-course" style="float: right;width:120px"><i class="mdi mdi-block-helper mr-2"></i> Suspend</button>
                        @else
                            <button class="btn btn-sm btn-primary mr-3" data-toggle="modal" data-target="#modal-state-course" style="float: right;width:120px"><i class="mdi mdi-check mr-2"></i> Active</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Title </label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['title'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Description</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['description'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Cover</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><img src="{{ $course['cover'] }}" alt="" style="width:100%;max-width: 500px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Zoom</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['zoom'] ?? '-' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Schedule Zoom</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['time_schedule'] ?? '-' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Topic</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['topic']['topic'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Educational</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['educational']['stage'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Teacher/Tutor</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><a href="{{ route('admin-teacher.detail', $course['teacher']['id']) }}"><i class="mdi mdi-account"></i>{{ $course['teacher']['someDetail']['fullname'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Created At</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['created_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Updated At</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $course['updated_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 15px !important;"> 
                <span style="font-size: 18px;" >Statistik </span>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label>Total Theories </label>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-10">{{ $course['theories_count'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label>Total Discussions</label>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-10">{{ $course['discussions_count'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label>Total Quiz</label>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-10"><img src="{{ $course['quizzes_count'] }}" alt=""></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-5">
                            <label>Total Students</label>
                        </div>
                        <div class="col-7">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-10">{{ $course['students_count'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- modal state course --}}
<div class="modal {{ $course['is_active'] == '0' ? 'modal-primary' : 'modal-warning' }} fade" tabindex="-1" role="dialog" id="modal-state-course">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($course['is_active'] == '0')
                <p>Are you sure to activate this course ? </p>
                @else
                <p>Are you sure to suspend this course ? </p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn {{ $course['is_active'] == '0' ? 'btn-primary' : 'btn-warning' }}" id="btn-state-course">Ok</button>
            </div>
        </div>
    </div>
</div>

{{-- modal update course --}}
<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="modal-update-course">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Info Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-update-course">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" name="title" value="{{ $course['title'] }}">
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" rows="6" class="form-control">{{ $course['description'] }}</textarea>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label>Cover</label>
                        <small style="float: right;margin-top:10px">*silahkan kosongi jika tidak ada perubahan</small>
                        <input type="file" class="form-control" name="cover">
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Zoom</label>
                                <input type="text" class="form-control" name="zoom" value="{{ $course['zoom'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Schedule Zoom</label>
                                <input type="datetime-local" class="form-control" name="schedule" value="{{ $course['schedule'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Topic</label>
                                <select name="topic_id" class="form-control">
                                    @foreach ($topics as $topic)
                                        <option value="{{ $topic['id'] }}" {{ $topic['id'] == $course['topic_id'] ? 'selected' : '' }}>{{ $topic['topic'] }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Educational</label>
                                <select name="educational_id" class="form-control">
                                    @foreach ($educationals as $educational)
                                        <option value="{{ $educational['id'] }}" {{ $educational['id'] == $course['educational_id'] ? 'selected' : '' }}>{{ $educational['stage'] }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-update-course">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- modal delete course --}}
<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete-course">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this course ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('admin-course.destroy', $course['id']) }}" id="form-delete" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" id="btn-delete" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

$('#btn-state-course').click(function (e) { 
    e.preventDefault();
    var btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ route('admin-course.state', $course['id']) }}",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            location.reload();
        }, 
        error: function (response) {
            alert(response.error);
            btn.html('Ok');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-update-course').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-update-course')[0]);

    $.ajax({
        type: "post",
        url: "{{ route('admin-course.update', $course['id']) }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

</script>
@endsection