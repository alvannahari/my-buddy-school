@extends('admin.layout.main')

@section('title', 'Detail Teacher')

@section('content')
<div class="row">
    <div class="col-12 col-md-4">
        <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
                <div class="row">
                    <div class="col-6">
                        <span style="font-size: 18px;" >Photo </span>
                    </div>
                    <div class="col-6">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-image" ><i class="mdi mdi-pen mr-2"></i> Update Photo</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-remove-image"><i class="mdi mdi-delete mr-2"></i> Remove Photo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body pt-0" style="text-align: center">
                <img class="rounded-circle m-3" src="{{ $teacher['detail']['photo'] }}" alt="" srcset="" width="250" height="250">
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8">
        <div class="card">
            <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
                <div class="row">
                    <div class="col-4">
                        <span style="font-size: 18px;" >Info </span>
                    </div>
                    <div class="col-8">
                        <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-teacher" ><i class="mdi mdi-pen mr-2"></i> Update User</a>
                            <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-delete-teacher"><i class="mdi mdi-delete mr-2"></i> Delete User</a>
                        </div>
                        @if ($teacher['email_verified_at'] == null)
                            <button class="btn btn-sm btn-warning mr-3" data-toggle="modal" data-target="#modal-state-teacher" style="float: right;width:120px"><i class="mdi mdi-block-helper mr-2"></i> Suspend</button>
                        @else
                            <button class="btn btn-sm btn-primary mr-3" data-toggle="modal" data-target="#modal-state-teacher" style="float: right;width:120px"><i class="mdi mdi-check mr-2"></i> Active</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Fullname</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['fullname'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Email</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['email'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Gender</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['gender'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Date of birth</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['place_of_birth'].', '.$teacher['detail']['date_of_birth'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Grade</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['degree'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Address</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['address'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Country</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['country'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Province</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['province'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>City</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['city'] }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Created At</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['created_at']}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-3 col-md-3">
                                    <label>Updated At</label>
                                </div>
                                <div class="col-9 col-md-9">
                                    <div class="row">
                                        <div class="col-1" style="text-align: center"> : </div>
                                        <div class="col-11">{{ $teacher['detail']['updated_at']}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-title py-3"> 
        <div class="row">
            <div class="col-12">
                <span class="ml-2" style="font-size: 18px;" >Course Owned</span>
            </div>
        </div>
    </div>
    <div class="card-body pt-0">
        <table class=" table table-hover table-sm" width="100%" >
            <thead>
                <tr>
                    <th style="border-top: none;"><center>NO.</center></th>
                    <th style="border-top: none;">TITLE</th>
                    <th style="border-top: none;"><center>TOPIC</center></th>
                    <th style="border-top: none;"><center>EDUCATIONAL</center></th>
                    <th style="border-top: none;"><center>STATUS</center></th>
                    <th style="border-top: none;"><center>TOTAL STUDENTS</center></th>
                    <th style="border-top: none;"><center>DATE</center></th>
                    <th style="border-top: none;"></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($teacher['courses'] as $course)
                    <tr>
                        <td><center>{{ $loop->iteration }}</center></td>
                        <td>{{ $course['title'] }}</td>
                        <td><center>{{ $course['topic']['topic'] }}</center></td>
                        <td><center>{{ $course['educational']['stage'] }}</center></td>
                        <td><center>
                            @if ($course['is_active'] == '1')
                            <label class="label label-rounded label-megna">Active</label>
                            @else
                            <label class="label label-rounded label-warning">Suspend</label>
                            @endif
                        </center></td>
                        <td><center>{{ $course['students_count'] }}</center></td>
                        <td><center>{{ $course['created_at'] }}</center></td>
                        <td>
                            <a href="{{ route('admin-course.detail', $course['id']) }}" class="btn btn-sm btn-primary m-l-5"><i class="mdi mdi-magnify"></i> Detail</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="7"><center>the teacher has never made an this courses</center></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>

{{-- modal update photo --}}
<div class="modal modal-primary fade" id="modal-update-image" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Photo Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form id="form-update-photo">
                    @csrf
                    <div class="form-group">
                        <input type="file" class="form-control" name="photo" >
                        <span class="help-block"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-update-photo">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- modal remove photo --}}
<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-remove-image">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this photo profile ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-remove-image">Ok</button>
            </div>
        </div>
    </div>
</div>

{{-- modal state teacher --}}
<div class="modal {{ $teacher['email_verified_at'] == null ? 'modal-primary' : 'modal-warning' }} fade" tabindex="-1" role="dialog" id="modal-state-teacher">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if ($teacher['email_verified_at'] == null)
                <p>Are you sure to activate this user ? </p>
                @else
                <p>Are you sure to suspend this user ? </p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn {{ $teacher['email_verified_at'] == null ? 'btn-primary' : 'btn-warning' }}" id="btn-state-teacher">Ok</button>
            </div>
        </div>
    </div>
</div>

{{-- modal update teacher --}}
<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="modal-update-teacher">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Info User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-update-teacher">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Fullname</label>
                                <input type="text" class="form-control" name="fullname" value="{{ $teacher['detail']['fullname'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" disabled value="{{ $teacher['email'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Gender</label>
                                <div class="row mb-2">
                                    <div class="col-6">
                                        <input type="radio" name="gender" id="gender-man" value="Laki-laki" checked style="margin-top: 3px">
                                        <label for="gender-man" style="position: absolute;margin-left:10px">Laki-laki</label>
                                    </div>
                                    <div class="col-6">
                                        <input type="radio" name="gender" id="gender-woman" value="Perempuan" style="margin-top: 3px">
                                        <label for="gender-woman" style="position: absolute;margin-left:10px">Perempuan</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Place Of Birth</label>
                                <input type="text" class="form-control" name="place_of_birth" value="{{ $teacher['detail']['place_of_birth'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="date" class="form-control" name="date_of_birth" value="{{ $teacher['detail']['date_of_birth'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Degree</label>
                                <input type="text" class="form-control" name="degree" value="{{ $teacher['detail']['degree'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address" value="{{ $teacher['detail']['address'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <input type="text" class="form-control" name="country" value="{{ $teacher['detail']['country'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>Province</label>
                                <input type="text" class="form-control" name="province" value="{{ $teacher['detail']['province'] }}">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" name="city" value="{{ $teacher['detail']['city'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-update-teacher">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- modal delete teacher --}}
<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete-teacher">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this teacher ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('admin-teacher.destroy', $teacher['id']) }}" id="form-delete" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" id="btn-delete" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#btn-update-photo').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-update-photo')[0]);

    $.ajax({
        type: "post",
        url: "{{ route('admin-teacher.update-photo', $teacher['id']) }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend: function() {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-remove-image').click(function() {
    let btn = $(this);
    $.ajax({
        type: "delete",
        url: "{{ route('admin-teacher.delete-photo', $teacher['id']) }}",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        dataType: "json",
        beforeSend:function () {
            btn.html('Removing...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                alert(response.error);
                btn.html('Ok');
                btn.prop('disabled', false);
            }
        }
    });
});

$('#btn-state-teacher').click(function (e) { 
    e.preventDefault();
    var btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ route('admin-teacher.state', $teacher['id']) }}",
        data: {
            "_token": "{{ csrf_token() }}",
        },
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            location.reload();
        }, 
        error: function (response) {
            alert(response.error);
            btn.html('Ok');
            btn.prop('disabled', false);
        }
    });
});

$('#btn-update-teacher').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);

    $.ajax({
        type: "post",
        url: "{{ route('admin-teacher.update', $teacher['id']) }}",
        data: $('#form-update-teacher').serialize(),
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            console.log(response);
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});
</script>
@endsection