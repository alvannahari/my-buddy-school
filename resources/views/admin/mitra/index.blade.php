@extends('admin.layout.main')

@section('title', 'List Mitra')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-10">
                        <!-- <h4 class="card-title">Modul</h4> -->
                        <h6 class="card-subtitle m-t-5 m-b-20">Halaman yang berisi daftar seluruh mitra atau partner yang telah bekerja sama dengan my buddy school. Data yang ditampilkan berisi info tentang mitra serta beberapa informasi media sosial dan videotron. </h6>
                    </div>
                    <div class="col-12 col-md-2">
                        <a href="{{ route('admin-mitra.create') }}" class="btn btn-primary m-b-10" style="float: right;color:#fff"><i class="mdi mdi-plus"></i> Add New Mitra</a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="table-mitras" class="table table-hover table-sm" cellspacing="0" width="100%">
                        <thead>
                            <tr style="line-height: 2.2">
                                <th>NO.</th>
                                <th>LOGO</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>CITY</th>
                                <th>EDUCATIONAL</th>
                                <th>CREATED</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Are you sure to delete this mitra ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form action="{{ route('admin-mitra.destroy', 1) }}" id="form-delete" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" id="btn-delete" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-suspend">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to suspend this student ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="btn-suspend">Ok</button>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function () {
    // $.fn.dataTable.ext.classes.sPageButton = 'button primary_button';
    let t = $('#table-mitras').DataTable( {
        "pageLength": 50,
        "processing": true,
        "ajax": "{!! url()->current() !!}",
        "columns": [
            {
                "data": null,
                "width": "10px",
                "sClass": "text-center",
                "bSortable": false
            },
            { "data": "null","bSortable": false, "sClass": "text-center", render: function ( data, type, row ) {
                    return '<img src="'+row.image+'" width="50">'
                }
            },
            { "data": "name"},
            { "data": "email"},
            { "data": "city"},
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    if (row.educational_id != '0') 
                        return row.educational.stage;
                    else 
                        return 'Company';
                }
            },
            { "data": "created_at", "sClass": "text-center" },
            { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                    let url = "{{ route('admin-mitra.detail', ':id') }}";
                    return `
                        <a href="`+url.replace(':id', row.id)+`" class="btn btn-sm btn-success" ><i class="mdi mdi-magnify"></i> Detail</a>
                        <button class="btn btn-sm btn-danger m-l-5" title="delete" onclick="deleteMitra(`+row.id+`)"><i class="mdi mdi-delete"></i> Delete</button>
                        `;
                },"bSortable": false
            }
        ]
    });
    t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
});

function deleteMitra(id) {
    let url = "{{ route('admin-mitra.destroy', ':id') }}";
    $('#form-delete').attr('action', url.replace(':id', id));
    $('#modal-delete').modal('show');
}

</script>

@endsection