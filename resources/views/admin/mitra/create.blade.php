@extends('admin.layout.main')

@section('title', 'Create New Mitra')

@section('content')
<div class="card">
    <div class="card-body">
        <form id="form-create-mitra">
            @csrf
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" name="name">
                <span class="help-block"></span>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" placeholder="ex. mybuddyschool@gmail.com">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Educational</label>
                        <select name="educational_id" class="form-control">
                            <option value="0" >Company</option>
                            @foreach ($educationals as $educational)
                                <option value="{{ $educational['id'] }}">{{ $educational['stage'] }}</option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="image">
                <span class="help-block"></span>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" class="form-control" name="address" placeholder="ex. Jl. Mertojoyo Selatan Merjosari">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control" name="city" placeholder="ex. Kota Malang">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" class="form-control" name="phone">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Video</label>
                        <input type="text" class="form-control" name="video" laceholder="ex. https://youtube.com/tr5ie93pws5">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Website</label>
                        <input type="text" class="form-control" name="website" placeholder="ex. https://labschool-um.sch.id/">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Facebook</label>
                        <input type="text" class="form-control" name="facebook"  placeholder="ex. https://facebook.com/krakatio">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Twitter</label>
                        <input type="text" class="form-control" name="twitter"  placeholder="ex. https://twitter.com/krakatio">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Youtube</label>
                        <input type="text" class="form-control" name="youtube" placeholder="ex. https://youtube.com/krakatio">
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button class="btn btn-success" style="float: right;width:120px" id="btn-create-mitra"><i class="mdi mdi-plus"></i> Save</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$('#btn-create-mitra').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-create-mitra')[0]);

    $.ajax({
        type: "post",
        url: "{{ route('admin-mitra.store') }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('<i class="mdi mdi-plus"></i> Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('<i class="mdi mdi-plus"></i> Save');
            btn.prop('disabled', false);
        }
    });
});
</script>

@endsection