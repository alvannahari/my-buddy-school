@extends('admin.layout.main')

@section('title', 'Detail Mitra')

@section('content')
<div class="card">
    <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;"> 
        <div class="row">
            <div class="col-4">
                <span style="font-size: 18px;" >Information </span>
            </div>
            <div class="col-8">
                <a href="#" type="button" style="float: right;font-size:26px;margin-top: -5px;" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" > <i class="mdi mdi-chevron-down"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-update-mitra" ><i class="mdi mdi-pen mr-2"></i> Update Mitra</a>
                    <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-delete-mitra"><i class="mdi mdi-delete mr-2"></i> Delete Mitra</a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Name </label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['name'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Email</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['email'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Image</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><img src="{{ $mitra['image'] }}" alt="" style="width:100%;max-width: 200px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Address</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['address'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>City</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['city'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Educational</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['educational']['stage'] ?? 'Company' }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Video</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><a href="{{ $mitra['video'] }}">{{ $mitra['video'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Phone Number</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['phone'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Website</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><a href="{{ $mitra['website'] }}">{{ $mitra['website'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Facebook</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><a href="{{ $mitra['facebook'] }}">{{ $mitra['facebook'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Twitter</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><a href="{{ $mitra['twitter'] }}">{{ $mitra['twitter'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Youtube</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11"><a href="{{ $mitra['youtube'] }}">{{ $mitra['youtube'] }}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Created At</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['created_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-3">
                            <label>Updated At</label>
                        </div>
                        <div class="col-9">
                            <div class="row">
                                <div class="col-1" style="text-align: center"> : </div>
                                <div class="col-11">{{ $mitra['updated_at'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-title" style="padding-top: 1rem !important;padding-bottom: 7px !important;">
        <span style="font-size: 18px;" >Video Mitra </span>
    </div>
    <div class="card-body" style="max-width:800px;max-height:600px;width:100%;align-self: center;">
        <div style="position: relative;width: 100%;height: 0;padding-bottom: 56.25%;">
            <iframe style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;" width="560" height="315" src="{{ $mitra['video'] }}" frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>

{{-- modal update mitra --}}
<div class="modal modal-primary fade" tabindex="-1" role="dialog" id="modal-update-mitra">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Info Mitra</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-update-mitra">
                    @csrf
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $mitra['name'] }}">
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" value="{{ $mitra['email'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Educational</label>
                                <select name="educational_id" class="form-control">
                                    <option value="0" {{ $mitra['educational_id'] == '0' ? 'selected' : '' }}>Company</option>
                                    @foreach ($educationals as $educational)
                                        <option value="{{ $educational['id'] }}" {{ $mitra['educational_id'] == $educational['id'] ? 'selected' : '' }}>{{ $educational['stage'] }}</option>
                                    @endforeach
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <small style="float: right;margin-top:10px">*silahkan dikosongi jika tidak ingin merubah gambar</small>
                        <input type="file" class="form-control" name="image" value="{{ $mitra['image'] }}">
                        <span class="help-block"></span>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address" value="{{ $mitra['address'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" name="city" value="{{ $mitra['city'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Phone Number</label>
                                <input type="text" class="form-control" name="phone" value="{{ $mitra['phone'] }}">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Video</label>
                                <input type="text" class="form-control" name="video" value="{{ $mitra['video'] }}" placeholder="ex. https://youtube.com/tr5ie93pws5">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" class="form-control" name="website" value="{{ $mitra['website'] }}" placeholder="ex. https://labschool-um.sch.id/">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Facebook</label>
                                <input type="text" class="form-control" name="facebook" value="{{ $mitra['facebook'] }}" placeholder="ex. https://facebook.com/krakatio">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Twitter</label>
                                <input type="text" class="form-control" name="twitter" value="{{ $mitra['twitter'] }}" placeholder="ex. https://twitter.com/krakatio">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Youtube</label>
                                <input type="text" class="form-control" name="youtube" value="{{ $mitra['youtube'] }}" placeholder="ex. https://youtube.com/krakatio">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn-update-mitra">Save</button>
            </div>
        </div>
    </div>
</div>

{{-- modal delete mitra --}}
<div class="modal modal-danger fade" tabindex="-1" role="dialog" id="modal-delete-mitra">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this mitra ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="{{ route('admin-mitra.destroy', $mitra['id']) }}" id="form-delete" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger" id="btn-delete" type="submit">Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>

$('#btn-update-mitra').click(function (e) { 
    e.preventDefault();
    clearError();
    let btn = $(this);
    let formData = new FormData($('#form-update-mitra')[0]);

    $.ajax({
        type: "post",
        url: "{{ route('admin-mitra.update', $mitra['id']) }}",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        beforeSend:function () {
            btn.html('Saving...');
            btn.prop('disabled', true);
        },
        success: function (response) {
            if (!response.error) {
                location.reload();
            } else {
                for (let key of Object.keys(response.error)) {
                    $('[name="'+key+'"]').addClass('is-invalid');
                    $('[name="'+key+'"]').next().html(response.error[key]);
                }
                alert('Something wrong !!!');
                btn.html('Save');
                btn.prop('disabled', false);
            }
        }, 
        error: function (){
            alert('Something wrong !!!');
            btn.html('Save');
            btn.prop('disabled', false);
        }
    });
});

</script>
@endsection