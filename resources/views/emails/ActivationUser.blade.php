<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;400;600;800&display=swap" rel="stylesheet">
    <style>
        * {
            padding: 0;
            margin: 0;
        }
        p {
            font-family: 'Nunito Sans', sans-serif;
        }
        header {
            height: 90px;
            background: #5FC2ED;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #FFFFFFFF;
            font-family: 'Nunito Sans', sans-serif;
            font-weight: bold;
            width: 100%;
            text-align: center;
        }
        .container {
            width: 100%;
            padding: 0px 35px;
            box-sizing: border-box;
            text-align: center;
        }
        .logo {
            margin: 25px 0;
        }
        .login-now {
            background: linear-gradient(180deg, #00A2E9 0%, #5FC2ED 100%);
            box-shadow: 0px 4px 40px rgba(0, 162, 233, 0.2);
            border-radius: 50px;
            color: #FFFFFFFF;
            text-decoration: none;
            padding: 10px 55px;
            font-family: 'Nunito Sans', sans-serif;
            font-weight: bold;
        }
        .content {
            text-align: left;
        }
        .salam,
        .info {
            margin-bottom: 20px;
        }
        .data-diri {
            margin-bottom: 50px;
        }
        .data-diri p {
            margin-bottom: 10px;
        }
        .thanks {
            margin-top: 40px;
        }
        .bold {
            font-weight: bold;
        }
        footer {
            background: #F4F4F4;
            text-align: center;
            color: #5A5A5A;
            padding: 15px;
            margin-top: 90px;
        }
    </style>
</head>
<body>
    <header>
        <div style="text-align: center;width:100%">
            <h1 style="margin-top: 20px;">Welcome to My Buddy School</h1>
        </div>
    </header>
    <div class="container">
        {{-- <img src="{{ asset('assets/img/logo-head.png') }}" alt="logo" class="m-auto logo"> --}}
        <img src="https://rumah-rehat.krakatio.com/public/assets/images/logo-head.png" alt="logo" class="m-auto logo">
        <div class="content">
            <p class="salam">Hi {{ $user['fullname'] }},</p>
            {{-- <p class="salam">Hi safasfsa,</p> --}}
            <p class="info">You have registered in <span class="bold">My Buddy School</span> as a <span class="bold">{{ $user['type'] }}</span>. Please keep your log in information below:</p>
            <div class="data-diri">
                <p>Username	: {{ $user['email'] }}</p>
                <p>Password	: {{ $user['password'] }}</p>
            </div>
            <a href="{{ $user['link'] }}" class="login-now" style="color: white">Log In Now</a>
            {{-- <a href="#" class="login-now" style="color: white">Log In Now</a> --}}
            <div class="thanks">
                <p>Thanks for choosing My Buddy School - The My Buddy School Team</p>
            </div>
        </div>
    </div>
    <footer>
        <p>© 2020 My Buddy School.</p>
    </footer>
</body>
</html>