@extends('template.layout')

@section('content')
<section class="cover-all profiles">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>Messages</h4>
                    <p>You have {{ session()->get('inbox_unread') }} unread message</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- <div class="image"><img src="../../assets/img/MaskGroups.png" alt=""></div> -->
        </div>
    </div>
</section>

<section class="message-breadcrumb selecteds">
    <div class="breads-cont">
        <div class="row">
            <div class="col-md-3">
                <div class="create d-none d-sm-block">
                    <a type="button" style="width: 100%;" data-toggle="modal" data-target="#exampleModalMsgEmpty">
                        @if (auth()->guard('student')->check())
                        <p>Create new message <span><img src="{{ asset('assets/img/mdi_message.png') }}" alt=""></span> </p>
                        @endif
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="hamburgs">
                    <div class="row">
                        <div class="col-md-11">
                            <div class="lefts">
                                <i class="far fa-long-arrow-left d-sm-none"></i>
                                <img class="rounded-circle" src="{{ $sender_user['some_detail']['photo'] }}" alt="" width="28" height="28">
                                <h6>{{ $sender_user['some_detail']['fullname'] }}</h6>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="rights">
                                <img class="d-sm-none" src="{{ asset('assets/img/Frames7.png') }}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-message selected">
        <div class="row no-gutters">
            <div class="col-md-3">
                <div class="profile-list d-none d-sm-block">
                    <ul>
                        @if (auth()->guard('student')->check())
                            @forelse ($inboxes as $inbox)
                            <li class="{{ $sender_user['id'] == $inbox['teacher']['id'] ? 'active' : '' }}">
                                <a href="{{ url('inbox').'/'.$inbox['teacher']['id'] }}">
                                    <div class="row no-gutters">
                                        <div class="col-md-3 col-3" style="text-align: right;padding-right: 22px;">
                                            <img class="rounded-circle" src="{{ $inbox['teacher']['some_detail']['photo'] }}" alt="" width="40" height="40">
                                            @if ($sender_user['id'] != $inbox['teacher']['id'])
                                                @if ($inbox['read_s'] == 0)
                                                <span class="badge badge-pill badge-warning text-white badge-mail" >!</span>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-md-9 col-9">
                                            <h6>{{ $inbox['teacher']['some_detail']['fullname'] }}</h6>
                                            <p>{{ $inbox['created_at'] }}</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @empty
                            <li>
                                <a href="#">
                                    <div class="row no-gutters">
                                        <div class="col-md-3 col-3" style="text-align: right;
                                        padding-right: 22px;">
                                            <img src="{{ asset('assets/img/Ellipse83.png') }}" alt="">
                                            {{-- <span class="badge badge-pill badge-warning text-white badge-mail" >!</span> --}}
                                        </div>
                                        <div class="col-md-9 col-9">
                                            <h6>Alvan Nahari</h6>
                                            <p>Lorem Ipsum</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @endforelse
                        @else
                        @forelse ($inboxes as $inbox)
                            <li class="{{ $sender_user['id'] == $inbox['student']['id'] ? 'active' : '' }}">
                                <a href="{{ url('inbox').'/'.$inbox['student']['id'] }}">
                                    <div class="row no-gutters">
                                        <div class="col-md-3 col-3" style="text-align: right;padding-right: 22px;">
                                            <img class="rounded-circle" src="{{ $inbox['student']['some_detail']['photo'] }}" alt="" width="40" height="40">
                                            @if ($sender_user['id'] != $inbox['student']['id'])
                                                @if ($inbox['read_t'] == 0)
                                                <span class="badge badge-pill badge-warning text-white badge-mail" >!</span>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-md-9 col-9">
                                            <h6>{{ $inbox['student']['some_detail']['fullname'] }}</h6>
                                            <p>{{ $inbox['created_at'] }}</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @empty
                            <li>
                                <a href="#">
                                    <div class="row no-gutters">
                                        <div class="col-md-3 col-3" style="text-align: right;
                                        padding-right: 22px;">
                                            <img src="{{ asset('assets/img/Ellipse83.png') }}" alt="">
                                            {{-- <span class="badge badge-pill badge-warning text-white badge-mail" >!</span> --}}
                                        </div>
                                        <div class="col-md-9 col-9">
                                            <h6>Alvan Nahari</h6>
                                            <p>Lorem Ipsum</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @endforelse
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="message-inbox">
                    <div class="chat">
                        @foreach ($messages as $keyMessage => $valMessage)
                            <div class="conversation-start" style="width: 100%;display: flex;">
                                <span>{{ $keyMessage }}</span>
                            </div>
                            @foreach ($valMessage as $message)
                                @if (auth()->guard('student')->check())
                                    @if ($message['sender_s'] == '1')
                                    <div class="bubble me">
                                        {{ $message['message'] }}
                                        <span>{{ substr($message['created_at'], 18, 8) }}</span>
                                    </div>
                                    @else 
                                    <div class="bubble you">
                                        {{ $message['message'] }}
                                        <span>{{ substr($message['created_at'], 18, 8) }}</span>
                                    </div>
                                    @endif
                                @else
                                    @if ($message['sender_s'] == '0')
                                    <div class="bubble me">
                                        {{ $message['message'] }}
                                        <span>{{ substr($message['created_at'], 18, 8) }}</span>
                                    </div>
                                    @else 
                                    <div class="bubble you">
                                        {{ $message['message'] }}
                                        <span>{{ substr($message['created_at'], 18, 8) }}</span>
                                    </div>
                                    @endif
                                @endif
                            @endforeach
                        @endforeach
                    </div>
                </div>
                <div class="write">
                    <form id="form-reply-message">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $sender_user['id'] }}">
                        <textarea placeholder="Type here...." rows="5" name="message"></textarea>
                        <img src="{{ asset('assets/img/direct1.png') }}" alt="" id="btn-reply-message">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@if (auth()->guard('student')->check())
<div class="modal fade" id="exampleModalMsgEmpty" tabindex="-1" role="dialog"aria-labelledby="exampleModalMsgEmptyTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-new-message">
                    @csrf
                    <div class="row no-gutters mb-3">
                        <div class="col-md-3 col-3">
                            <label for="people">Send To:</label>
                        </div>
                        <div class="col-md-9 col-9">
                            <select id="people" name="user_id">
                                @forelse ($tutors as $tutor)
                                <option value="{{ $tutor['id'] }}" class="option" style="margin-top: 20px;
                                padding-right: 10px;">
                                    {{-- <img src="{{ {{ $tutor['some_detail'] }} }}" alt=""> --}}
                                    <h6>{{ $tutor['some_detail']['fullname'] }}</h6>
                                </option>
                                @empty
                                <option value="volvo" class="option" style="margin-top: 20px;
                                padding-right: 10px;">
                                    {{-- <img src="{{ asset('assets/img/Ellipse833.png') }}" alt=""> --}}
                                    <h6>No Tutor Available</h6>
                                </option>
                                @endforelse
                            </select>
        
                        </div>
                    </div>
                    <div class="inns">
                        <textarea placeholder="Type here...." rows="5" name="message"></textarea>
                    </div>
                    <button id="btn-send-new-message">KIRIM</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

<script>
    $('#btn-send-new-message').click(function (e) { 
        e.preventDefault();
        var button = $(this);
        
        $.ajax({
            type: "post",
            url: "{{ url('inbox') }}",
            data: $('#form-new-message').serialize(),
            beforeSend: function() {
                button.html('Mengirim...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert('Terjadi Kesalahan !!');
                    button.html('Kirim');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }   
            }
        });
    });
    $('#btn-reply-message').click(function (e) { 
        e.preventDefault();
        $.ajax({
            type: "post",
            url: "{{ url('inbox') }}",
            data: $('#form-reply-message').serialize(),
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert('Terjadi Kesalahan !!');
                }   
            }
        });
    });
</script>

@endsection