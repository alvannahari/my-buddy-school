@extends('template/layout')

@section('content')
<section class="cover-all dashboards">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>Indonesian Buddy School system</h4>
                    <p>BSS adalah suatu sistem jejaring yang dikembangkan dengan pola kemitraan (partnership) antar
                        sekolah, antar pendidik, dan antar siswa, dan bahkan antar orang tua peserta didik yang
                        ingin belajar untuk memajukan pendidikan anaknya.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div>
        </div>
    </div>
</section>

<section class="dashboard-sec-1">
    <div class="prelative container">
        <div class="charts-box">
            <div class="juduls">
                <h4>All Course</h4>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="chart">
                        <canvas id="oilChart"></canvas>
                        <div class="absolute-center text-center">
                            <b><p>{{ $count_course['total'] }}</p></b>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="texts">
                                    <h5>Primary School (SD)</h5>
                                    <h6>{{ $count_course['1'] ?? 0 }}</h6>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="texts">
                                    <h5>Junior High School (SMP)</h5>
                                    <h6>{{ $count_course['2'] ?? 0 }}</h6>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="texts">
                                    <h5 style="color: orange;">Senior High School (SMA)</h5>
                                    <h6>{{ $count_course['3'] ?? 0 }}</h6>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="texts">
                                    <h5 style="color: plum;">Higher School</h5>
                                    <h6>{{ $count_course['4'] ?? 0 }}</h6>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="texts">
                                    <h5 style="color: rebeccapurple;">Others</h5>
                                    <h6>{{ $count_course['5'] ?? 0 }}</h6>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="texts">
                                    <h5 style="color: black;">TOTAL</h5>
                                    <h6>{{ $count_course['total'] }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="footerss">
                        <div class="row">
                            <div class="col-md-4 col-4">
                                <div class="text2">
                                    <h6>On Going</h6>
                                    <p>6 Course</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-4">
                                <div class="text2">
                                    <h6>On Going</h6>
                                    <p>6 Course</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-4">
                                <div class="text2">
                                    <h6>On Going</h6>
                                    <p>6 Course</p>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dashboard-sec-2">
    <div class="prelative container">
        <div class="courses">
            <div class="row">
                <div class="col-md-6 col-6">
                    <h5>My Courses</h5>
                </div>
                <div class="col-md-6 col-6">
                    <a href="{{ url('myCourse') }}">
                        <h6>See More</h6>
                    </a>
                </div>
            </div>
            <div class="row no-gutters pb-5">
                @forelse ($courses as $course)
                <div class="col-md-3 col-6">
                    <div class="box-content mb-3">
                        <img src="{{ $course['cover'] }}" alt="">
                        <div class="inss">
                            <a href="{{ url('course').'/'.$course['id'] }}"><h4>{{ $course['title'] }}</h4></a>
                            <h5>{{ $course['topic']['topic'] }}</h5>
                            <h6>{{ $course['educational']['stage'] }}</h6>
                            <div class="author">
                                <div class="row no-gutters">
                                    <div class="col-md-1 col-1">
                                        <img src="{{ $course['teacher']['some_detail']['photo'] }}" class="rounded-circle" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>{{ $course['teacher']['some_detail']['fullname'] }}</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                @empty
                    <center class="mt-5"><h4>Belum Pernah Mengikuti Sebuah Kursus</h4></center>
                @endforelse
            </div>
        </div>
        @if (auth()->guard('student')->check())
        <div class="discussion">
            <div class="row">
                <div class="col-md-6 col-6">
                    <h5>Recently Discussion</h5>
                </div>
                <div class="col-md-6 col-6">
                    <a href="#">
                        <!-- <h6>See More</h6> -->
                    </a>
                </div>
            </div>
            @forelse ($discussions as $discussion)
            <div class="discussion-box">
                <div class="inners">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="titles">
                                <ul>
                                    <li>
                                        <div class="images">
                                            <img src="{{ auth()->guard(session()->get('guard'))->user()->detail->photo }}" class="rounded-circle" alt="" width="54" height="54">
                                        </div>
                                    </li>
                                    <li>
                                        <div class="conts">
                                            <h4>{{ auth()->guard(session()->get('guard'))->user()->detail->fullname }}</h4>
                                            <h6>Student</h6>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="date">
                                            <p>{{ $discussion['created_at'] }}</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="content">
                                <h5>{{ $discussion['title'] }}</h5>
                                <p>{{ $discussion['discussion'] }}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="responses">
                    <div class="inst">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="accordion" id="faq">
                                <div class="card">
                                    <div class="card-header" id="faqhead2">
                                        <a href="{{ url('discussion').'/'.$discussion['id'] }}" class="btn btn-header-link collapsed" >Rensponses {{ $discussion['comments_count'] }}</a>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <center><h3>Belum Pernah Membuat Sebuah Konten Diskusi</h3></center>
            @endforelse
        </div>
        @endif
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<script>
    var data = {
        labels: [
            "Primary School",
            "Junior High School",
            "Senior High School",
            "Higher School",
            "Others",
        ],
        datasets: [
            {
            data: [4, 5, 2, 3,1],
            backgroundColor: [
                "#71caf1",
                "#45e8c3",
                "#ecc432",
                "#e883e8",
                "#8b41d6"
            ],
            hoverBackgroundColor: [
                "#71caf1",
                "#67f1d1",
                "#f1ce51",
                "#efa5ef",
                "#a86be6"
            ]
        }]
    };

var chart = document.getElementById('oilChart');
chart.height = 170;

var promisedDeliveryChart = new Chart(chart, {
  type: 'doughnut',
  data: data,
  options: {
  	responsive: true,
    legend: {
      display: false
    }
  }
});
</script>

@endsection