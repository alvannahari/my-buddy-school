<div class="profffsss">
    <a type="button" data-toggle="modal" data-target="#modal-profile"><img src="{{ $user['detail']['photo'] }}" class="rounded-circle" alt="" width="170" height="170"></a><button class="badge badge-pill badge-danger text-white" data-toggle="modal" data-target="#modal-remove-photo">X</button>
    <h6>{{ $user['detail']['fullname'] }}</h6>
    <p>{{ auth()->guard('student')->check() ? 'Student' : 'Teacher' }}</p>
</div>
<div class="card sidebar profiles border-0 mb-3">
    <ul class="list-group list-group-flush" id="list-menu-sidebar">
        <li class="list-group-item d-flex p-0 {{ (request()->is('profile')) ? 'active' : '' }}">
            <a href="{{ url('profile') }}" class="d-flex align-items-center link-all">
                <img src="{{ asset('assets/img/1111.png') }}" alt="world">
                <p class="mb-0 ml-2 text-bold">PERSONAL DATA</p>
            </a>
            <button class="btn-toggle-sidebar btn-show show">
                <i class="fal fa-chevron-down"></i>
            </button>
            <button class="btn-toggle-sidebar btn-hide">
                <i class="fas fa-times"></i>
            </button>
        </li>
        <li class="list-group-item p-0 {{ (request()->is('profile/location')) ? 'active' : '' }}">
            <a class="nav-link d-flex align-items-center justify-content-between"
                href="{{ url('profile/location') }}" id="navbar" role="button" data-toggle=""
                aria-haspopup="true" aria-expanded="false">
                <div class="menu d-flex">
                    <img src="{{ asset('assets/img/1112.png') }}" alt="book">
                    <p class="mb-0 ml-2">LOCATION</p>
                </div>
            </a>
        </li>
        <li class="list-group-item p-0 {{ (request()->is('profile/socmed')) ? 'active' : '' }}">
            <a class="nav-link d-flex align-items-center justify-content-between" href="{{ url('profile/socmed') }}"
                id="navbar" role="button" data-toggle="" aria-haspopup="true"
                aria-expanded="false">
                <div class="menu d-flex">
                    <img src="{{ asset('assets/img/1113.png') }}" alt="book">
                    <p class="mb-0 ml-2">SOCIAL MEDIA</p>
                </div>
            </a>
        </li>
        <li class="list-group-item p-0 {{ (request()->is('profile/password')) ? 'active' : '' }}">
            <a class="nav-link d-flex align-items-center justify-content-between" href="{{ url('profile/password') }}"
                id="navbar" role="button" data-toggle="" aria-haspopup="true"
                aria-expanded="false">
                <div class="menu d-flex">
                    <img src="{{ asset('assets/img/1114.png') }}" alt="book">
                    <p class="mb-0 ml-2">PASSWORD</p>
                </div>
            </a>
        </li>
        <li class="list-group-item p-0">
            <a class="nav-link d-flex align-items-center justify-content-between" href="{{ url('logout') }}"
                id="navbar" role="button" data-toggle="" aria-haspopup="true"
                aria-expanded="false">
                <div class="menu d-flex">
                    <img src="{{ asset('assets/img/1115.png') }}" alt="book">
                    <p class="mb-0 ml-2">LOG OUT</p>
                </div>
            </a>
        </li>
    </ul>
</div>

<!-- Modal -->
<div class="modal modal-delete fade" id="modal-remove-photo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus foto ini ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="confirms" id="btn-remove-photo">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-primary fade" id="modal-profile" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Photo Profile</h5>
            </div>
            <div class="modal-body">
                <form id="form-photo-profile">
                    @csrf
                    <div class="form-group">
                        <label class="mb-2" style="width: 100%">Photo </label>
                        <input type="file" name="photo" >
                        <span class="error"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Close</button>
                <button type="button" class="confirms" id="btn-update-photo">Save</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btn-remove-photo').click(function (e) { 
        // e.preventDefault();
        var button = $(this);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: "post",
            url: "{{ url('profile/remove') }}",
            dataType: 'json',
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if(!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.html('Ok');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                    $('#modal-remove-photo').modal('hide');
                }
            }
        });
    });

    $('#btn-update-photo').click(function (e) {
        e.preventDefault();
        var button = $(this);
        var formData = new FormData($('#form-photo-profile')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('profile/photo') }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert('Terjadi Kesalahan')
                    button.html('Ok');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>
