@extends('template.layout')

@section('content')
@include('profile.component.banner')

<section class="contents-courses profilessss pb-5 pt-5">
    <div class="prelative container">
        <div class="courses-sidebars">
            <div class="row">
                <div class="col-md-4">
                    @include('profile.component.navigation')
                </div>
                <div class="col-md-8">
                    <div class="box-right">
                        <p>Location</p>
                        <form id="form-location-profile">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mt-3">
                                    <label>Alamat</label>
                                    <input type="text" class="form-control" placeholder="Masukan alamat tempat tinggal anda" name="address" value="{{ $user['detail']['address'] }}">
                                    {{-- <select id="select-month">
                                        <option value="0">a
                                        <option value="1">a
                                        <option value="2">a
                                    </select> --}}
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label>Kota/Kabupaten</label>
                                    <input type="text" class="form-control" placeholder="Masukan kota tempat tinggal anda" name="city" value="{{ $user['detail']['city'] }}">
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label>Provinsi</label>
                                    <input type="text" class="form-control" placeholder="Masukan Provinsi tempat tinggal anda" name="province" value="{{ $user['detail']['province'] }}">
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label>Negara</label>
                                    <input type="text" class="form-control" placeholder="Masukan kecamatan tempat tinggal anda" name="country" value="{{ $user['detail']['country'] }}">
                                </div>
                                {{-- <div class="col-s --}}
                                <div class="col-md-12 mt-5">
                                    <button class="formssss" id="btn-save-location">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#btn-save-location').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "post",
            url: "{{ url('profile/location') }}",
            data: $('#form-location-profile').serialize(),
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                // console.log(response);
                if(!response.error) {
                    location.reload();
                } else {
                    alert('Terjadi kesalahan !. Silahkan diperiksa kembali data-data yang dimasukkan');
                    button.html('SIMPAN');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>
@endsection