@extends('template.layout')

@section('content')
@include('profile.component.banner')
<section class="contents-courses profilessss pb-5 pt-5">
    <div class="prelative container">
        <div class="courses-sidebars">
            <div class="row">
                <div class="col-md-4">
                    @include('profile.component.navigation')
                </div>
                <div class="col-md-8">
                    <div class="box-right">
                        <p>Personal Data</p>
                        <form id="form-personal-profile">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="formGroupExampleInput">Nama Lengkap</label>
                                    <input type="text" class="form-control" placeholder="Masukan nama lengkap anda" name="fullname" value="{{ $user['detail']['fullname'] }}">
                                </div>
                                <label for="">Gender</label>
                                <div class="col-md-12">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender"
                                            id="inlineRadio1" value="Laki-laki" {{ $user['detail']['gender'] == 'Laki-laki' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="inlineRadio1">Laki - Laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="gender"
                                            id="inlineRadio2" value="Perempuan" {{ $user['detail']['gender'] == 'Perempuan' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="formGroupExampleInput">Where were you born?</label>
                                    <input type="text" class="form-control" placeholder="Birth town" name="place_of_birth" value="{{ $user['detail']['place_of_birth'] }}">
                                </div>
                                <div class="col-md-12">
                                    <label for="formGroupExampleInput">Birth Date</label>
                                    <div class="form-row">
                                        <div class="col">
                                            <select id="select-day" name="date">
                                                @for ($i = 1; $i <= 31; $i++)
                                                    <option {{ substr($user['detail']['date_of_birth'], 8, 2) == $i ? 'selected' : '' }}>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col">
                                            <select id="select-month" name="month">
                                                <option value="01" {{ substr($user['detail']['date_of_birth'], 5, 2) == '01' ? 'selected' : '' }}>January</option>
                                                <option value="02" {{ substr($user['detail']['date_of_birth'], 5, 2) == '02' ? 'selected' : '' }}>February</option>
                                                <option value="03" {{ substr($user['detail']['date_of_birth'], 5, 2) == '03' ? 'selected' : '' }}>March</option>
                                                <option value="04" {{ substr($user['detail']['date_of_birth'], 5, 2) == '04' ? 'selected' : '' }}>April</option>
                                                <option value="05" {{ substr($user['detail']['date_of_birth'], 5, 2) == '05' ? 'selected' : '' }}>May</option>
                                                <option value="06" {{ substr($user['detail']['date_of_birth'], 5, 2) == '06' ? 'selected' : '' }}>June</option>
                                                <option value="07" {{ substr($user['detail']['date_of_birth'], 5, 2) == '07' ? 'selected' : '' }}>July</option>
                                                <option value="08" {{ substr($user['detail']['date_of_birth'], 5, 2) == '08' ? 'selected' : '' }}>August</option>
                                                <option value="09" {{ substr($user['detail']['date_of_birth'], 5, 2) == '09' ? 'selected' : '' }}>September</option>
                                                <option value="10" {{ substr($user['detail']['date_of_birth'], 5, 2) == '10' ? 'selected' : '' }}>October</option>
                                                <option value="11" {{ substr($user['detail']['date_of_birth'], 5, 2) == '11' ? 'selected' : '' }}>November</option>
                                                <option value="12" {{ substr($user['detail']['date_of_birth'], 5, 2) == '12' ? 'selected' : '' }}>December</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <select id="select-year" name="year">
                                                @for ($i = 70; $i >= 1; $i--)
                                                <option {{ substr($user['detail']['date_of_birth'], 0, 4) == 2020-$i ? 'selected' : '' }}>{{ 2020-$i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mt-3">
                                    @if (auth()->guard('student')->check())
                                    <label for="formGroupExampleInput">Grade</label>
                                    <input type="text" class="form-control" placeholder="Masukan penddikan anda" name="grade" value="{{ $user['detail']['grade'] }}">
                                    @else
                                    <label for="formGroupExampleInput">Degree</label>
                                    <input type="text" class="form-control" placeholder="Masukan gelar anda jika ada" name="degree" value="{{ $user['detail']['degree'] }}">
                                    @endif
                                </div>
                                <div class="col-md-12 mt-3">
                                    <label for="formGroupExampleInput">No. Handphone</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">+62</span>
                                        </div>
                                        <input type="number" class="form-control nohp" placeholder="" aria-label="Username" aria-describedby="basic-addon1" name="phone" value="{{ $user['detail']['phone'] }}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button class="formssss" id="btn-save-personal">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#btn-save-personal').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "post",
            url: "{{ url('profile/personal') }}",
            data: $('#form-personal-profile').serialize(),
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                // console.log(response);
                if(!response.error) {
                    location.reload();
                } else {
                    alert('Terjadi kesalahan !. Silahkan diperiksa kembali data-data yang dimasukkan');
                    button.html('SIMPAN');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>
@endsection