@extends('template.layout')

@section('content')
@include('profile.component.banner')
<section class="contents-courses profilessss pb-5 pt-5">
    <div class="prelative container">
        <div class="courses-sidebars">
            <div class="row">
                <div class="col-md-4">
                    @include('profile.component.navigation')
                </div>
                <div class="col-md-8">
                    <div class="box-right">
                        <p>Personal Data</p>
                        <form id="form-password-profile">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="old_password">Kata Sandi Lama</label>
                                    <input type="password" class="form-control" id="old_password" name="password" placeholder="Kata Sandi Lama">
                                </div>
                                <div class="col-md-12">
                                    <label for="new_password">Kata Sandi Baru</label>
                                    <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Kata Sandi Baru">
                                </div>
                                <div class="col-md-12">
                                    <label for="new_password_confirmation">Konfirmasi Kata Sandi Baru</label>
                                    <input type="password" class="form-control" id="new_password_confirmation" name="new_password_confirmation" placeholder="Kata Sandi Baru">
                                </div>
                                <div class="col-md-12 mt-3">
                                    <button class="formssss" id="btn-save-password">SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#btn-save-password').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "post",
            url: "{{ url('profile/password') }}",
            data: $('#form-password-profile').serialize(),
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                // console.log(response);
                if(!response.error) {
                    location.reload();
                } else {
                    alert('Terjadi kesalahan !. Silahkan diperiksa kembali data-data yang dimasukkan');
                    button.html('SIMPAN');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>
@endsection