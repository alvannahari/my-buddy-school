@extends('template/layout')

@section('title', 'Kuis')

@section('content')
@include('course.component.banner')
<section class="courses-insides quiz-list-backs">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content quiz-intro">
                        <div class="overview">
                            <h6>{{ $quiz['title'] }}</h6>
                            <p>Latihan soal pada quiz ini adalah soal pilihan ganda. Petunjuk pengerjaan latihan
                                soal ini yaitu dengan cara memilih salah satu jawaban yang kamu anggap tepat dari
                                jawaban yang sudah tersedia pada kolom jawaban A, B, C, dan D pada setiap soal.</p>
                            <div class="btn-quiz">
                                @if ($quiz['hasResult'])
                                    <button class="btn-download-pembahasan exampleModalQuizs" id="exampleModalQuizs" data-toggle="modal" data-target="#exampleModalQuiz">START QUIZ</button>
                                    @if ($quiz['file'] != null)
                                    <a href="{{ $quiz['file'] }}" target="_blank"><button class="btn-start-quiz">DOWNLOAD PEMBAHASAN</button></a>
                                    @endif
                                @else
                                    <button class="btn-start-quiz" id="btn-start-quiz">START QUIZ</button>
                                    @if ($quiz['file'] != null)
                                    <button class="btn-download-pembahasan">DOWNLOAD PEMBAHASAN</button>
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="score">
                            <h6>Score Quiz</h6>
                            @if ($quiz['hasResult'])
                            <p>Score dari pengerjaan latihan soal adalah <span>{{ $quiz['score'] }}</span></p>
                            @else
                            <p>Score dari pengerjaan latihan soal adalah <span>0</span></p>
                            @endif
                        </div>
                    </div>

                    {{-- QUestion Start --}}
                    @if (!$quiz['hasResult'])
                    <form id="form-quiz">
                        @csrf
                        @foreach ($quiz['questions'] as $question)
                            <div class="card form-edit" style="width: 100%;display:none">
                                <div class="card-body">
                                    <div class="container-soal">
                                        <div class="row ">
                                            <div class="col-md-12">
                                                <div class="soal">
                                                    <div class="title">
                                                        @php
                                                            $iteration = $loop->iteration
                                                        @endphp
                                                        <h6>{{ $iteration.'. '.$question['question'].' ?' }}</h6>
                                                    </div>
                                                    <!-- <div class="images"><img src="../../assets/img/bio.png" alt=""></div> -->
                                                    <fieldset class="form-group">
                                                        <div class="row">
                                                            <div class="col-sm-11 list">
                                                                @php
                                                                    $alphabet = range('A','Z');
                                                                @endphp
                                                                @foreach ($question['options'] as $option)
                                                                <div class="form-check" style="margin-bottom: 20px;">
                                                                    <input class="form-check-input" type="radio"
                                                                    name="question{{ $iteration }}" id="option{{ $iteration.$loop->iteration }}" value="{{ $loop->iteration }}" form="form-quiz">
                                                                    <label class="form-check-label form-control list-group-item"
                                                                        for="option{{ $iteration.$loop->iteration }}">
                                                                        <span>{{ $alphabet[$loop->index] }}.</span>
                                                                        {{ $option['option'] }}
                                                                    </label>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        @if($loop->last)
                                        <div class="row" style="justify-content : center">
                                            <button class="btn btn-info" type="button" data-toggle="modal" data-target="#modal-confirm-quiz" style="width: 174px">Selesai</button>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </form>
                    @endif
                    {{-- Question End --}}

                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="exampleModalQuiz" tabindex="-1" role="dialog" aria-labelledby="exampleModalQuizTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Congratulations</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <div class="inns">
                    {{-- <p>Anda berhasil menyelesaikan latihan soal ini dengan rincian <strong>8</strong> soal benar dan <strong>2</strong> soal salah.Score yang anda dapatkan adalah :</p> --}}
                    <p>Anda berhasil menyelesaikan latihan soal ini dengan score yang anda dapatkan adalah :</p>
                    @if ($quiz['hasResult'])
                    <h5>{{ $quiz['score'] }}</h5>
                    @endif
                    <button class="close" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm-quiz" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Apakah Anda Yakin telah Menyelesaikan Semua Pertanyaan?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms" id="btn-confirm-quiz">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var unsaved = false;

    $(window).bind('beforeunload', function(){
        if(unsaved){
            return 'Anda Akan Memiliki Nilai 0 atau Kosong. Anda Yakin ?';
        }
    });

    $('#btn-start-quiz').click(function (e) { 
        e.preventDefault();
        var button = $(this);
        $.ajax({
            type: "post",
            url: "{{ url('quiz').'/'.$quiz['id'].'/submit' }}",
            data: $('#form-quiz').serialize(),
            beforeSend: function () {
                button.html('MENYIAPKAN ...');
                button.prop('disabled', true);
            },
            success: function (response) {
                // location.reload();
                if(!response.error) {
                    alert('Silahkan Dikerjakan Dengan Sebaik Mungkin !. Jika Anda Keluar dari Halaman Ini Nilai Skor Kuis Bernilai 0.');
                    setTimeout( function() {
                        unsaved = true;
                        $('.quiz-intro').hide();
                        $('.form-edit').css('display', 'block');
                    }, 1000);
                }
            }
        });
    });

    $(".list-group-item").click(function () {

        // Select all list items 
        var listItems = $(this).parent().parent().find('.list-group-item');

        // // Remove 'active' tag for all list items 
        for (let i = 0; i < listItems.length; i++) {
            listItems[i].classList.remove("active");
        }

        // Add 'active' tag for currently selected item 
        this.classList.add("active");
    });

    function testReload() {
        unsaved = false;
        location.reload();
    }

    // $('#modal-confirm-quiz').on('hidden.bs.modal', function () {
    //     unsaved = true;
    // })
    // $('#modal-confirm-quiz').on('show.bs.modal', function () {
    //     unsaved = false;
    // })

    $('#btn-confirm-quiz').click(function (e) { 
        e.preventDefault();
        var button = $(this);
        unsaved = false;
        $.ajax({
            type: "post",
            url: "{{ url('quiz').'/'.$quiz['id'].'/submit' }}",
            data: $('#form-quiz').serialize(),
            beforeSend: function () {
                button.html('Mengirim Jawaban...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                // location.reload();
                // console.log(response);
                if(!response.error) {
                    location.reload();
                } else {
                    button.html('Kirim Ulang');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>
@endsection