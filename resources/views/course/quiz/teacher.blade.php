@extends('template/layout')

@section('title', 'Kuis')

@section('content')
@include('course.component.banner')
<section class="courses-insides quiz-list-backs">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <ul class="nav justify-content-end">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Option
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-form-quiz" >
                                    <img class="mr-2" src="{{ asset('assets/img/pencil.png') }}" alt="" width="18">
                                    Edit
                                </a>
                                <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-state-quiz">
                                    @if ($quiz['is_active'] == 0)
                                    <img class="mr-2" src="{{ asset('assets/img/Vector (4).png') }}" alt="" width="18">  
                                    Aktifkan
                                    @else
                                    <img class="mr-2" src="{{ asset('assets/img/signal.png') }}" alt="" width="18">  
                                    Tangguhkan
                                    @endif
                                </a>
                                <a class="dropdown-item" href="#"  type="button" data-toggle="modal" data-target="#modal-delete-quiz">
                                    <img class="mr-2" src="{{ asset('assets/img/trash.png') }}" alt="" width="18">
                                    Hapus
                                </a>
                            </div>
                        </div>
                        {{-- <li class="nav-item">
                            <a class="nav-link btn btn-primary" data-toggle="modal" data-target="#modal-form-quiz">
                                <img class="mr-1" src="{{ asset('assets/img/pencil.png') }}" alt="">
                                Edit
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn btn-warning" href="#" type="button" data-toggle="modal" data-target="#modal-state-quiz">
                                @if ($quiz['is_active'] == 0)
                                <img class="mr-1" src="{{ asset('assets/img/Vector (4).png') }}" alt="">  
                                Activate
                                @else
                                <img class="mr-1" src="{{ asset('assets/img/signal.png') }}" alt="">  
                                Disable
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link btn btn-danger" type="button" data-toggle="modal" data-target="#modal-delete-quiz" >
                                <img class="mr-1" src="{{ asset('assets/img/trash.png') }}" alt="">
                                Delete
                            </a>
                        </li> --}}
                    </ul>
                    <div class="box-content quiz-intro">
                        <div class="overview">
                            <h6>{{ $quiz['title'] }}</h6>
                            <p>Latihan soal pada quiz ini adalah soal pilihan ganda. Petunjuk pengerjaan latihan
                                soal ini yaitu dengan cara memilih salah satu jawaban yang kamu anggap tepat dari
                                jawaban yang sudah tersedia pada kolom jawaban A, B, C, dan D pada setiap soal.</p>
                            <div class="btn-quiz">
                                <button class="btn-start-quiz" type="button" data-toggle="modal" data-target="#modal-form-question">Add Question</button>
                                @if ($quiz['file'] == null)
                                <button class="btn-download-pembahasan disabled">Download Pembahasan</button>
                                @else
                                <a href="{{ $quiz['file'] }}" target="_blank"><button class="btn-start-quiz">Download Pembahasan</button></a>
                                @endif
                            </div>
                        </div>
                    </div>

                    {{-- QUestion Start --}}
                    @forelse ($quiz['questions'] as $question)
                        <div class="card form-edit" style="width: 100%">
                            <div class="card-body">
                                <li class="nav-item dropdown" style="display: block;">
                                    <a style="float: right;" class="nav-link d-flex flex-row align-items-center" onclick="deleteQuestion({{ $question['id'] }})">
                                        <i class="fa fa-trash" style="color: red"></i>
                                    </a>
                                    {{-- <a href="#" style="float: right;" class="nav-link d-flex flex-row align-items-center" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="../../assets/img/titiktiga.png" width="24px" alt="">
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="#">
                                            <img src="../../assets/img/pencil.png" alt="">  
                                            Edit
                                        </a>
                                        <a class="dropdown-item" href="#"> 
                                            <img src="../../assets/img/Vector (4).png" alt="">  
                                            Activate
                                        <a class="dropdown-item" href="#">
                                            <img src="../../assets/img/trash.png" alt="">
                                            Delete</a>
                                        </a>
                                    </div> --}}
                                </li>
                                <div class="container-soal">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <div class="soal">
                                                <div class="title">
                                                    <h6>{{ $loop->iteration.'. '.$question['question'].' ?' }}</h6>
                                                </div>
                                                @if ($question['image'] != null)
                                                    <div class="images ml-1"><img src="{{ $question['image'] }}" alt=""></div>
                                                @endif
                                                <!-- <div class="images"><img src="../../assets/img/bio.png" alt=""></div> -->
                                                <fieldset class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-11 list">
                                                            @php
                                                                $alphabet = range('A','Z');
                                                            @endphp
                                                            @foreach ($question['options'] as $option)
                                                            <div class="form-check" style="margin-bottom: 20px;">
                                                                <input class="form-check-input" type="radio"
                                                                name="question" id="option" value="" form="form-quiz">
                                                                <label class="form-check-label form-control list-group-item {{ $option['is_answer'] == '1' ? 'active' : '' }}" for="option" >
                                                                    <span>{{ $alphabet[$loop->index] }}.</span>
                                                                    {{ $option['option'] }}
                                                                </label>
                                                            </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <center><h4>Belum ada pertanyaan dalam kuis ini</h4></center>
                    @endforelse
                    {{-- Question End --}}

                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-form-quiz" tabindex="-1" aria-labelledby="ModalDeleteTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Quiz</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-quiz">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Isi Judul" name="title" value="{{ $quiz['title'] }}">
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>File Pembahasan</label><span class="ml-1" style="font-size:12px">*type:pdf, max:15mb</span>
                        <input type="file" name="file" value="{{ $quiz['file'] }}">
                        <span class="error"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="keluar" data-dismiss="modal">Batal</button>
                <button type="button" class="simpan" id="btn-edit-quiz">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-state fade show" id="modal-state-quiz" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
            </div>
            <div class="modal-body">
                @if ($quiz['is_active'] == 0)
                <p>Apakah anda yakin ingin mengaktifkan status kuis ini?</p>
                @else
                <p>Apakah anda yakin ingin tangguhkan status kuis ini?</p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms" id="btn-state-quiz">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-delete fade show" id="modal-delete-quiz" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus kuis ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Batal</button>
                <button type="button" class="confirms" id="btn-delete-quiz">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-question fade show" id="modal-form-question" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pertanyaan Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-quiz">
                    @csrf
                    <div class="form-group row description mb-0">
                        <label class="col-sm-2 col-form-label">Pertanyaan</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" placeholder="Add your question here......" rows="2" name="question"></textarea>
                            <span class="error"></span>
                        </div>
                    </div>
                    <div class="form-group row description">
                        <label class="col-sm-2 col-form-label">Gambar</label>
                        <div class="col-sm-10">
                            <div class="file">
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                            <label class="col-form-label col-sm-2 pt-0">Pilihan</label>
                            <div class="col-sm-10 list">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answer" value="0" style="margin-top: 10px;">
                                    <input type="text" class="form-control mt-0" name="option[0]">
                                    <span class="error"></span>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answer" value="1" style="margin-top: 10px;">
                                    <input type="text" class="form-control" name="option[1]">
                                    <span class="error"></span>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answer" value="2" style="margin-top: 10px;">
                                    <input type="text" class="form-control" name="option[2]">
                                    <span class="error"></span>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answer" value="3" style="margin-top: 10px;">
                                    <input type="text" class="form-control" name="option[3]">
                                    <span class="error"></span>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answer" value="4" style="margin-top: 10px;">
                                    <input type="text" class="form-control" name="option[4]">
                                    <span class="error" id="error-answer"></span>
                                </div>
                            </div>
                        </div>
                    </fieldset>     
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="keluar" data-dismiss="modal">Batal</button>
                <button type="button" class="simpan" id="btn-question">Simpan</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-delete fade show" id="modal-delete-question" tabindex="-1" role="dialog" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Konfirmasi</h5>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus pertanyaan ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">batal</button>
                <button type="button" class="confirms" id="btn-delete-question">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var question_id = 0;

    $('#btn-edit-quiz').click(function (e) { 
        e.preventDefault();
        emptyFormInput();
        var button = $(this);
        var formData = new FormData($('#form-edit-quiz')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('quiz').'/'.$quiz['id'].'/update' }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if(!response.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Terjadi Kesalahan !!!');
                    button.html('Simpan');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('#btn-state-quiz').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "get",
            url: "{{ url('quiz').'/'.$quiz['id'].'/state' }}",
            beforeSend: function() {
                button.html('Memperbarui...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    button.html('Ok');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('#btn-delete-quiz').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "get",
            url: "{{ url('quiz').'/'.$quiz['id'].'/delete' }}",
            beforeSend: function() {
                button.html('Menghapus...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    window.location = "{{ url('course').'/'.$course['id'].'/quiz' }}";
                } else {
                    button.html('Ok');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('#btn-question').click(function (e) { 
        e.preventDefault();
        emptyFormInput();
        var button = $(this);
        var formData = new FormData($('#form-quiz')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('question').'/'.$quiz['id'] }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if(!response.error) {
                    location.reload();
                } else {
                    console.log(response);
                    for (let key of Object.keys(response.error)) {
                        if(key == 'answer') {
                            $('#error-answer').html(response.error[key]);
                        } else {
                            let temp = key.slice(-1);
                            if(!isNaN(temp)){
                                $('[name="option['+temp+']"]').addClass('is-invalid');
                                $('[name="option['+temp+']"]').next().html(response.error[key]);
                            } else {
                                $('[name="'+key+'"]').addClass('is-invalid');
                                $('[name="'+key+'"]').next().html(response.error[key]);
                            }
                        }
                    }
                    alert('Terjadi Kesalahan !!!');
                    button.html('Simpan');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    });
 
    function deleteQuestion(id) {
        question_id = id;
        $('#modal-delete-question').modal('show');
    }

    $('#btn-delete-question').click( function() {
        var button = $(this);
        $.ajax({
            type: "get",
            url: "{{ url('question').'/'}}"+question_id+'/delete',
            beforeSend: function() {
                button.html('Menghapus...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    button.html('Ok');
                    button.removeClass('disabled');
                    button.prop('disabled', false);
                }
            }
        });
    })

</script>
@endsection