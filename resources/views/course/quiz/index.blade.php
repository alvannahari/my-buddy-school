@extends('template/layout')

@section('title', 'Kuis')

@section('content')
@include('course.component.banner')

<section class="courses-insides quiz-list-backs">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content quiz">
                        <div class="overview">
                            <h6>Quiz</h6>
                            <p>Quiz Content</p>
                        </div>
                        <div class="content">
                            <div class="list-content">
                                @forelse ($quizzes['quizzes'] as $quiz)
                                    <a href="{{ url('quiz').'/'.$quiz['id'] }}">
                                        <div class="item quiz mb-3 d-flex align-items-center justify-content-between">
                                            <p class="title active mr-2">{{ $quiz['title'] }}</p>
                                            @if (auth()->guard('student')->check() && $quiz['hasResult'] )
                                            <div class="done d-flex align-items-center justify-content-between">
                                                <p>Done</p>
                                                <img src="{{ asset('assets/img/done.png') }}" alt="done" class="ml-2">
                                            </div>
                                            @elseif (auth()->guard('teacher')->check() && $quiz['is_active'] )
                                                <div class="done d-flex align-items-center justify-content-between">
                                                    <p>Activate</p>
                                                    <img src="{{ asset('assets/img/done.png') }}" alt="done" class="ml-2">
                                                </div>
                                            @elseif (auth()->guard('teacher')->check() && $quiz['is_active'] == 0 )
                                                <div class="done d-flex align-items-center justify-content-between">
                                                    <p>Disabled</p>
                                                    <img src="{{ asset('assets/img/signal.png') }}" alt="done" class="ml-2">
                                                </div>
                                            @endif
                                        </div>
                                    </a>
                                @empty
                                    <tr>
                                        <td colspan="3"><center>Tidak Ada Kuis Ditemukan!!</center></td>
                                    </tr>
                                @endforelse
                            </div>
                            @if (auth()->guard('teacher')->check())
                            <button type="button" data-toggle="modal" data-target="#modal-form-quiz">
                                ADD QUIZ
                            </button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-form-quiz" tabindex="-1" aria-labelledby="ModalDeleteTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New Quiz</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-add-quiz">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Isi Judul" name="title">
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>File Pembahasan</label><span class="ml-1" style="font-size:12px">*type:pdf, max:15mb</span>
                        <input type="file" name="file">
                        <span class="error"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="keluar" data-dismiss="modal">Cancel</button>
                <button type="button" class="simpan" id="btn-add-quiz">Simpan</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btn-add-quiz').click(function (e) { 
        e.preventDefault();
        emptyFormInput();
        var button = $(this);
        var formData = new FormData($('#form-add-quiz')[0]);

        $.ajax({
            type: "post",
            url: "{{ url('quiz').'/'.$course['id'] }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function () {
                button.html('Menyimpan...');
                button.addClass('disabled');
                button.prop('disabled', true);
            },
            success: function (response) {
                if(!response.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Terjadi Kesalahan !!!');
                    button.html('Menyimpan...');
                    button.removeClass('disabled');
                    button.prop('disabled', true);
                }
            }
        });
    });
</script>

@endsection