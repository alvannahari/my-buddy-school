@extends('template/layout')

@section('content')
@include('course.component.banner')

<section class="courses-insides">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content learn-course">
                        <div class="overview">
                            <h6>{{ $theory['title'] }}</h6>
                            <p>{{ $theory['description'] }}</p>
                        </div>
                        <div class="materi">
                            <h5>Materi</h5>
                            <div class="frames">
                                <div class="inns">
                                    <div class="heads">
                                        <div class="btn-heads">
                                            <button class="download"><img src="{{ asset('assets/img/ppp.png') }}" alt=""><span>Download
                                            </span></button>
                                            <button class="fullscreen"><img src="{{ asset('assets/img/layars.png') }}"
                                                    alt=""><span>Layar Penuh
                                                </span></button>
                                        </div>
                                    </div>
                                    <div class="txt">
                                        <h5>{{ $theory['title'] }}</h5>
                                        {!! $theory['content'] !!}
                                        {{-- <p> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tumbuhan merupakan salah satu
                                            dari
                                            klasifikasi
                                            makhluk
                                            hidup.
                                            Tumbuhan
                                            memiliki klorofil atau zat hijau daun yang berfungsi sebagai media
                                            penciptaan makanan dan untuk proses fotosintesis. Dalam ilmu biologi,
                                            tumbuhan termasuk organisme yang disebut Regnum Plantaeyang merupakan
                                            organisme multiseluler atau terdiri atas banyak sel. Tercatat sekitar
                                            350.000 spesies tumbuhan, dari jumlah tersebut 258.650 jenis merupakan
                                            tumbuhan berbunga dan 18.000 jenis termasuk tumbuhan lumut. Hampir semua
                                            anggota tumbuhan bersifat autotrof dan mendapatkan energi langsung dari
                                            cahaya matahari melalui proses fotosintesis.</p>
                                        <p>Ciri yang sangat mudah dikenali pada tumbuhan adalah warna hijau yang
                                            dominan akibat kandungan pigmen klorofil yang berperan vital dalam
                                            proses penangkapan energi melalui fotosintesis sehingga tumbuhan secara
                                            umum bersifat autotrof. Beberapa perkecualian, seperti pada sejumlah
                                            tumbuhan parasit. Hal ini terjadi karena akibat adaptasi terhadap cara
                                            hidup dan lingkungan yang unik. Sifatnya yang autotrof, membuat tumbuhan
                                            selalu menempati posisi pertama dalam rantai aliran energi melalui
                                            organisme hidup (rantai makanan).</p> --}}
                                        {{-- <div class="mid">
                                            <h6>Apa Itu Tumbuhan</h6>
                                            <img src="../../assets/img/ttt.png" alt="">
                                        </div>
                                        <p>Tumbuhan bersifat stasioner atau tidak bisa berpindah atas kehendak
                                            sendiri, meskipun beberapa alga hijau bersifat motil (mampu berpindah)
                                            karena memiliki flagelum. Akibat sifatnya yang pasif ini tumbuhan harus
                                            beradaptasi secara fisik atas perubahan lingkungan dan gangguan yang
                                            diterimanya. Variasi morfologi tumbuhan jauh lebih besar daripada
                                            anggota kerajaan lainnya. Selain itu, tumbuhan menghasilkan banyak
                                            sekali metabolit sekunder sebagai mekanisme pertahanan hidup atas
                                            perubahan lingkungan atau serangan pengganggu. Pada tingkat selular,
                                            dinding sel yang tersusun dari selulosa, hemiselulosa, dan pektin
                                            menjadi ciri khasnya, meskipun pada tumbuhan tingkat sederhana
                                            kadang-kadang hanya tersusun dari pektin. Hanya sel tumbuhan yang
                                            memiliki plastida dan vakuola yang besar serta seringkali mendominasi
                                            volume sel. Supaya lebih memahami ciri-ciri dari tumbuhan secara rinci
                                            dapat dilihat pada tabel di bawah ini.</p>
                                        <div class="youtube">
                                            <iframe width="100%" height="375"
                                                src="https://www.youtube.com/embed/08Ndzf5-HxI" frameborder="0"
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowfullscreen></iframe>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>
@endsection