@extends('template.layout')

@section('content')
@include('course.component.banner')
<section class="courses-insides taken-backs">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content taken">
                        <div class="card form-edit">
                            <div class="card-body">
                                <input type="hidden" id="id-theory" value="{{ $theory['id'] ?? '' }}">
                                <form id="form-theory">
                                    @csrf
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" placeholder="Isi Judul" name="title" value="{{ $theory['title'] ?? '' }}">
                                        <span class="error"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea rows="5" class="form-control" placeholder="Isi Deskripsi" name="description">{{ $theory['description'] ?? '' }}</textarea>
                                        <span class="error"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Content</label>
                                        <textarea class="form-control tinymce-editor" rows="5" cols="40" name="content">{{ $theory['content'] ?? '' }}</textarea>
                                        <span class="error"></span>
                                    </div>
                                </form>
                                <div class="row mt-4">
                                    <div class="button col-6" style="text-align: right">
                                        <button class="keluar" data-dismiss="modal">Cancel</button>
                                    </div>
                                    <div class="button col-6">
                                        <button class="simpan" id="btn-form-theory">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('assets/js/tinymce_v2/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script>
    var state = '{{ $state }}';
    var theory_id = $('#id-theory').val();

    tinymce.init({
        selector: 'textarea.tinymce-editor',
        init_instance_callback : function(editor) {
            let freeTiny = document.querySelector('.tox .tox-notification--in');
            freeTiny.style.display = 'none';
        },
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        height: 930,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount',
            'quickbars media',
        ],
        toolbar: 'undo redo | ' +
        'bold italic backcolor | alignleft aligncenter ' +
        'alignright alignjustify | bullist numlist outdent indent | ' +
        'removeformat | quickimage media',
        content_css: '//www.tiny.cloud/css/codepen.min.css'
    });

    $('#btn-form-theory').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        let url = "{{ url('theory').'/'.$course['id'] }}";
        if (state == '1') {
            url = "{{ url('theory').'/'}}"+theory_id+'/update';
        }

        let formData = new FormData($('#form-theory')[0]);
        $.ajax({    
            type: "post",
            url: url,
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                button.html('Menyimpan Data ...');
                button.prop('disabled', true);
            },
            success : function(response) {
                if (!response.error) {
                    window.location = "{{ url('course').'/'.$course['id'] }}";
                    // history.back();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                    button.html('Simpan');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>

@endsection