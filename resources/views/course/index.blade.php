@extends('template/layout')

@section('content')
<section class="cover-all">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    @if ($topic == 'all')
                    <h4>{{ $educational }} </h4>
                    @else
                    <h4>{{ $topic }} </h4>
                    @endif
                    <p>Asah kemampuanmu dengan beragam materi.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div>
        </div>
    </div>
</section>

<section class="contents-courses pb-5">
    <div class="breadcrumbs">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('course/allCourse') }}">Course</a></li>
                <li class="breadcrumb-item"><a href="{{ url('course/'.$educational.'/all') }}">{{ $educational }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $topic }}</li>
            </ol>
        </nav>
    </div>
    <div class="prelative container">
        <div class="courses courses-sidebars">
            <div class="row">
                <div class="col-md-3">
                    <div class="card sidebar border-0 mb-3">
                        <ul class="list-group list-group-flush" id="list-menu-sidebar">
                            <li class="list-group-item d-flex p-0">
                                <a href="{{ url('course/'.$educational).'/all' }}" class="d-flex active align-items-center link-all">
                                    <img src="{{ asset('assets/img/world.svg') }}" alt="world">
                                    <p class="mb-0 ml-2 text-bold">ALL</p>
                                </a>
                                <button class="btn-toggle-sidebar btn-show show">
                                    <i class="fal fa-chevron-down"></i>
                                </button>
                                <button class="btn-toggle-sidebar btn-hide">
                                    <i class="fas fa-times"></i>
                                </button>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/book.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">LESSON</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Natural Science' }}">Natural Science</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Mathematic' }}">Mathematic</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/English Language' }}">English Language</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Indonesian Language' }}">Indonesian Languange</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Social Science' }}">Social Science</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Vernacular' }}">Vernacular</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Civilization' }}">Civilization</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/softskills.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">SOFTSKILL</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Communication' }}">Communication</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Social Skill' }}">Social Skill</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Personal Skill' }}">Personal Skill</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Leadership' }}">Leadership</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Enterpreneurship' }}">Enterpreneurship</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/world.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">PRA-VOCATIONAL</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Craft' }}">Craft</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Cultivation' }}">Cultivation</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Engineering' }}">Engineering</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Cooking' }}">Cooking</a>
                                </div>
                            </li>
                            
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/pedagogics.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">OTHERS</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Sports' }}">Sports</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Art' }}">Art</a>
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Music' }}">Music</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/others.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">RELIGION</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('course/'.$educational).'/Islam' }}">Islam</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="mitras">
                        <div class="tops">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <h5>Mitra</h5>
                                </div>
                                <div class="col-md-6 col-6">
                                    <a href="{{ url('mitra') }}">
                                        <h6>See More</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @forelse ($mitra as $mitra)
                            <div class="col-md-3 col-6">
                                <div class="box-mitras">
                                    <a href="{{ url('mitra').'/'.$mitra['id'] }}">
                                        <p><img class="rounded-circle" src="{{ $mitra['image'] }}" alt="" width="130" height="130"></p>
                                        <h5>{{ $mitra['name'] }}</h5>
                                        <h6>{{ $mitra['city'] }}</h6>
                                    </a>
                                </div>
                            </div>
                            @empty
                            <div class="col-md-3 col-6">
                                <div class="box-mitras">
                                    <a href="#">
                                        <img src="{{ asset('assets/img/LogoPng-279x300.png') }}" alt="">
                                        <h5>SMA LABORATORIUM</h5>
                                        <h6>Kota Malang</h6>
                                    </a>
                                </div>
                            </div>
                            @endforelse
                        </div>
                    </div>
                    <div class="tutor-bss">
                        <div class="tops">
                            <div class="row">
                                <div class="col-md-6 col-6">
                                    <h5>Tutor BSS</h5>
                                </div>
                                <div class="col-md-6 col-6">
                                    <a href="{{ url('teacher') }}">
                                        <h6>See More</h6>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach ($tutor as $item)
                            <div class="col col-md-auto">
                                <div class="box-tutor">
                                    <a href="{{ url('teacher').'/'.$item['id'] }}">
                                        <img class="rounded-circle" src="{{ $item['some_detail']['photo'] }}" alt="" width="120" height="120">
                                        <h5>{{ $item['some_detail']['fullname'] }}</h5>
                                        <h6>Shared {{ $item['courses_count'] }} Course</h6>
                                    </a>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="courses mt-4">
                        <div class="titels">
                            <h6>Courses</h6>
                        </div>
                        <div class="row no-gutters pb-5">
                            @forelse ($courses as $course)
                                <div class="col-md-4 col-6">
                                    <div class="box-content mb-3">
                                        <img src="{{ $course['cover'] }}" alt="">
                                        <div class="inss">
                                            <a href="{{ url('course').'/'.$course['id'] }}"><h4>{{ $course['title'] }}</h4></a>
                                            <h5 style="margin-bottom: 135px">{{ $course['topic']['topic'] }}</h5>
                                            {{-- <h6>{{ $course['educational']['stage'] }}</h6> --}}
                                            <div class="author mb-0">
                                                <div class="row no-gutters mb-3">
                                                    <div class="col-md-1 col-1">
                                                        <img class="rounded-circle" src="{{ $course['teacher']['some_detail']['photo'] }}" alt="">
                                                    </div>
                                                    <div class="col-md-9 col-9">
                                                        <div class="texts">
                                                            <h3>{{ $course['teacher']['some_detail']['fullname'] }}</h3>
                                                            <p>Tutor</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if ($course['has_taken'])
                                                    <button type="button" type="button" class="quit" onclick="statusCourse({{ $course['id'] }}, 'quit')">QUIT COURSE</button>
                                                @else
                                                    <button type="button" class="take" onclick="statusCourse({{ $course['id'] }}, 'take')">TAKE COURSE</button>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <center><h1>Tidak Ada Kursus Ditemukan !</h1></center>
                            @endforelse

                            {{-- <div class="col-md-3 col-6">

                                <div class="box-content mb-3">
                                    <img src="{{ asset('assets/img/Frames4.jpg') }}" alt="">
                                    <div class="inss">
                                        <h4>Proses Fotosintesis pada Tanaman</h4>
                                        <h5>ENGLISH LANGUAGE</h5>
                                        <h6>Primary School (SD)</h6>
                                        <div class="author">
                                            <div class="row no-gutters">
                                                <div class="col-md-1 col-1">
                                                    <img src="{{ asset('assets/img/35x35.png') }}" alt="">
                                                </div>
                                                <div class="col-md-9 col-9">
                                                    <div class="texts">
                                                        <h3>Hayley Williams</h3>
                                                        <p>Tutor</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="button" data-toggle="modal" data-target="#ModalTake"
                                            class="take">TAKE COURSE</button>
                                    </div>
                                </div>
                            </div> --}}

                        </div>
                        {{-- <button class="loads mb-5">LOAD MORE</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="ModalTake" tabindex="-1" role="dialog" aria-labelledby="ModalTakeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to take this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalQuit" tabindex="-1" role="dialog" aria-labelledby="ModalQuitTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to quit this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var idCourse = 1;
    var stateCourse = 1;

    function statusCourse(id, status) {
        idCourse = id;
        stateCourse = status;
        if (status == 'quit') $('#ModalQuit').modal('show');
        else $('#ModalTake').modal('show');
    }

    $('.confirms').click(function () {
        var button = $(this);
        let url = "{{ url('takeCourse').'/'}}"+idCourse;
        if (stateCourse == 'quit') url = "{{ url('quitCourse').'/'}}"+idCourse;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            complete: function(response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    })
</script>

@endsection