@extends('template/layout')

@section('content')
<section class="cover-all">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>All Course </h4>
                    <p>Asah kemampuanmu dengan beragam materi.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div>
        </div>
    </div>
</section>


<section class="contents-courses pb-5">
    <div class="breadcrumbs">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Course</a></li>
                <li class="breadcrumb-item active" aria-current="page">All Course</li>
            </ol>
        </nav>
    </div>
    <div class="prelative container">
        <div class="courses">
            <div class="titels">
                <h6>Latest Courses</h6>
            </div>
            <div class="slick-carousel">
                @foreach ($latest as $course)
                <div>
                    <div class="box-content">
                        <img src="{{ $course['cover'] }}" alt="">
                        <div class="inss">
                            <h4>{{ $course['title'] }}</h4>
                            <h5>{{ $course['topic']['topic'] }}</h5>
                            <h6>{{ $course['educational']['stage'] }}</h6>
                            <div class="author">
                                <div class="row no-gutters">
                                    <div class="col-md-1 col-1">
                                        <img class="rounded-circle" src="{{ $course['teacher']['some_detail']['photo'] }}" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>{{ $course['teacher']['some_detail']['fullname'] }}</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if ($course['has_taken'])
                            <button type="button" type="button" class="quit" onclick="statusCourse({{ $course['id'] }}, 'quit')">QUIT COURSE</button>
                            @else
                            <button type="button" type="button" class="take" onclick="statusCourse({{ $course['id'] }}, 'take')">TAKE COURSE</button>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="dividers-hr"></div>
        </div>
        <div class="courses">
            <div class="titels">
                <h6>All Courses</h6>
            </div>
            <div class="row no-gutters pb-5">
                @forelse ($courses as $course)
                <div class="col-md-3 col-6">
                    <div class="box-content mb-3">
                        <img src="{{ $course['cover'] }}" alt="">
                        <div class="inss">
                            <a href="{{ url('course').'/'.$course['id'] }}"><h4>{{ $course['title'] }}</h4></a>
                            <h5>{{ $course['topic']['topic'] }}</h5>
                            <h6 style="margin-bottom: 135px">{{ $course['educational']['stage'] }}</h6>
                            <div class="author mb-0">
                                <div class="row no-gutters mb-3">
                                    <div class="col-md-1 col-1">
                                        <img class="rounded-circle" src="{{ $course['teacher']['some_detail']['photo'] }}" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>{{ $course['teacher']['some_detail']['fullname'] }}</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                                @if ($course['has_taken'])
                                <button type="button" type="button" class="quit" onclick="statusCourse({{ $course['id'] }}, 'quit')">QUIT COURSE</button>
                                @else
                                <button type="button" type="button" class="take" onclick="statusCourse({{ $course['id'] }}, 'take')">TAKE COURSE</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                    
                @endforelse
            </div>
        </div>
        {{-- <button class="loads mb-5">LOAD MORE</button> --}}
    </div>
</section>

<div class="modal fade" id="ModalTake" tabindex="-1" role="dialog" aria-labelledby="ModalTakeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to take this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalQuit" tabindex="-1" role="dialog" aria-labelledby="ModalQuitTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to quit this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var idCourse = 1;
    var stateCourse = 1;

    function statusCourse(id, status) {
        idCourse = id;
        stateCourse = status;
        if (status == 'quit') $('#ModalQuit').modal('show');
        else $('#ModalTake').modal('show');
    }

    $('.confirms').click(function () {
        var button = $(this);
        let url = "{{ url('takeCourse').'/'}}"+idCourse;
        if (stateCourse == 'quit') url = "{{ url('quitCourse').'/'}}"+idCourse;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            complete: function(response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    })
</script>
@endsection