<section class="cover-all dets-course">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>{{ $course['title'] }}</h4>
                    <div class="ins" style="max-width: 100%">
                        <div class="row">
                            <div class="col-md-6 col-6">
                                <p>By : {{ $course['teacher']['some_detail']['fullname'] }}</p>
                            </div>
                            <div class="col-md-6 col-6">
                                <p><span><img src="{{ asset('assets/img/Groupsss.png') }}" alt=""></span>{{ $course['topic']['topic'] }}</p>
                            </div>
                        </div>
                    </div>
                    {{-- <h5 style="font-size: 13px;">Last Edited : {{ $course['updated_at'] }}</h5> --}}
                </div>
            </div>
        </div>
    </div>
</section>