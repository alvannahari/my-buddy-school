<div class="box-contents-right taken">
    <img src="{{ $course['cover'] }}" alt="">
    <div class="inss">
        <div class="above-imgs">
            <div class="row no-gutters pb-3">
                <div class="col-md-4 col-4">
                    <h6>Grades</h6>
                </div>
                <div class="col-md-8 col-8">
                    <h5>: {{ $course['educational']['stage'] }}</h5>
                </div>
            </div>
            <div class="row no-gutters pb-3">
                <div class="col-md-4 col-4">
                    <h6>Categories</h6>
                </div>
                <div class="col-md-8 col-8">
                    <h5>: {{ $course['topic']['category']['category'] }}</h5>
                </div>
            </div>
            <div class="row no-gutters pb-3">
                <div class="col-md-4 col-4">
                    <h6>Class</h6>
                </div>
                <div class="col-md-8 col-8">
                    <h5>: {{ $course['topic']['topic'] }}</h5>
                </div>
            </div>
        </div>
        <div class="author">
            <div class="row no-gutters">
                <div class="col-md-1 col-1">
                    <img class="rounded-circle" src="{{ $course['teacher']['some_detail']['photo'] }}" alt="" width="45">
                </div>
                <div class="col-md-9 col-9">
                    <div class="texts">
                        <h3>{{ $course['teacher']['some_detail']['fullname'] }}</h3>
                        <p>Tutor</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="after-author">
            <ul class="collapse-menu">
                <li>
                    <button class="btn-toggle-collapse">
                    <a href="{{ url('course').'/'.$course['id'] }}" class="active d-flex align-items-center justify-content-between">
                        <div class="menu d-flex align-items-center">
                            <span class="mr-2">
                                <img src="{{ asset('assets/img/11.png') }}" alt="">
                            </span>
                            Menu
                        </div>
                            <i class="fas fa-chevron-down"></i>
                        </a>
                    </button>
                </li>
                @if (request()->segment(1) == 'theory')
                    @foreach ($allTheory as $item)
                    <li>
                        @if (request()->segment(2) == $item['id'])
                        <a href="#" class="active">
                        @else
                        <a href="{{ route('theory.show', $item['id']) }}">
                        @endif
                            <span class="mr-2">
                                <img src="{{ asset('assets/img/11.png') }}" alt="">
                            </span>
                            Materi {{ $loop->iteration }}
                        </a>
                    </li>
                    @endforeach
                @else
                    <li>
                        <a href="{{ url('course').'/'.$course['id'] }}" class="{{ request()->segment(1) == 'course' && request()->segment(3) == '' ? 'active' : '' }}">
                            <span class="mr-2">
                                <img src="{{ asset('assets/img/11.png') }}" alt="">
                            </span>
                            Learn Course
                        </a>
                    </li>
                    @if ($course['has_taken'] )
                        <li>
                            <a href="{{ url('course').'/'.$course['id'].'/discussion' }}" class="{{ request()->segment(1) == 'discussion' || request()->segment(3) == 'discussion' ? 'active' : '' }}">
                                <span class="mr-2">
                                    <img src="{{ asset('assets/img/12.png') }}" alt="">
                                </span>
                                Discussion
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('course').'/'.$course['id'].'/quiz' }}" class="{{ request()->segment(1) == 'quiz' || request()->segment(3) == 'quiz' ? 'active' : '' }}">
                                <span class="mr-2">
                                    <img src="{{ asset('assets/img/13.png') }}" alt="">
                                </span>
                                Quiz
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('course').'/'.$course['id'].'/result' }}" class="{{ request()->segment(3) == 'result' ? 'active' : '' }}">
                                <span class="mr-2">
                                    <img src="{{ asset('assets/img/14.png') }}" alt="">
                                </span>
                                Leaderboard
                            </a>
                        </li>
                        <li>
                            <a type="button" class="exampleModalZoomEmpt" id="exampleModalZoomEmpt" data-toggle="modal"
                            data-target="#exampleModalZoomEmpty">
                                <span class="mr-2">
                                    <img src="{{ asset('assets/img/15.png') }}" alt="">
                                </span>
                                Zoom
                            </a>
                        </li>
                    @endif
                @endif
            </ul>
        </div>
        @if (auth()->guard('student')->check())
            @if ($course['has_taken'])
            <button type="button" onclick="statusCourse({{ $course['id'] }}, 'quit')" class="quit">QUIT COURSE</button>
            @else
            <button type="button" onclick="statusCourse({{ $course['id'] }}, 'take')" class="take">TAKE COURSE</button>
            @endif
        @endif
        @if (auth()->guard('teacher')->check())
            @if ($course['is_active'])
            <button type="button" data-toggle="modal" data-target="#modal-state-course" class="quit">DISABLE COURSE</button>
            @else
            <button type="button" data-toggle="modal" data-target="#modal-state-course" class="take">ACTIVATE COURSE</button>
            @endif
        @endif
    </div>
</div>

<div class="modal fade" id="exampleModalZoomEmpty" tabindex="-1" role="dialog" aria-labelledby="exampleModalZoomEmptyTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header mb-3">
                <h5 class="modal-title">Link Zoom</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if ($course['zoom'] != null)
                <a href="{{ $course['zoom'] }}">{{ $course['zoom'] }}</a>
            @else
                <a href="#">https://zoom.us/meetings</a>
            @endif
            <div class="modal-body">
                <div class="inns">
                    <p>Ikuti kelas online bersama Tutor anda.</p>
                    @if ($course['zoom'] != null)
                        <h6><img src="{{ asset('assets/img/bunder.png') }}" alt="">{{ $course['time_schedule'] }}</h6>
                    @else
                        <h6><img src="{{ asset('assets/img/bunder.png') }}" alt="">Maaf kelas online masih belum tersedia</h6>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalTake" tabindex="-1" role="dialog" aria-labelledby="ModalTakeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to take this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms student-state-course">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalQuit" tabindex="-1" role="dialog" aria-labelledby="ModalQuitTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to quit this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms student-state-course">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-state fade show" id="modal-state-course" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                @if ($course['is_active'] == 0)
                <p>Are you sure to activate this course?</p>
                @else
                <p>Are you sure to disable this course?</p>
                @endif
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms" id="btn-state-course">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var idCourse = 1;
    var stateCourse = 1;

    function statusCourse(id, status) {
        idCourse = id;
        stateCourse = status;
        if (status == 'quit') $('#ModalQuit').modal('show');
        else $('#ModalTake').modal('show');
    }

    // var segments = window.location.href.split( '/' );
    // var page = decodeURI(segments[5]);
    // if (page === "undefined") {
    //     page = decodeURI(segments[3]);
    // }
    // var listItem = $('.collapse-menu li a');
    // // console.log(page);

    // if (page === "course") {
    //     listItem.eq(1).addClass('active');
    // } else {
    //     $.each(listItem, function (indexInArray, valueOfElement) { 
    //         // console.log($.trim($(this).text()));
    //         if ($.trim($(this).text()) == capitalizeFirstLetter(page)) {
    //             $(this).addClass('active');
    //             return false;
    //         }
    //     });
    // }

    // function capitalizeFirstLetter(string) {
    //     return string.charAt(0).toUpperCase() + string.slice(1);
    // }

    $('#btn-state-course').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "get",
            url: "{{ url('course').'/'.$course['id'].'/state' }}",
            beforeSend: function() {
                button.html('Memperbarui...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    button.html('Ok');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('.student-state-course').click(function () {
        var button = $(this);
        let url = "{{ url('takeCourse').'/'}}"+idCourse;
        if (stateCourse == 'quit') url = "{{ url('quitCourse').'/'}}"+idCourse;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            complete: function(response) {
                if (!response.error) {
                    // location.reload();
                    window.location.href = '{{ url("course") }}'+'/'+idCourse;
                } else {
                    alert(response.error);
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    })
</script>