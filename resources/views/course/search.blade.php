@extends('template/layout')

@section('content')
<section class="cover-all search">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                <h4>{{ count($result).'  hasil untuk '.'"'.$search.'"' }}</h4>
                    <p>Temukan kursus yang anda inginkan di My Buddy School</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
        </div>
    </div>
</section>

<section class="contents-courses searchs pb-5 pt-3">
    <div class="prelative container">
        <div class="row">
            {{-- <div class="col-md-12">
                <div class="filters d-flex">
                    <div class="btn btn-filters d-flex align-items-center">
                        <i class="fas fa-filter"></i>
                        <p class="ml-1 mb-0">filters</p>
                        <p class="number m-0">(4)</p>
                    </div>
                    <div class="input-group mb-3 most-relevant ml-2 col-4 d-none d-md-flex d-lg-flex d-xl-flex">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">
                                <i class="fas fa-filter"></i>
                            </label>
                        </div>
                        <select class="custom-select" id="inputGroupSelect01">
                            <option selected>Urutkan A - Z</option>
                            <option value="1">Urutkan A - Z</option>
                            <option value="2">Newest</option>
                        </select>
                    </div>
                    <button class="btn btn-hapus-filter d-none d-md-flex d-lg-flex d-xl-flex">hapus filter</button>
                    <p class="ml-auto">0 hasil</p>
                </div>
            </div> --}}
            <div class="col-md-3">
                <div class="filters-categorys">
                    <div class="card card-filter border-0">
                        <div class="card-header text-bold text-white">
                            GRADE
                        </div>
                        <form method="get" action="{{ url('search') }}">
                            <div class="card-body">
                                <input type="hidden" name="search" value="{{ $search }}">
                                <div class="form-group row">
                                    <label for="grade1" class="col-10 col-form-label">Primary School (SD)</label>
                                    <div class="col-2 d-flex align-items-center justify-content-center">
                                        <input class="p-0 m-0 form-check-input" type="checkbox" id="grade1" name="grade['primary_school']" value="1">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="grade2" class="col-10 col-form-label">Junior High School (SMP)</label>
                                    <div class="col-2 d-flex align-items-center justify-content-center">
                                        <input class="p-0 m-0 form-check-input" type="checkbox" id="grade2" name="grade['junior_high_school']" value="2">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="grade3" class="col-10 col-form-label">Senior High School (SMA)</label>
                                    <div class="col-2 d-flex align-items-center justify-content-center">
                                        <input class="p-0 m-0 form-check-input" type="checkbox" id="grade3" name="grade['senior_high_school']" value="3">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="grade4" class="col-10 col-form-label">Higher School (PT)</label>
                                    <div class="col-2 d-flex align-items-center justify-content-center">
                                        <input class="p-0 m-0 form-check-input" type="checkbox" id="grade4" name="grade['higher_school']" value="4">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="grade5" class="col-10 col-form-label">Other</label>
                                    <div class="col-2 d-flex align-items-center justify-content-center">
                                        <input class="p-0 m-0 form-check-input" type="checkbox" id="grade5" name="grade['others']" value="5">
                                    </div>
                                </div>
                            </div>
                            <div class="card-header rounded-0 text-bold text-white">
                                CATEGORI
                            </div>
                            <div class="card-body">
                                    <div class="form-group row">
                                        <label for="category1" class="col-10 col-form-label">Lesson</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="category1" name="category['lesson']" value="1">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="category2" class="col-10 col-form-label">Religion</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="category2" name="category['softskill']" value="2">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="category3" class="col-10 col-form-label">Softskills</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="category3" name="category['pra_vocational']" value="3">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="category4" class="col-10 col-form-label">Pedagogics</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="category4" name="category['others']" value="4">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="category5" class="col-10 col-form-label">Other</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="category5" name="category['religion']" value="5">
                                        </div>
                                    </div>
                            </div>
                            {{-- <div class="card-header rounded-0 text-bold text-white d-block d-sm-none">
                                SORT
                            </div>
                            <div class="card-body d-block d-sm-none">
                                <form>
                                    <div class="form-group row">
                                        <label for="sort" class="col-10 col-form-label">Urutkan A - Z</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="sort" name="sort">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sort" class="col-10 col-form-label">News</label>
                                        <div class="col-2 d-flex align-items-center justify-content-center">
                                            <input class="p-0 m-0 form-check-input" type="checkbox" id="sort" name="sort">
                                        </div>
                                    </div>
                                </form>
                            </div> --}}
                            <div class="card-body">
                                <button class="btn-submit" type="submit">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="courses">
                    <div class="row no-gutters pb-5">
                        @foreach ($result as $item)
                            <div class="col-md-4">
                                <div class="box-content mb-3">
                                    <img src="{{ $item['cover'] }}" alt="">
                                    <div class="inss">
                                        <a href="{{ url('course').'/'.$item['id'] }}"><h4>{{ $item['title'] }}</h4></a>
                                        <h5>{{ $item['topic']['topic'] }}</h5>
                                        <h6 style="margin-bottom: 135px">{{ $item['educational']['stage'] }}</h6>
                                        <div class="author mb-0">
                                            <div class="row no-gutters mb-3">
                                                <div class="col-md-1">
                                                    <img class="rounded-circle" src="{{ $item['teacher']['some_detail']['photo'] }}" alt="" width="45">
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="texts">
                                                        <h3>{{ $item['teacher']['some_detail']['fullname'] }}</h3>
                                                        <p>Tutor</p>
                                                    </div>
                                                </div>
                                            </div>
                                            @if ($item['has_taken'])
                                                <button type="button" type="button" class="quit" onclick="statusCourse({{ $item['id'] }}, 'quit')">QUIT COURSE</button>
                                            @else
                                                <button type="button" class="take" onclick="statusCourse({{ $item['id'] }}, 'take')">TAKE COURSE</button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{-- <button class="loads mb-5">LOAD MORE</button> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="ModalTake" tabindex="-1" role="dialog" aria-labelledby="ModalTakeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to take this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalQuit" tabindex="-1" role="dialog" aria-labelledby="ModalQuitTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to quit this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var idCourse = 1;
    var stateCourse = 1;

    $('.input-search').val('{{ $search }}');

    function statusCourse(id, status) {
        idCourse = id;
        stateCourse = status;
        if (status == 'quit') $('#ModalQuit').modal('show');
        else $('#ModalTake').modal('show');
    }

    $('.confirms').click(function () {
        var button = $(this);
        let url = "{{ url('takeCourse').'/'}}"+idCourse;
        if (stateCourse == 'quit') url = "{{ url('quitCourse').'/'}}"+idCourse;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            complete: function(response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    })

</script>
@endsection