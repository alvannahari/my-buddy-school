@extends('template/layout')

@section('content')
@include('course.component.banner')

<section class="courses-insides leads-backs">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content leaderboard">
                        <div class="tables">
                            <div class="titles">
                                <h5>Ranking List</h5>
                            </div>
                            @forelse ($result['results'] as $result)
                                <div class="inns">
                                    <div class="row no-gutters">
                                        <div class="col-md-1 col-1">
                                            <div class="numbers">
                                                @if ($loop->iteration < 4)
                                                    @if ($loop->iteration == 1)
                                                    <img class="numbs" src="{{ asset('assets/img/111.png') }}" alt="">
                                                    @endif
                                                    @if ($loop->iteration == 2)
                                                    <img class="numbs" src="{{ asset('assets/img/112.png') }}" alt="">
                                                    @endif
                                                    @if ($loop->iteration == 3)
                                                    <img class="numbs" src="{{ asset('assets/img/113.png') }}" alt="">
                                                    @endif
                                                @else
                                                <p>{{ $loop->iteration }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-7 col-7">
                                            <div class="names">
                                                <img class="name rounded-circle" src="{{ $result['data']['some_detail']['photo'] }}" alt="">
                                                <p>{{ $result['data']['some_detail']['fullname'] }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-2">
                                            <div class="quiz">
                                                <p>{{ $result['count_quiz'] }} Quiz</p>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-2">
                                            <div class="point">
                                                <p>{{ $result['score'] }} Poin</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <h5>Belum ada pengguna yang menyelesaikan Kuis di Kursus Ini</h5>
                            @endforelse
                        </div>
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>

@endsection