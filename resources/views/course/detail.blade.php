@extends('template/layout')

@section('content')
@include('course.component.banner')

<section class="courses-insides taken-backs">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content taken">
                        @if (auth()->guard('teacher')->check())
                        <ul class="nav justify-content-end">
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Option
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modal-form-course" >
                                        <img class="mr-2" src="{{ asset('assets/img/pencil.png') }}" alt="" width="18">
                                        Edit
                                    </a>
                                    <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal-zoom-course">
                                        <i class="fa fa-alarm-clock mr-2"></i>
                                        Zoom
                                    </a>
                                    <a class="dropdown-item" href="#"  type="button" data-toggle="modal" data-target="#modal-delete-course">
                                        <img class="mr-2" src="{{ asset('assets/img/trash.png') }}" alt="" width="18">
                                        Delete
                                    </a>
                                </div>
                            </div>
                        </ul>
                        @endif
                        <div class="overview">
                            <h6>Overview</h6>
                            <p>{{ $course['description'] }}</p>
                        </div>
                        <div class="content-desc">
                            <h5>Content Description</h5>
                            <div class="content">
                                <div class="list-content">
                                    @forelse ($course['theories'] as $theory)
                                        <div class="item mb-2 d-flex align-items-center">
                                            <a class="ml-0" href="{{ url('theory').'/'.$theory['id'] }}" style="background: none;">
                                                <p class="title mr-2">Materi {{ $loop->iteration }} -</p>
                                                <p class="after-topic">{{ $theory['title'] }}</p>
                                                @if (auth()->guard('teacher')->check())
                                                <a href="{{ url('theory').'/'.$course['id'].'/form/'.$theory['id'] }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a type="button" onclick="deleteTheory({{ $theory['id'] }})" style="margin-left:0">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                                @endif
                                            </a>
                                        </div>
                                    @empty
                                    <div class="item mb-2 d-flex align-items-center">
                                        <p class="title mr-2"></p>
                                        <p class="after-topic">Tidak Ada Materi tersedia</p>
                                    </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        @if (auth()->guard('teacher')->check())
                        <div class="loads col-md-12">
                            <a href="{{ url('theory').'/'.$course['id'].'/form' }}"><button class="mb-5" >Add Materi</button></a>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>

{{-- modal edit course --}}
<div class="modal fade show" id="modal-form-course" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-course">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Isi Judul" name="title" value="{{ $course['title'] }}">  
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea rows="5" class="form-control" placeholder="Isi Deskripsi" name="description">{{ $course['description'] }}</textarea>
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Cover</label><span class="ml-2" style="font-size:11px">*Jika tidak ingin mengubah cover bisa dikosongi</span>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="cover">
                        <span class="error"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="keluar" data-dismiss="modal">Cancel</button>
                <button type="button" class="simpan" id="btn-update-course">Simpan</button>
            </div>
        </div>
    </div>
</div>

{{-- modal add zoom --}}
<div class="modal modal-state fade show" id="modal-zoom-course" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <form id="form-zoom-course">
                    @csrf
                    <div class="form-group">
                        <label>Link Zoom</label>
                        <input type="text" class="form-control" placeholder="ex : https://zoom.us/8euuY73" name="zoom" value="{{ $course['zoom'] }}" autocomplete="off"> 
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>Schedule</label>
                        <input type="datetime-local" class="form-control" placeholder="" name="schedule" value="{{ $course['schedule'] }}" autocomplete="off">   
                        <span class="error"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms" id="btn-zoom-course">OK</button>
            </div>
        </div>
    </div>
</div>

{{-- modal delete course --}}
<div class="modal modal-delete fade show" id="modal-delete-course" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms" id="btn-delete-course">OK</button>
            </div>
        </div>
    </div>
</div>

{{-- modal delete theory --}}
<div class="modal modal-delete fade show" id="modal-delete-theory" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this theory?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms" id="btn-delete-theory">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
    var theory_id = 0;

    $('#btn-update-course').click(function () {
        let button = $(this);
        emptyFormInput();
        var formData = new FormData($('#form-edit-course')[0]);

        $.ajax({    
            type: "post",
            url: "{{ url('course').'/'.$course['id'] }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function(){
                button.html('Menyimpan Data ...');
                button.prop('disabled', true);
            },
            success : function(response) {
                // console.log(response);
                if (!response.error) {
                    location.reload();
                } else {
                    for (let key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                    button.html('Simpan');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('#btn-zoom-course').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "post",
            data: $('#form-zoom-course').serialize(),
            url: "{{ url('course').'/'.$course['id'].'/zoom' }}",
            beforeSend: function() {
                button.html('Memperbarui...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    button.html('Ok');
                    button.prop('disabled', false);
                }
            }
        });
    });

    $('#btn-delete-course').click(function (e) { 
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "get",
            url: "{{ url('course').'/'.$course['id'].'/delete' }}",
            beforeSend: function() {
                button.html('Menghapus...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    window.location = "{{ url('myCourse') }}";
                } else {
                    button.html('Ok');
                    button.prop('disabled', false);
                }
            }
        });
    });

    function deleteTheory(id) {
        theory_id = id;
        $('#modal-delete-theory').modal('show');
    }

    $('#btn-delete-theory').click(function(e) {
        e.preventDefault();
        var button = $(this);

        $.ajax({
            type: "get",
            url: "{{ url('theory').'/'}}"+theory_id+'/delete',
            beforeSend: function() {
                button.html('Menghapus...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload();
                } else {
                    button.html('Ok');
                    button.prop('disabled', false);
                }
            }
        });
    });
</script>


{{-- <section class="courses-insides-sec-2">
    <div class="prelative container">
        <div class="more-courses">
            <div class="titles">
                <h4>More Courses by Hayley Williams</h4>
            </div>
            <div class="row">
                <div class="col-md-3 col-6">
                    <div class="box-content mb-3">
                        <img src="{{ asset('assets/img/Frames4.jpg') }}" alt="">
                        <div class="inss">
                            <h4>Proses Fotosintesis pada Tanaman</h4>
                            <h5>ENGLISH LANGUAGE</h5>
                            <h6>Primary School (SD)</h6>
                            <div class="author">
                                <div class="row no-gutters">
                                    <div class="col-md-1 col-1">
                                        <img src="{{ asset('assets/img/35x35.png') }}" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>Hayley Williams</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" data-toggle="modal" data-target="#ModalTake" class="take">TAKE
                                COURSE</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="box-content mb-3">
                        <img src="{{ asset('assets/img/Frames4.jpg') }}" alt="">
                        <div class="inss">
                            <h4>Proses Fotosintesis pada Tanaman</h4>
                            <h5>ENGLISH LANGUAGE</h5>
                            <h6>Primary School (SD)</h6>
                            <div class="author">
                                <div class="row no-gutters">
                                    <div class="col-md-1 col-1">
                                        <img src="{{ asset('assets/img/35x35.png') }}" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>Hayley Williams</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" data-toggle="modal" data-target="#ModalTake" class="take">TAKE
                                COURSE</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="box-content mb-3">
                        <img src="{{ asset('assets/img/Frames4.jpg') }}" alt="">
                        <div class="inss">
                            <h4>Proses Fotosintesis pada Tanaman</h4>
                            <h5>ENGLISH LANGUAGE</h5>
                            <h6>Primary School (SD)</h6>
                            <div class="author">
                                <div class="row no-gutters">
                                    <div class="col-md-1 col-1">
                                        <img src="{{ asset('assets/img/35x35.png') }}" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>Hayley Williams</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" data-toggle="modal" data-target="#ModalTake" class="take">TAKE
                                COURSE</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-6">
                    <div class="box-content mb-3">
                        <img src="{{ asset('assets/img/Frames4.jpg') }}" alt="">
                        <div class="inss">
                            <h4>Proses Fotosintesis pada Tanaman</h4>
                            <h5>ENGLISH LANGUAGE</h5>
                            <h6>Primary School (SD)</h6>
                            <div class="author">
                                <div class="row no-gutters">
                                    <div class="col-md-1 col-1">
                                        <img src="{{ asset('assets/img/35x35.png') }}" alt="">
                                    </div>
                                    <div class="col-md-9 col-9">
                                        <div class="texts">
                                            <h3>Hayley Williams</h3>
                                            <p>Tutor</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" data-toggle="modal" data-target="#ModalTake" class="take">TAKE
                                COURSE</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="loads mb-5">LOAD MORE</button>
                </div>
            </div>
        </div>
    </div>
</section> --}}

@endsection