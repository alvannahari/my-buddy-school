@extends('template/layout')

@section('content')
@include('course.component.banner')

<section class="courses-insides all-sec">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content discussions">
                        <div class="overview">
                            <h6>Discussion</h6>
                            <p>Diskusikan bersama dengan guru dan siswa lainnya untuk mempermudah proses belajar anda.</p>
                        </div>
                        @if (auth()->guard('student')->check())
                        <form class="disc-ask mb-3" id="form-create-discussion">
                            @csrf
                            <input type="text" class="title" placeholder="Title..." name="title">
                            <span class="is-invalid" style="color: red;display:none"></span>
                            <textarea type="text" class="write" placeholder="Write down your discussion" rows="5" style="width: 100%;" name="discussion"></textarea>
                            <button class="posts" id="btn-submit-post">POST</button>
                        </form>
                        @endif
                        @forelse ($course['discussions'] as $discussion)
                        <div class="discussion-box mb-2">
                            <div class="inners">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="titles">
                                            <ul>
                                                <li>
                                                    <div class="images">
                                                        <img class="rounded-circle" src="{{ $discussion['student']['some_detail']['photo'] }}" alt="" width="50">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="conts">
                                                        <h4>{{ $discussion['student']['some_detail']['fullname'] }}</h4>
                                                        <h6>Student</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="date">
                                                        <p>{{ $discussion['updated_at'] }}</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="content">
                                            <a href="{{ url('discussion').'/'.$discussion['id'] }}">
                                            <h5>{{ $discussion['title'] }}</h5>
                                            <p>{{ $discussion['discussion'] }}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="responses">
                                <div class="inst">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion" id="faq">
                                                    <div class="card-header" id="faqhead2">
                                                        <a href="{{ url('discussion').'/'.$discussion['id'] }}" class="btn btn-header-link collapsed"
                                                            aria-expanded="true" aria-controls="faq2">Rensponses
                                                            ({{ $discussion['comments_count'] }})</a>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                            <b><h4>Belum Ada Diskusi di Dalam Kursus Ini</h4></b>
                        @endforelse
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#btn-submit-post').click(function (e) { 
        e.preventDefault();
        button = $(this);
        
        $.ajax({
            type: "post",
            url: "{{ url('discussion').'/'.$course['id'] }}",
            data: $('#form-create-discussion').serialize(),
            beforeSend : function () {
                button.html('...');
                button.prop('disabled', true);
            },
            success : function(data) {
                if (!data.error) {
                    location.reload();
                } else {
                    for (var key of Object.keys(data.error)) {
                        $('.is-invalid').css('display','block');
                        $('.is-invalid').html(data.error[key]);
                    }
                    alert('Terjadi Kesalahan !!!');
                }
            },
            complete:function(data) {
                button.html('POST');
                button.prop('disabled', false);
            }
        });
    });
</script>
@endsection