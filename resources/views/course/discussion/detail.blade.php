@extends('template/layout')

@section('content')
@include('course.component.banner')

<section class="courses-insides all-sec">
    <div class="prelative container">
        <div class="inner taken">
            <div class="row">
                <div class="col-md-8 order-md-1 order-2">
                    <div class="box-content discussions">
                        <div class="overview">
                            <h6>Detail Discussion</h6>
                            <p>{{ $discussions['title'] }}</p>
                        </div>
                        <div class="discussion-box">
                            <div class="inners">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="titles">
                                            <ul>
                                                <li>
                                                    <div class="images">
                                                        <img class="rounded-circle" src="{{ $discussions['student']['some_detail']['photo'] }}" alt="" width="50">
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="conts">
                                                        <h4>{{ $discussions['student']['some_detail']['fullname'] }}</h4>
                                                        <h6>Student</h6>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="date">
                                                        <p>{{ $discussions['created_at'] }}</p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="content">
                                            <p>{{ $discussions['discussion'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="responses">
                                <div class="inst">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion" id="faq">
                                                <div class="card">
                                                    <div class="card-header" id="faqhead2">
                                                        <a href="#" class="btn btn-header-link collapsed"
                                                            data-toggle="collapse" data-target="#faq2"
                                                            aria-expanded="true" aria-controls="faq2">Rensponses
                                                            {{ $discussions['comments_count'] }}</a>
                                                    </div>

                                                    <div id="faq2" class="collapse" aria-labelledby="faqhead2"
                                                        data-parent="#faq">
                                                        <div class="card-body">
                                                            @forelse ($discussions['comments'] as $comment)
                                                                <div class="content-inners">
                                                                    <div class="titles">
                                                                        <div class="row no-gutters">
                                                                            <div class="col-md-9 col-8">
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="images">
                                                                                            <img class="rounded-circle" src="{{ $comment['userable']['some_detail']['photo'] }}" alt="" width="50">
                                                                                        </div>
                                                                                    </li>
                                                                                    <li>
                                                                                        <div class="conts">
                                                                                            <h4>{{ $comment['userable']['some_detail']['fullname'] }}</h4>
                                                                                            <h6>{{ $comment['userable_type'] }}</h6>
                                                                                        </div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <div class="col-md-3 col-4">
                                                                                <div class="date">
                                                                                    <p>{{ $comment['created_at'] }}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="answers">
                                                                    <p>{{ $comment['comment'] }}.</p>
                                                                </div>
                                                            @empty
                                                                <b><h5>Belum ada Komentar Tentang Diskusi Ini</h5></b>
                                                            @endforelse
                                                        </div>
                                                        <div class="replys">
                                                            <div class="titles-reply">
                                                                <h6>REPLY</h6>
                                                            </div>
                                                            <div class="area-reply">
                                                                <div class="subs-titles-area">
                                                                    <ul>
                                                                        <li>
                                                                            <div class="images">
                                                                                <img class="rounded-circle" src="{{ $user['some_detail']['photo'] }}" alt="" width="60">
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="conts">
                                                                                <h4>{{ $user['some_detail']['fullname'] }}</h4>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    <form id="form-submit-comments">
                                                                        @csrf
                                                                        <textarea name="comment" autocomplete="off" placeholder="Write something about this discussion ....."> </textarea>
                                                                        <span class="error" style="color: red;display:none"></span>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <div class="posts">
                                                                <button class="posts" id="btn-submit-comment">POST</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 order-md-2 order-1">
                    @include('course.component.navigation')
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#btn-submit-comment').click(function (e) { 
        e.preventDefault();
        button = $(this);
        
        $.ajax({
            type: "post",
            url: "{{ url('discussion').'/'.$discussions['id'].'/submit' }}",
            data: $('#form-submit-comments').serialize(),
            beforeSend : function () {
                button.html('...');
                button.prop('disabled', true);
            },
            success : function(data) {
                if (!data.error) {
                    location.reload();
                } else {
                    for (var key of Object.keys(data.error)) {
                        $('.error').css('display','block');
                        $('.error').html(data.error[key]);
                    }
                    alert('Terjadi Kesalahan !!!');
                }
            },
            complete:function(data) {
                button.html('POST');
                button.prop('disabled', false);
            }
        });
    });
</script>
@endsection

