@extends('template/layout')

@section('content')
<section class="cover-all contact-abouts">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>Contact Us</h4>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            {{-- <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div> --}}
        </div>
    </div>
</section>

<section class="contact-about-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-4">
                <div class="row no-gutters">
                    <div class="col-md-2 col-2">
                        <div class="icons"><img src="{{ asset('assets/img/locs.png') }}" alt=""></div>
                    </div>
                    <div class="col-md-10 col-10">
                        <div class="txt">
                            <h6>Address</h6>
                            <p>Jl. Danau Sentani Dalam H1 L4 RT 04 RW 08 Kel. Madyopuro Kec. Kedung Kandang Kota
                                Malang 65139</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row no-gutters">
                    <div class="col-md-2 col-2">
                        <div class="icons"><img src="{{ asset('assets/img/locs2.png') }}" alt=""></div>
                    </div>
                    <div class="col-md-10 col-10">
                        <div class="txt">
                            <h6>Telephone</h6>
                            <p>(0341) 368639 (Jam Kerja)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row no-gutters">
                    <div class="col-md-2 col-2">
                        <div class="icons"><img src="{{ asset('assets/img/locs3.png') }}" alt=""></div>
                    </div>
                    <div class="col-md-10 col-10">
                        <div class="txt">
                            <h6>Email</h6>
                            <p>mybuddyschool@gmail.com</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="maps">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.9436445420833!2d106.73099471476874!3d-6.138273795555664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d57d9978215%3A0x36e9ef4e56eedc16!2sPT.%20KRAKATOA%20STUDIO%20GEMILANG!5e0!3m2!1sid!2sid!4v1601366315001!5m2!1sid!2sid"
                        width="100%" height="524" frameborder="0" style="border:0;" allowfullscreen=""
                        aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection