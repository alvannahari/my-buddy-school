@extends('template.layout')

@section('content')
<section class="cover-all covers-dets">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>Detail Tutor</h4>
                    <p>Informasi Tutor Kursus Team My Buddy School.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- <div class="image"><img src="../../assets/img/MaskGroups.png" alt=""></div> -->
        </div>
    </div>
</section>


<section class="contents-courses details-tutors pb-5">
    <div class="breadcrumbs">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="padding: 0">
            </ol>
        </nav>
    </div>
    <div class="prelative container">
        <div class="courses grade-details-tutors">
            <div class="detail-tutors-content">
                <div class="context-tops">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-lefts">
                                <div class="inns">
                                <img class="rounded-circle" src="{{ $tutor['detail']['photo'] }}" alt="" width="120" height="120">
                                    <div class="profile">
                                        <h5>{{ $tutor['detail']['fullname'] }}</h5>
                                        <h6>Tutor</h6>
                                    </div>
                                    
                                </div>
                                <div class="sends">
                                    <p class="d-sm-none d-md-block mt-3" style="color: rgb(247, 155, 36)">Shared {{ $tutor['courses_count'] }}</p>
                                    {{-- <a href="#">
                                    <img src="../../assets/img/email1.png" alt="">
                                    </a> --}}
                                </div>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <div class="box-right">
                                <form action="#">
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label> Nama Lengkap</label>
                                            <input type="text" class="form-control" 
                                            placeholder="SMA LABOLATORIUM UM" disabled value="{{ $tutor['detail']['fullname'] }}">
                                        </div>
                                        <div class="col-md-12">
                                            <label> Address</label>
                                            <input type="text" class="form-control" 
                                                placeholder="Jl. Bromo No.16, Klojen, Kota Malang, Jawa Timur 65119" disabled value="{{ $tutor['detail']['address'].', '.$tutor['detail']['city'].', '.$tutor['detail']['province'] }}">
                                        </div>
                                        <div class="col-md-12">
                                            <label> Member Since</label>
                                            <input type="text" class="form-control" 
                                            placeholder="09/07/2018" disabled value="{{ $tutor['detail']['created_at'] }}">
                                        </div>
                                    </div>
                                </form>
                                <div class="bottoms" style="margin-top: 4px">
                                    <div class="row justify-content-center">
                                        <div class="col-md-4 col-2">
                                            <div class="context">     
                                                <a href="{{ $tutor['detail']['facebook'] }}">
                                                    <img src="{{ asset('assets/img/fb.png') }}" alt="">
                                                    <p>Facebook</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-2">
                                            <div class="context">
                                                <a href="{{ $tutor['detail']['twitter'] }}">
                                                    <img src="{{ asset('assets/img/twitter.png') }}" alt="">
                                                    <p>Twitter</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-2">
                                            <div class="context">       
                                                <a href="{{ $tutor['detail']['instagram'] }}">
                                                    <img src="{{ asset('assets/img/yt.png') }}" alt="">
                                                    <p>Youtube</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="courses">
                    <div class="titels">
                        <h6>Teaching</h6>
                        <p>Courses / Teach</p>
                    </div>
                    <div class="row no-gutters pb-5">
                        @forelse ($tutor['courses'] as $course)
                        <div class="col-md-4 col-6">
                            <div class="box-content mb-3">
                                <img src="{{ $course['cover'] }}" alt="">
                                <div class="inss">
                                    <h4>{{ $course['title'] }}</h4>
                                    <h5>{{ $course['topic']['topic'] }}</h5>
                                    <h6 style="margin-bottom: 135px;">{{ $course['educational']['stage'] }}</h6>
                                    <div class="author mb-0">
                                        <div class="row no-gutters mb-3">
                                            <div class="col-md-1 col-1">
                                                <img class="rounded-circle" src="{{ $tutor['detail']['photo'] }}" alt="">
                                            </div>
                                            <div class="col-md-9 col-9">
                                                <div class="texts">
                                                    <h3>{{ $tutor['detail']['fullname'] }}</h3>
                                                    <p>Tutor</p>
                                                </div>
                                            </div>
                                        </div>
                                        @if ($course['has_taken'])
                                            <button type="button" type="button" class="quit" onclick="statusCourse({{ $course['id'] }}, 'quit')">QUIT COURSE</button>
                                        @else
                                            <button type="button" class="take" onclick="statusCourse({{ $course['id'] }}, 'take')">TAKE COURSE</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        <div class="col-md-4 col-6">
                            <div class="box-content mb-3">
                                <img src="{{ asset('assets/img/Frames4.jpg') }}" alt="">
                                <div class="inss">
                                    <h4>Proses Fotosintesis pada Tanaman</h4>
                                    <h5>ENGLISH LANGUAGE</h5>
                                    <h6>Primary School (SD)</h6>
                                    <div class="author">
                                        <div class="row no-gutters">
                                            <div class="col-md-1 col-1">
                                                <img src="{{ asset('assets/img/35x35.png') }}" alt="">
                                            </div>
                                            <div class="col-md-9 col-9">
                                                <div class="texts">
                                                    <h3>Hayley Williams</h3>
                                                    <p>Tutor</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" data-toggle="modal" data-target="#ModalQuit" type="button" data-toggle="modal" data-target="#ModalQuit" class="quit">QUIT COURSE</button>
                                </div>
                            </div>
                        </div>
                        @endforelse
                    </div>
                    {{-- <button class="loads mb-5">LOAD MORE</button> --}}
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="ModalTake" tabindex="-1" role="dialog" aria-labelledby="ModalTakeTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to take this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalQuit" tabindex="-1" role="dialog" aria-labelledby="ModalQuitTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirmation</h5>
            </div>
            <div class="modal-body">
                <p>Are you sure to quit this course?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="confirms">OK</button>
            </div>
        </div>
    </div>
</div>
<script>
    var idCourse = 1;
    var stateCourse = 1;

    function statusCourse(id, status) {
        idCourse = id;
        stateCourse = status;
        if (status == 'quit') $('#ModalQuit').modal('show');
        else $('#ModalTake').modal('show');
    }

    $('.confirms').click(function () {
        var button = $(this);
        let url = "{{ url('takeCourse').'/'}}"+idCourse;
        if (stateCourse == 'quit') url = "{{ url('quitCourse').'/'}}"+idCourse;
        $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            beforeSend: function() {
                button.html('Menyimpan...');
                button.prop('disable', true);
            },
            complete: function(response) {
                if (!response.error) {
                    location.reload();
                } else {
                    alert(response.error);
                    button.html('Simpan');
                    button.prop('disable', false);
                }
            }
        });
    })
</script>
@endsection