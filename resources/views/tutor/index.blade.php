@extends('template.layout')

@section('content')
<section class="cover-all">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>All Tutor </h4>
                    <p>Daftar Seluruh Tutor dalam Team My Buddy School.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div>
        </div>
    </div>
</section>


<section class="contents-courses pb-5">
    {{-- <div class="breadcrumbs">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Course</a></li>
                <li class="breadcrumb-item"><a href="#">Senior High School</a></li>
                <li class="breadcrumb-item"><a href="#">Mitra</a></li>
                <li class="breadcrumb-item active" aria-current="page">All</li>
            </ol>
        </nav>
    </div> --}}
    <div class="prelative container">
        <div class="courses grade-all-tutors pt-4">
            <div class="tutor-bss">
                <div class="tops">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Tutor BSS</h5>
                        </div>
                        <div class="col-md-6">
                            <!-- <h6>See More</h6> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    @forelse ($data as $tutor)
                    <div class="col-md-2 col-6">
                        <div class="box-tutor">
                            <a href="{{ url('teacher').'/'.$tutor['id'] }}">
                                <img class="rounded-circle" src="{{ $tutor['detail']['photo'] }}" alt="" width="142" height="142">
                                <h5>{{ $tutor['detail']['fullname'] }}</h5>
                                <h6>Shared {{ $tutor['courses_count'] }} Course</h6>
                            </a>
                        </div>
                    </div>
                    @empty
                    <div class="col-md-2 col-6">
                        <div class="box-tutor">
                            <a href="#">
                                <img src="{{ asset('assets/img/Ellipse 95.png') }}" alt="">
                                <h5>Deri Armandha</h5>
                                <h6>Shared 100 Course</h6>
                            </a>
                        </div>
                    </div>
                    @endforelse
                    {{-- <div class="col-md-12">
                        <button class="loads mb-5 mt-5">LOAD MORE</button>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection