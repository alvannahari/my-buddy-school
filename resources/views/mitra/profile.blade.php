@extends('template.layout')

@section('content')
<section class="cover-all covers-dets">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>Detail Mitra </h4>
                    <p>Seluruh informasi Tentang Pihak Mitra My Buddy School.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div>
        </div>
    </div>
</section>


<section class="contents-courses details-mitra pb-5">
    {{-- <div class="breadcrumbs">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Course</a></li>
                <li class="breadcrumb-item"><a href="#">Senior High School</a></li>
                <li class="breadcrumb-item"><a href="#">Mitra</a></li>
                <li class="breadcrumb-item active" aria-current="page">Profile Mitra</li>
            </ol>
        </nav>
    </div> --}}
    <div class="prelative container">
        <div class="courses grade-details-mitra pt-4">
            <div class="detail-mitra-content">
                <div class="tops-dets">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heads">
                                <h5>Profile </h5>
                                <h5>{{ $mitra['name'] }}</h5>
                                <img class="rounded-circle" src="{{ $mitra['image'] }}" alt="">
                            </div>
                            <form action="#">
                                <div class="container text-center mb-3" >
                                    <a href="{{ url('assets/file/PROFIL SPMI SMA LAB PDF.pdf') }}" target="_blank">Download File Profil Sekolah</a>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <label for="formGroupExampleInput">Company Name</label>
                                        <input type="text" class="form-control" disabled placeholder="SMA LABOLATORIUM UM" value="{{ $mitra['name'] }}">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="formGroupExampleInput">Address</label>
                                        <input type="text" class="form-control" disabled
                                            placeholder="Jl. Bromo No.16, Klojen, Kota Malang, Jawa Timur 65119" value="{{ $mitra['address'] }}">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="formGroupExampleInput">Phone</label>
                                        <input type="text" class="form-control" disabled
                                            placeholder="(0341) 368639" value="{{ $mitra['phone'] }}">
                                    </div>
                                    <div class="col-md-12">
                                        <label for="formGroupExampleInput">Email</label>
                                        <input type="text" class="form-control" disabled
                                            placeholder="smalabum@yahoo.com" value="{{ $mitra['email'] }}">
                                    </div>
                                </div>  
                            </form>
                            <div class="bottoms">
                                <div class="row justify-content-center">
                                    <div class="col-md-3 col-2">
                                        <div class="context">
                                            <img src="{{ asset('assets/img/12507.png') }}" alt="">
                                            <a href="{{ $mitra['website'] }}" {{ $mitra['website'] == '#' ? '' : 'target="_blank"' }}>
                                                <p>Website</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2">
                                        <div class="context">
                                            <img src="{{ asset('assets/img/fb.png') }}" alt="">
                                            <a href="{{ $mitra['facebook'] }}" {{ $mitra['facebook'] == '#' ? '' : 'target="_blank"' }}>
                                                <p>Facebook</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2">
                                        <div class="context">
                                            <img src="{{ asset('assets/img/twitter.png') }}" alt="">
                                            <a href="{{ $mitra['twitter'] }}" {{ $mitra['twitter'] == '#' ? '' : 'target="_blank"' }}>
                                                <p>Twitter</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-2">
                                        <div class="context">
                                            <img src="{{ asset('assets/img/yt.png') }}" alt="">
                                            <a href="{{ $mitra['youtube'] }}" {{ $mitra['youtube'] == '#' ? '' : 'target="_blank"' }}>
                                                <p>Youtube</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="max-width:600px;max-height:450px;width:100%;align-self: center;">
                    <div style="position: relative;width: 100%;height: 0;padding-bottom: 56.25%;">
                        <iframe style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;" width="560" height="315" src="{{ $mitra['video'] }}" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection