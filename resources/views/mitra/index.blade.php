@extends('template.layout')

@section('content')
<section class="cover-all">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>All Mitra </h4>
                    <p>Seluruh Mitra yang Bekerja Sama dengan My Buddy School Team.</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="image"><img src="{{ asset('assets/img/MaskGroups.png') }}" alt=""></div>
        </div>
    </div>
</section>


<section class="contents-courses pb-5">
    {{-- <div class="breadcrumbs">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Course</a></li>
                <li class="breadcrumb-item"><a href="#">Senior High School</a></li>
                <li class="breadcrumb-item"><a href="#">Mitra</a></li>
                <li class="breadcrumb-item active" aria-current="page">All</li>
            </ol>
        </nav>
    </div> --}}
    <div class="prelative container">
        <div class="courses grade-all-mitra pt-4">
            <div class="mitras">
                <div class="tops">
                    <div class="row">
                        <div class="col-md-6">
                            <h5>Mitra</h5>
                        </div>
                        <div class="col-md-6">
                            <!-- <h6>See More</h6> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    @forelse ($mitras as $mitra)
                    <div class="col col-md-auto">
                        <div class="box-mitras">
                            <a href="{{ url('mitra').'/'.$mitra['id'] }}">
                                <img src="{{ $mitra['image'] }}" alt="" width="140" height="140">
                                <h5>{{ $mitra['name'] }}</h5>
                                <h6>{{ $mitra['city'] }}</h6>
                            </a>
                        </div>
                    </div>
                    @empty
                    <div class="col col-md-auto">
                        <div class="box-mitras">
                            <a href="#">
                                <img src="{{ asset('assets/img/LogoPng-279x300.png') }}" alt="">
                                <h5>SMA LABORATORIUM</h5>
                                <h6>Kota Malang</h6>
                            </a>
                        </div>
                    </div>
                    @endforelse
                    {{-- <div class="col-md-12">
                        <button class="loads mb-5 mt-5">LOAD MORE</button>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection