<div class="drawer drawer-left slide" tabindex="-1" role="dialog" aria-labelledby="drawer-1-title" aria-hidden="true" id="drawer-1">
    <div class="drawer-content drawer-content-scrollable" role="document">
        <div class="drawer-header">
            <h4 class="drawer-title" id="drawer-1-title">
                <img src="{{ asset('assets/img/logo-head.png') }}" alt="logo" height="25">
            </h4>
        </div>
        <div class="drawer-body">
            <form class="form-inline my-2 my-lg-0 search-form">
                <div class="input-group w-100">
                    <input type="text" class="form-control input-search" placeholder="Search" name="key">
                    <div class="input-group-append">
                        <button class="btn-search">
                            <img src="{{ asset('assets/img/search.png') }}" alt="">
                        </button>
                    </div>
                </div>
            </form>

            <ul class="navbar-nav border-top mt-2">
                <li class="nav-item">
                    <a href="{{ url('dashboard') }}" class="nav-link text-primary pt-2 pb-2"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('course/allCourse') }}" class="nav-link text-primary pt-2 pb-2"><i class="fas fa-book-open"></i> Course</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('myCourse') }}" class="nav-link text-primary pt-2 pb-2"><i class="fas fa-user-graduate"></i> My Course</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('profile') }}" class="nav-link text-primary pt-2 pb-2"><i class="fas fa-user"></i> Profile</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('inbox') }}" class="nav-link text-primary pt-2 pb-2"><i class="fas fa-comment-alt"></i> Message</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('logout') }}" class="nav-link text-primary pt-2 pb-2"><i class="fas fa-sign-out-alt"></i> Logout</a>
                </li>
            </ul>
        </div>
        <div class="drawer-footer">
            <div class="profile d-flex flex-row align-items-first">
                <div class="img-profile">
                    <img src="{{ asset('assets/img/profile.jpg') }}" alt="profile" class="img-fit rounded">
                </div>
                <div class="detail ml-1">
                    <p class="text-bold">Hi, {{ auth()->guard(session()->get('guard'))->user()->detail->fullname }}</p>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light m-navbar">
    <div class="container headerss">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('assets/img/logo-head.png') }}" alt="logo">
        </a>
        <button class="navbar-toggler" data-toggle="drawer" data-target="#drawer-1">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="d-flex flex-row align-items-center justiy-content-between w-100">
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a href="{{ url('dashboard') }}" class="nav-link text-black d-flex align-items-center">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('myCourse') }}" class="nav-link text-black d-flex align-items-center">My Course</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ url('inbox') }}" class="nav-link text-black d-flex align-items-center">Message<span class="badge badge-pill badge-warning text-white badge-mail" style="padding: .25em;position: relative;top: -11px;left: 5px;">{{ session()->get('inbox_unread') }}</span></a>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link d-flex flex-row align-items-center dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <p class="m-0 text-black">Hi, {{ Str::limit(auth()->guard(session()->get('guard'))->user()->detail->fullname, 20, '...') }}</p>
                            <img src="{{ auth()->guard(session()->get('guard'))->user()->detail->photo }}" alt="profile" class="img-fit ml-1 rounded-circle">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ url('profile') }}">Profile</a>
                            <a class="dropdown-item" href="{{ url('myCourse') }}">My Course</a>
                            <a class="dropdown-item" href="{{ url('inbox') }}">Message</a>
                            <a class="dropdown-item text-danger" href="{{ url('logout') }}">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</nav>