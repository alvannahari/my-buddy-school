<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Buddy School</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('assets/style/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style/bootstrap-drawer.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/style/dist/styles2.css') }}">
    {{-- <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"> --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
</head>
<body>
    @if (auth()->guard('student')->check())
        @include('template.header.student')
        @if (session()->get('check_profile') && request()->segment(1) != 'profile')
            @include('template.alert')
        @endif
    @elseif (auth()->guard('teacher')->check())
        @include('template.header.teacher')
        @if (session()->get('check_profile') && request()->segment(1) != 'profile')
            @include('template.alert')
        @endif
    @else
    <nav class="navbar navbar-expand-lg navbar-light bg-light m-navbar">
        <div class="container headerss">
            <a class="navbar-brand" href="#">
                <img src="{{asset('assets/img/logo-head.png') }}" alt="logo">
            </a>
            <ul class="navbar-nav ml-auto d-flex flex-row">
                <li class="nav-item d-flex align-items-center mr-1">
                    <a href="{{ url('login') }}" class="nav-link btn btn-signin text-white">SIGN IN</a>
                </li>
                <li class="nav-item d-flex align-items-center">
                    <a href="{{ url('register') }}" class="nav-link btn btn-signup">SIGN UP</a>
                </li>
            </ul>
        </div>
    </nav>    
    @endif

    @yield('content')

    <div class="p-5 pref-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-12">
                    <div class="logo mb-4">
                        <img src="{{ asset('assets/img/logo-head.png') }}" alt="logo" height="35">
                    </div>
                    <p class="about-us mb-4">My Buddy School adalah media pembelajaran yang dapat diakses dimana saja dan kapan saja sehingga penggunaan lebih mudah dimengerti.</p>
                    <ul class="social-media d-flex align-items-center p-0">
                        <li class="item">
                            <a href="{{ $setting['youtube'] }}" target="_blank" class="link p-2 d-inline-block"><i class="fab fa-youtube"></i></a>
                        </li>
                        <li class="item ml-2">
                            <a href="{{ $setting['facebook'] }}" target="_blank" class="link p-2 d-inline-block"><i class="fab fa-facebook-square"></i></a>
                        </li>
                        <li class="item ml-2">
                            <a href="{{ $setting['twitter'] }}" target="_blank" class="link p-2 d-inline-block"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li class="item ml-2">
                            <a href="{{ $setting['instagram'] }}" target="_blank" class="link p-2 d-inline-block"><i class="fab fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-12 mt-3">
                    <ul class="footer-link p-0">
                        <li class="item text-bold">My Buddy School</li>
                        <li class="item"><a href="{{ url('aboutUs') }}">About Us</a></li>
                        <li class="item"><a href="{{ url('contactUs') }}">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-12 mt-3">
                    <ul class="footer-link p-0">
                        <li class="item text-bold">Menu</li>
                        <li class="item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="item"><a href="{{ url('course/allCourse') }}">Course</a></li>
                        <li class="item"><a href="{{ url('myCourse') }}">My Course</a></li>
                        <li class="item"><a href="{{ url('inbox') }}">Inbox</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-2 col-12 mt-3">
                    <ul class="footer-link p-0">
                        <li class="item text-bold">For users</li>
                        <li class="item"><a href="{{ url('login') }}">Login</a></li>
                        <li class="item"><a href="{{ url('register') }}">Register</a></li>
                        <li class="item"><a href="{{ url('profile') }}">Account</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer py-2">
        <p class="text-white text-center m-0">© 2020 My Buddy School.</p>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    {{-- <script src="https://kit.fontawesome.com/1d26a38738.js" crossorigin="anonymous"></script> --}}
    <script type="text/javascript" src="{{ asset('assets/js/index.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap-drawer.js') }}"></script>
    <script src="{{ asset('assets/js/header.js') }}"></script>
    <script src="{{ asset('assets/js/footer.js') }}"></script>
    <script src="{{ asset('assets/js/modals.js') }}"></script>
    {{-- <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> --}}
    <script>
        // AOS.init();
        function emptyFormInput() {
            $('.form-control').removeClass('is-invalid');
            $('.error').html('');
        }
    </script>
</body>
</html>