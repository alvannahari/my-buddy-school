<div class="alert d-sm-none d-none d-md-block" role="alert">
    <div class="row justify-content-center">
        <img src="{{ asset('assets/img/warning.png') }}" alt="">
        <p>Please complete your profile data</p>
    </div>
    <div class="hidden">
        <div class="row">
            <div class="button">
                <a href="{{ url('profile') }}"><button class="btn"><span>Change Profile</span></button></a>
            </div>
            <div class="button-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin-top: -13px;">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>      
    </div>
</div>