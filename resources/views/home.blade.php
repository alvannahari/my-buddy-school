@extends('template/layout')

@section('content')
<section class="landing-sec-1">
    <div class="row">
        <div class="col-md-4">
            <div class="inners" id="slogan-height">
                <div class="box-content">
                    <h4 id="slogan-home">Get Access to Unlimited Educational Resources. <strong>Everywhere, Everytime!</strong></h4>
                    @if (auth()->guard('teacher')->check())
                    @else 
                    <a href="{{ url('course/allCourse') }}"><button>Learn Now!</button></a>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="image"><img src="{{ asset('assets/img/slide-path.png') }}" alt="" width="800px" style="margin-top: 10px;"></div>
        </div>
    </div>
</section>

<section class="landing-sec-2">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-4 col-4">
                <div class="box-content">
                    <img src="{{ asset('assets/img/Icons1.png') }}" alt="">
                    <h5>Unlimited Access</h5>
                    <p>Unlimited access to </p>
                </div>
            </div>
            <div class="col-md-4 col-4">
                <div class="box-content">
                    <img src="{{ asset('assets/img/Icons2.png') }}" alt="">
                    <h5>Expert Teachers</h5>
                    <p>Learn from experts who are passionate about teaching</p>
                </div>
            </div>
            <div class="col-md-4 col-4">
                <div class="box-content">
                    <img src="{{ asset('assets/img/Icons3.png') }}" alt="">
                    <h5>Learn Anywhere</h5>
                    <p>Switch between your computer, tablet, or mobile device.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="landing-sec-3">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <h5>Apa itu BSS?</h5>
                    <p>BSS adalah suatu sistem jejaring yang dikembangkan dengan pola kemitraan (partnership) antar
                        sekolah, antar pendidik, dan antar siswa, dan bahkan antar orang tua peserta didik yang
                        ingin belajar untuk memajukan pendidikan anaknya. BSS merupakan suatu sistem, artinya
                        melibatkan banyak komponen untuk berperan sesuai kapasitas dan fungsinya. Melalui BSS ini
                        setiap komponen yang memiliki keunggulan, potensi dan kelebihan, secara sukarela ingin
                        membantu dan berbagi kepada yang lain secara sukarela.</p>
                        <button>Learn More</button>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="landing-sec-4">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-4 col-4">
                <div class="box-content">
                    <img src="{{ asset('assets/img/student-3 1.png') }}" alt="" >

                        <h5>{{ $count['students'] }}</h5>
                    <p>Registered Students</p>
                </div>
            </div>
            <div class="col-md-4 col-4">
                <div class="box-content">
                    <img src="{{ asset('assets/img/teacher-2 1.png') }}" alt="">

                    <h5>{{ $count['teachers'] }}</h5>
                    <p>Registered Tutors</p>
                </div>
            </div>
            <div class="col-md-4 col-4">
                <div class="box-content">
                    <img src="{{ asset('assets/img/solution-2 2.png') }}" alt="">

                    <h5>{{ $count['courses'] }}</h5>
                    @php
                        $cut = strlen($count['courses']) - 1;
                        if ($cut == 0) {
                            $string = '';
                        } else {
                            $string = '0';
                            for ($i=2; $i < strlen($count['courses']); $i++) { 
                                $string .= '0';
                            };
                        }
                    @endphp
                    <p>More than {{ substr($count['courses'], 0, 1).$string }}+ content</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="landing-sec-5">
    <div class="prelative container">
        <div class="title">
            <h5>Meet Our <strong>Tutor</strong></h5>
        </div>
        <div class="row">
            @foreach ($tutors as $tutor)
                <div class="col-md-3 col-6">
                    <div class="box-content">
                        <div class="inners">
                            <img class="rounded-circle" src="{{ $tutor['some_detail']['photo'] }}" alt="" width="180" height="180">
                            <h6>{{ $tutor['some_detail']['fullname'] }}</h6>
                            <p>Shared {{ $tutor['courses_count'] }} Courses</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<script>
    if ($(window).height() < 660) {
        $('#slogan-home').css('font-size','36px');
        $('#slogan-home').css('line-height','52px');
        $('#slogan-height').css('height','80%');
    } else if ($(window).height() < 790) {
        $('#slogan-home').css('font-size','42px');
        $('#slogan-home').css('line-height','58px');
        $('#slogan-height').css('height','90%');
    }
    // alert($(window).height());
</script>
@endsection