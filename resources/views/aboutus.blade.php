@extends('template/layout')

@section('content')
<section class="cover-all contact-abouts">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>About Us</h4>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- <div class="image"><img src="../../assets/img/MaskGroups.png" alt=""></div> -->
        </div>
    </div>
</section>

<section class="contact-about-sec-1">
    <div class="prelative container">
        <div class="row">
            <div class="col-md-12">
                <div class="box-content">
                    <img src="{{ asset('assets/img/abouts.png') }}" alt="">
                    <h5>My Buddy School</h5>
                    <p style="text-align: justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BSS adalah suatu sistem jejaring yang
                        dikembangkan
                        dengan
                        pola
                        kemitraan
                        (partnership)
                        antar sekolah, antar pendidik, dan antar siswa, dan bahkan antar orang tua peserta didik
                        yang ingin belajar untuk memajukan pendidikan anaknya. BSS merupakan suatu sistem, artinya
                        melibatkan banyak komponen untuk berperan sesuai kapasitas dan fungsinya. Melalui BSS ini
                        setiap komponen yang memiliki keunggulan, potensi dan kelebihan, secara sukarela ingin
                        membantu dan berbagi kepada yang lain secara sukarela (relawan). Misalnya siswa yang tinggal
                        di pedesaan, mereka memiliki keunggulan di bidang pertanian, maka secara sukarela berbagi
                        dengan siswa di kota untuk mengajarkan cara bercocok tanam. Sebaliknya, siswa di kota yang
                        memiliki kelebihan uang secara ekonomi, mereka secara sukarela membantu memberi sumbangan
                        alat sekolah kepada siswa yang berkekurangan di desa.</p>
                    <p style="text-align: justify;">Siswa yang memberi bantuan cara bercocok tanam disebut peer teacher atau peer coaching,
                        sedangkan siswa yang belajar dari temannya disebut learner. Jadi belajar dari sebayanya itu
                        disebut peer.</p>
                    <p style="text-align: justify;">Melalui BSS on line, bantuan sukarela (baik pemikiran, dana, alat, dan hal-hal lain yang
                        positif) dari warga sekolah (terutama siswa dan guru) dapat tersalurkan kepada orang lain
                        (siswa dan guru di sekolah lain) yang membutuhkan melalui media online.</p>
                </div>
            </div>
            <div class="col-md-12">
                <div class="mission">
                    <h6>Misi</h6>
                    <p>Kehidupan akan bemakna jika bisa berbagi bersama
                        Bersama kita bisa
                        Ikhlas berbuat agar orang lain mendapat manfaat
                        Jadikan Anda sebagai jalan penunjuk kesuksesan orang lain</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection