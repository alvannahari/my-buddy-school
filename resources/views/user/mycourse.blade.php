@extends('template.layout')

@section('content')
<section class="cover-all my-course" style="height: 160px;">
    <div class="row">
        <div class="col-md-6">
            <div class="inners">
                <div class="box-content">
                    <h4>My Courses</h4>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!-- <div class="image"><img src="../../assets/img/MaskGroups.png" alt=""></div> -->
        </div>
        {{-- <div class="col-md-12">
            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="pills-courses-tab" data-toggle="pill" href="#pills-courses"
                        role="tab" aria-controls="pills-courses" aria-selected="true">Courses</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-ongoing-tab" data-toggle="pill" href="#pills-ongoing" role="tab"
                        aria-controls="pills-ongoing" aria-selected="false">On Going</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-evertaken-tab" data-toggle="pill" href="#pills-evertaken"
                        role="tab" aria-controls="pills-evertaken" aria-selected="false">Ever Taken</a>
                </li>
            </ul>
        </div> --}}
    </div>
</section>

<section class="contents-courses pb-4 pt-4">
    <div class="prelative container">
        <div class="courses">
            <div class="row">
                {{-- filter category --}}
                <div class="col-md-3">
                    <div class="card sidebar border-0 mb-3 mt-0">
                        <ul class="list-group list-group-flush" id="list-menu-sidebar">
                            <li class="list-group-item d-flex p-0">
                                <a href="{{ url('myCourse') }}" class="d-flex align-items-center link-all">
                                    <img src="{{ asset('assets/img/world.svg') }}" alt="world">
                                    <p class="mb-0 ml-2 text-bold">ALL</p>
                                </a>
                                <button class="btn-toggle-sidebar btn-show show">
                                    <i class="fal fa-chevron-down"></i>
                                </button>
                                <button class="btn-toggle-sidebar btn-hide">
                                    <i class="fas fa-times"></i>
                                </button>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/book.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">LESSON</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('myCourse/Natural Science') }}">Natural Science</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Mathematic') }}">Mathematic</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/English Language') }}">English Language</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Indonesian Language') }}">Indonesian Languange</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Social Science') }}">Social Science</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Vernacular') }}">Vernacular</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Civilization') }}">Civilization</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/world.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">SOFTSKILL</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('myCourse/Communication') }}">Communication</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Social Skill') }}">Social Skill</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Personal Skill') }}">Personal Skill</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Leadership') }}">Leadership</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Enterpreneurship') }}">Enterpreneurship</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/softskills.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">PRA-VOCATIONAL</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('myCourse/Craft') }}">Craft</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Cultivation') }}">Cultivation</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Engineering') }}">Engineering</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Cooking') }}">Cooking</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/pedagogics.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">OTHERS</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('myCourse/Sports') }}">Sports</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Art') }}">Art</a>
                                    <a class="dropdown-item" href="{{ url('myCourse/Music') }}">Music</a>
                                </div>
                            </li>
                            <li class="list-group-item dropdown p-0">
                                <a class="nav-link dropdown-toggle d-flex align-items-center justify-content-between"
                                    href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div class="menu d-flex">
                                        <img src="{{ asset('assets/img/others.svg') }}" alt="book">
                                        <p class="mb-0 ml-2">RELIGION</p>
                                    </div>
                                </a>
                                <div class="dropdown-menu rounded-0 border-0 p-0 pb-2"
                                    aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ url('myCourse/Islam') }}">Islam</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    @if (auth()->guard('teacher')->check())
                        <button class="btn button take" type="button" data-toggle="modal" data-target="#modal-form-course"><i class="mr-2 fa fa-plus"></i> New Course</button>
                    @endif
                </div>
                {{-- content --}}
                <div class="col-md-9">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-courses" role="tabpanel"
                            aria-labelledby="pills-courses-tab">
                            <div class="courses">
                                <div class="row no-gutters pb-5">
                                    {{-- for looping --}}
                                    @forelse ($courses as $course)
                                    <div class="col-md-4 col-6">
                                        <div class="box-content mb-3">
                                            <img src="{{ $course['cover'] }}" alt="">
                                            <div class="inss">
                                                <a href="{{ url('course').'/'.$course['id'] }}"><h4>{{ $course['title'] }}</h4></a>
                                                <h5>{{ $course['topic']['topic'] }}</h5>
                                                <h6 style="margin-bottom: 100px">{{ $course['educational']['stage'] }}</h6>
                                                <div class="author">
                                                    <div class="row no-gutters">
                                                        <div class="col-md-1 col-1">
                                                            <img class="rounded-circle" src="{{ $course['teacher']['some_detail']['photo'] }}" alt="" width="40">
                                                        </div>
                                                        <div class="col-md-9 col-9">
                                                            <div class="texts">
                                                                <h3>{{ $course['teacher']['some_detail']['fullname'] }}</h3>
                                                                <p>Tutor</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @empty
                                        <h4>Tidak ada kursus yang anda ikuti</h4>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-ongoing" role="tabpanel"
                            aria-labelledby="pills-ongoing-tab">
                            <div class="courses">
                                <div class="row no-gutters pb-5">

                                </div>
                                {{-- <button class="loads mb-5">LOAD MORE</button> --}}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-evertaken" role="tabpanel"
                            aria-labelledby="pills-evertaken-tab">
                            <div class="courses">
                                <div class="row no-gutters pb-5">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade show" id="modal-form-course" tabindex="-1" aria-labelledby="ModalDeleteTitle" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">New Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form-new-course">
                    @csrf
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" class="form-control" placeholder="Isi Judul" name="title"> 
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea rows="5" class="form-control" placeholder="Isi Deskripsi" name="description"></textarea>
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>Topic</label>
                        <select class="form-control" name="topic_id">
                            <option value="1">Natural Science</option>
                            <option value="2">Social Science</option>
                            <option value="3">Vernacular</option>
                            <option value="10">Mathematic</option>
                            <option value="11">Indonesian Language</option>
                            <option value="12">English Language</option>
                            <option value="13">Civilization</option>
                            <option value="8">Communication</option>
                            <option value="9">Social Skill</option>
                            <option value="15">Personal Skill</option>
                            <option value="16">Leadership</option>
                            <option value="17">Enterpreneurship</option>
                            <option value="4">Craft</option>
                            <option value="5">Cultivation</option>
                            <option value="6">Engineering</option>
                            <option value="14">Cooking</option>
                            <option value="7">Sports</option>
                            <option value="18">Arts</option>
                            <option value="19">Music</option>
                            <option value="20">Islam</option>
                        </select>
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label>Educational</label>
                        <select class="form-control" name="educational_id">
                            <option value="1">Primary School (SD)</option>
                            <option value="2">Junior High School (SMP)</option>
                            <option value="3">Senior High School (SMA)</option>
                            <option value="4">High School (PT)</option>
                            <option value="5">Other</option>
                        </select>
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlFile1">Cover</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="cover">
                        <span class="error"></span>
                    </div>
                    {{-- <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                        <label class="form-check-label" for="inlineRadio1">Active</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Non Active</label>
                    </div> --}}
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="keluar" data-dismiss="modal">Cancel</button>
                <button type="button" class="simpan" id="btn-add-course">Simpan</button>
            </div>
        </div>
    </div>
</div>
<script>
    var segments = window.location.href.split( '/' );
    var topic = decodeURI(segments[4]);
    console.log(topic);

    if (topic === "undefined") {
        $('.list-group-item').first().addClass('active');
    } else {
        var listItem = $('.dropdown-item');
        $.each(listItem, function (indexInArray, valueOfElement) { 
            if ($(this).html() == topic) {
                $(this).addClass('active');
                $(this).parent().parent().addClass('active');
                return false;
            }
        });
    }

    $('#btn-add-course').click(function (e) { 
        e.preventDefault();
        emptyFormInput();
        var button = $(this);
        var formData = new FormData($('#form-new-course')[0]);
        $.ajax({
            type: "post",
            url: "{{ url('course') }}",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend: function () {
                button.html('Menyimpan...');
                button.prop('disabled', true);
            },
            success: function (response) {
                if (!response.error) {
                    location.reload()
                } else {
                    for (var key of Object.keys(response.error)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').next().html(response.error[key]);
                    }
                    alert('Ada Beberapa Kolom yang Salah');
                    button.html('Simpan');
                    button.prop('disabled', false);
                }
            }
        });
        
    });
</script>
@endsection