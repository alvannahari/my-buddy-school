<?php 
    define('UPLOAD_COVER_COURSE', 'assets/img/cover');
    define('UPLOAD_PHOTO_USER', 'assets/img/user');
    define('UPLOAD_PHOTO_STUDENT', 'assets/img/student');
    define('UPLOAD_PHOTO_TEACHER', 'assets/img/teacher');
    define('UPLOAD_IMAGE_QUESTION','assets/img/question');
    define('UPLOAD_FILE_QUIZ','assets/file/quiz');

    // Dynamic Constant PATH
    define('PHOTO_USER_PATH','assets/img/user');
    define('PHOTO_STUDENT_PATH','assets/img/student');
    define('PHOTO_TEACHER_PATH','assets/img/teacher');
    define('IMAGE_MITRA_PATH','assets/img/mitra');
    // define('VIDEO_MITRA_PATH','assets/video/mitra');
    define('COVER_COURSE_PATH','assets/img/cover');
    define('FILE_QUIZ_PATH','assets/file/quiz');
    define('IMAGE_QUESTION_PATH','assets/img/question');

?>