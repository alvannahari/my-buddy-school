<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm');
Route::get('register', 'Auth\RegisterController@showRegistrationForm');

Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('register', 'Auth\RegisterController@register')->name('register');

Route::get('verify/{token}/{user_id}/{role}/{password}', 'Auth\VerificationController@verifyEmail')->name('verification.verify');
Route::post('resendEmail', 'Auth\VerificationController@resendEmail')->name('verification.resend');

// Route::group(['middleware' => 'verified'], function () {
//     Route::get('/home', 'HomeController@index')->name('home');
// });

Route::get('/', 'HomeController@index')->name('home');
Route::get('contactUs', 'HomeController@contactUs')->name('contact');
Route::get('aboutUs', 'HomeController@aboutus')->name('about');

Route::get('test', 'HomeController@test')->name('test');

Route::group(['middleware' => ['auth:teacher']], function () {
    Route::post('course', 'CourseController@store');
    Route::group(['middleware' => ['have.course']], function () {
        Route::get('course/{course}/delete', 'CourseController@destroy');
        Route::get('course/{course}/state', 'CourseController@changeState');
        Route::post('course/{course}/zoom', 'CourseController@zoom');
        Route::post('course/{course}', 'CourseController@update');
    
        Route::get('theory/{course}/form/{theory?}', 'TheoryController@form');
        Route::post('theory/{course}', 'TheoryController@store');
        Route::post('theory/{theory}/update', 'TheoryController@update');
        Route::get('theory/{theory}/delete', 'TheoryController@destroy');
    
        Route::post('discussion/{discussion}/update', 'DiscussionController@update');
        Route::get('discussion/{discussion}/delete', 'DiscussionController@destroy');
    
        Route::post('quiz/{course}', 'QuizController@store');
        Route::post('quiz/{quiz}/update', 'QuizController@update');
        Route::get('quiz/{quiz}/state', 'QuizController@changeState');
        Route::get('quiz/{quiz}/delete', 'QuizController@destroy');
        
        Route::post('question/{quiz}', 'QuestionController@store'); 
        Route::get('question/{question}', 'QuestionController@edit'); 
        Route::post('question/{question}/update', 'QuestionController@update'); 
        Route::get('question/{question}/delete', 'QuestionController@destroy'); 
    });
});

Route::group(['middleware' => 'auth:student'], function () {
    Route::get('search', 'CourseController@search');
    Route::get('course/allCourse', 'CourseController@index');
    Route::get('takeCourse/{course}', 'CourseController@takeCourse');

    Route::group(['middleware' => 'auth:student'], function () {
        Route::get('quitCourse/{course}', 'CourseController@quitCourse')->middleware('have.course');
        
        Route::post('discussion/{course}', 'DiscussionController@store')->middleware('have.course');
        
        Route::post('quiz/{quiz}/submit', 'QuizController@submitResult')->middleware('have.course');
    });
});

Route::group(['middleware' => 'auth:student,teacher'], function () {
    Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('myCourse/{topic?}', 'HomeController@myCourse')->name('myCourse');
    
    Route::get('course/{course}/discussion', 'DiscussionController@index')->middleware('have.course');
    Route::get('course/{course}/quiz', 'QuizController@index')->middleware('have.course');
    Route::get('course/{course}/result', 'CourseController@getResult')->middleware('have.course');
    Route::get('course/{course}', 'CourseController@show');

    Route::get('theory/{theory}', 'TheoryController@show')->middleware('have.course')->name('theory.show');

    Route::get('discussion/{discussion}', 'DiscussionController@show')->middleware('have.course');
    Route::post('discussion/{discussion}/submit', 'DiscussionController@submitComment')->middleware('have.course');

    Route::get('quiz/{quiz}', 'QuizController@show')->middleware('have.course'); 

    Route::get('profile/{page?}', 'UserController@index');
    Route::post('profile/remove', 'UserController@removePhoto');
    Route::post('profile/photo', 'UserController@updatePhoto');
    Route::post('profile/personal', 'UserController@updatePersonal');
    Route::post('profile/location', 'UserController@updateLocation');
    Route::post('profile/socmed', 'UserController@updateSocmed');
    Route::post('profile/password', 'UserController@updatePassword');

    Route::get('inbox', 'InboxController@index');
    Route::post('inbox', 'InboxController@store');
    Route::get('inbox/{user_id}', 'InboxController@show');
    Route::post('inbox/{inbox}/delete', 'InboxController@destroy');

    Route::get('teacher', 'TeacherController@index');
    Route::get('teacher/{teacher}', 'TeacherController@show');

    Route::get('mitra', 'MitraController@index');
    Route::get('mitra/{mitra}', 'MitraController@show');

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth:student,teacher,admin,super'], function () {
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['middleware' => 'auth:student'], function () {
    Route::get('course/{educational}/{topic}', 'CourseController@index');
});

Route::group(['middleware' => 'auth:admin', 'prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin-'], function () {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::get('course', 'CourseController@index')->name('course');
    Route::get('course/{course}', 'CourseController@show')->name('course.detail');
    Route::post('course/{course}', 'CourseController@update')->name('course.update');
    Route::post('course/{course}/state', 'CourseController@state')->name('course.state');
    Route::delete('course/{course}', 'CourseController@destory')->name('course.destroy');

    Route::get('student', 'StudentController@index')->name('student');
    Route::get('student/{student}', 'StudentController@show')->name('student.detail');
    Route::post('student/{student}', 'StudentController@update')->name('student.update');
    Route::post('student/{student}/state', 'StudentController@state')->name('student.state');
    Route::post('student/{student}/photo', 'StudentController@updatePhoto')->name('student.update-photo');
    Route::delete('student/{student}/photo', 'StudentController@deletePhoto')->name('student.delete-photo');
    Route::delete('student/{student}', 'StudentController@destroy')->name('student.destroy');

    Route::get('mitra', 'MitraController@index')->name('mitra');
    Route::get('mitra/create', 'MitraController@create')->name('mitra.create');
    Route::post('mitra', 'MitraController@store')->name('mitra.store');
    Route::get('mitra/{mitra}', 'MitraController@show')->name('mitra.detail');
    Route::post('mitra/{mitra}/update', 'MitraController@update')->name('mitra.update');
    Route::delete('mitra/{mitra}', 'MitraController@destroy')->name('mitra.destroy');

    Route::get('teacher', 'TeacherController@index')->name('teacher');
    Route::get('teacher/{teacher}', 'TeacherController@show')->name('teacher.detail');
    Route::post('teacher/{teacher}', 'TeacherController@update')->name('teacher.update');
    Route::post('teacher/{teacher}/state', 'TeacherController@state')->name('teacher.state');
    Route::post('teacher/{teacher}/photo', 'TeacherController@updatePhoto')->name('teacher.update-photo');
    Route::delete('teacher/{teacher}/photo', 'TeacherController@deletePhoto')->name('teacher.delete-photo');
    Route::delete('teacher/{teacher}', 'TeacherController@destroy')->name('teacher.destroy');

    Route::get('setting', 'SettingController@socmed')->name('setting.social');
    Route::post('setting', 'SettingController@updateSocmed')->name('setting.social.update');
});