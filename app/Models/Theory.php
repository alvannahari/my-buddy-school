<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theory extends Model {
    
    const ID = 'id';
    const COURSE_ID = 'course_id';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const CONTENT = 'content';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function course() {
        return $this->belongsTo(Course::class);
    }
}
