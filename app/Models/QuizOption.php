<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizOption extends Model {
    
    const ID = 'id';
    const QUESTION_ID = 'question_id';
    const OPTION = 'option';
    const IS_ANSWER = 'is_answer';

    protected $guarded = [];

    function quiz() {
        return $this->belongsTo(Quiz::class);
    }
}
