<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseStudentTeacher extends Model {
    
    const ID = 'id';
    const STUDENT_ID = 'student_id';
    const COURSE_ID = 'course_id';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s'
    ];
    
}
