<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    
    const ID = 'id';
    const CATEGORY = 'category';
    const DESCRIPTION = 'description';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function topics() {
        return $this->hasMany(Topic::class);
    }
}
