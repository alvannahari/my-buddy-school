<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Course extends Model {

    const COVER_PATH = 'course/';
    
    const ID = 'id';
    const TEACHER_ID = 'teacher_id';
    const TOPIC_ID = 'topic_id';
    const EDUCATIONAL_ID = 'educational_id';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const COVER = 'cover';
    const ZOOM = 'zoom';
    const SCHEDULE = 'schedule';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    protected $appends = ['time_schedule'];

    function delete() {
        Storage::disk('public')->delete(Self::COVER_PATH.$this->attributes[Self::COVER]);

        $this->theories()->delete();
        $this->discussions()->delete();
        $this->quizzes()->delete();
        $this->zooms()->delete();

        return parent::delete();
    }

    public function getCoverAttribute($value) {
        return Storage::url(SELF::COVER_PATH.$value);
    }

    function getTimeScheduleAttribute() {
        $value = $this->attributes[Self::SCHEDULE] ?? 'null';
        $result = null;
        if ($value != null) {
            $date = str_replace('/', '-', $value);
            $result = date('Y-m-d H:i:s', strtotime($date));
        }
        return $result;
    }

    function isActive() {
        return (boolean) $this->where(SELF::IS_ACTIVE, '1')->first();
    }

    function theories() {
        return $this->hasMany(Theory::class);
    }

    function discussions() {
        return $this->hasMany(Discussion::class);
    }

    function quizzes() {
        return $this->hasMany(Quiz::class);
    }

    function teacher() {
        return $this->belongsTo(Teacher::class);
    }

    function topic() {
        return $this->belongsTo(Topic::class);
    }

    function educational() {
        return $this->belongsTo(Educational::class);
    }

    function students() {
        return $this->belongsToMany(Student::class, 'course_student_teachers')->orderby('created_at', 'desc')->withPivot(SELF::CREATED_AT);
    }
}
