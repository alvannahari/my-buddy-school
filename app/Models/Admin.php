<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Admin extends Authenticatable {
    
    use Notifiable;

    const ID = 'id';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const FULLNAME = 'fullname';
    const PHONE = 'phone';
    const VERIFIED_AT = 'email_verified_at';
    const REMEMBER_TOKEN = 'remember_token';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD, SELF::REMEMBER_TOKEN];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];
}
