<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Partner extends Model {

    const IMAGE_PATH = 'mitra/';
    
    const ID = 'id';
    const EDUCATIONAL_ID = 'educational_id';
    const NAME = 'name';
    const IMAGE = 'image';
    const EMAIL = 'email';
    const ADDRESS = 'address';
    const CITY = 'city';
    const VIDEO = 'video';
    const PHONE = 'phone';
    const WEBSITE = 'website';
    const FACEBOOK = 'facebook';
    const TWITTER = 'twitter';
    const INSTAGRAM = 'instagram';
    const YOUTUBE = 'youtube';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
        return parent::delete();
    }

    public function getImageAttribute($value) {
        if ($value == null) return Storage::url(Self::IMAGE_PATH.'default,png');
        else return Storage::url(Self::IMAGE_PATH.$value);
    }

    // public function getVideoAttribute() {
    //     if ($this->attributes[SELF::VIDEO] == null) return 'https://www.youtube.com/embed/PT2_F-1esPk';
    // }

    public function getVideoAttribute() {
        if ($this->attributes[SELF::VIDEO] == null) return '#';
        else return $this->attributes[SELF::VIDEO];
    }

    public function getWebsiteAttribute() {
        if ($this->attributes[SELF::WEBSITE] == null) return '#';
        else return $this->attributes[SELF::WEBSITE];
    }
    public function getFacebookAttribute() {
        if ($this->attributes[SELF::FACEBOOK] == null) return '#';
        else return $this->attributes[SELF::FACEBOOK];
    }
    public function getTwitterAttribute() {
        if ($this->attributes[SELF::TWITTER] == null) return '#';
        else return $this->attributes[SELF::TWITTER];
    }
    public function getInstagramAttribute() {
        if ($this->attributes[SELF::INSTAGRAM] == null) return '#';
        else return $this->attributes[SELF::INSTAGRAM];
    }
    public function getYoutubeAttribute() {
        if ($this->attributes[SELF::YOUTUBE] == null) return '#';
        else return $this->attributes[SELF::YOUTUBE];
    }

    function educational() {
        return $this->belongsTo(Educational::class);
    }
}
