<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Question extends Model {

    const IMAGE_PATH = 'question/';

    const ID = 'id';
    const QUIZ_ID = 'quiz_id';
    const QUESTION = 'question';
    const IMAGE = 'image';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        if ($this->attributes[Self::IMAGE] != null)
            Storage::disk('public')->delete(Self::IMAGE_PATH.$this->attributes[Self::IMAGE]);
        
        $this->options()->delete();

        return parent::delete();
    }

    public function getImageAttribute($value) {
        if ($value != null) return Storage::url(Self::IMAGE_PATH.$value);
    }

    function quiz() {
        return $this->belongsTo(Quiz::class);
    }

    function options() {
        return $this->hasMany(QuizOption::class);
    }
}
