<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model {
    
    const ID = 'id';
    const STUDENT_ID = 'student_id';
    const TEACHER_ID = 'teacher_id';
    const SENDER_S = 'sender_s';
    const MESSAGE = 'message';
    const READ_S = 'read_s';
    const READ_T = 'read_t';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function student() {
        return $this->belongsTo(Student::class);
    }

    function teacher() {
        return $this->belongsTo(Teacher::class);
    }
}
