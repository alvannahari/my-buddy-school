<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DiscussionComment extends Model {
    
    const ID = 'id';
    const DISCUSSION_ID = 'discussion_id';
    const USER_ID = 'userable_id';
    const USER_TYPE = 'userable_type';
    const COMMENT = 'comment';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function discussion() {
        return $this->belongsTo(Discussion::class);
    }

    public function userable() {
        return $this->morphTo();
    }
}
