<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model {
    
    const ID = 'id';
    const YOUTUBE = 'youtube';
    const FACEBOOK = 'facebook';
    const TWITTER = 'twitter';
    const INSTAGRAM = 'instagram';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];
}
