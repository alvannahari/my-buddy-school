<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Educational extends Model {
    
    const ID = 'id';
    const STAGE = 'stage';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function course() {
        return $this->hasMany(Course::class);
    }

    function partners() {
        return $this->hasMany(Partner::class);
    }
}
