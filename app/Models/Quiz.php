<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Quiz extends Model {

    const FILE_PATH = 'quiz/';
    
    const ID = 'id';
    const COURSE_ID = 'course_id';
    const TITLE = 'title';
    const FILE = 'file';
    const IS_ACTIVE = 'is_active';
    // const ANSWER = 'answer';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        if ($this->attributes[Self::FILE] != null) 
            Storage::disk('public')->delete(Self::FILE_PATH.$this->attributes[Self::FILE]);

        $this->questions()->delete();

        return parent::delete();
    }

    public function getFileAttribute($value) {
        if ($value != null) return Storage::url(Self::FILE_PATH.$value);
    }

    function course() {
        return $this->belongsTo(Course::class);
    }

    function results() {
        return $this->hasMany(QuizResult::class);
    }

    function sumresults() {
        return $this->results()->select('student_id', DB::raw('sum(quiz_results.score) total'))
        ->groupBy('student_id');
    }

    function questions() {
        return $this->hasMany(Question::class);
    }

    function isActive() {
        return (boolean) $this->where(SELF::IS_ACTIVE, '1')->first();
    }

}
