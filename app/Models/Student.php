<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\Models\StudentDetail;
use Illuminate\Support\Carbon;

class Student extends Authenticatable implements MustVerifyEmail {
    
    use Notifiable;
    
    const ID = 'id';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const VERIFIED_AT = 'email_verified_at';
    const REMEMBER_TOKEN = 'remember_token';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD, SELF::REMEMBER_TOKEN];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->detail()->delete();

        return parent::delete();
    }

    function detail() {
        return $this->hasOne(StudentDetail::class);
    }
    function checkDetail() {
        $detail = $this->detail->attributes;
        foreach ($detail as $key => $value) 
            if (($key != 'lati_loc' && $key != 'long_loc') && $value == null) 
                return $key;

        return false;
    }

    // only use to poymorphic with spesific column
    function someDetail() {
        return $this->detail()->select(StudentDetail::STUDENT_ID, StudentDetail::FULLNAME, StudentDetail::PHOTO);
    }

    function inboxes() {
        return $this->hasMany(Inbox::class)->latest();
    }

    function unreadInbox() {
        return $this->inboxes()->where(Inbox::READ_S, '0')->count();
    }

    function results() {
        return $this->hasMany(QuizResult::class);
    }

    function discussions() {
        return $this->hasMany(Discussion::class);
    }

    function comments() {
        return $this->morphMany(DiscussionComment::class, 'userable');
    }

    function histories() {
        return $this->morphMany(HistoryLogin::class, 'userable');
    }

    function courses() {
        return $this->belongsToMany(Course::class, 'course_student_teachers')->orderby('course_student_teachers.created_at', 'desc')->withPivot(SELF::CREATED_AT);
    }

    function takeCourse($course_id) {
        $this->courses()->attach($course_id);
        return $this;
    }

    public function quitCourse($course_id) {
        $this->courses()->detach($course_id);
        return $this;
    }

    public function hasTaken($course_id)  {
        return (boolean) $this->courses()->where('course_id', $course_id)->first(['course_student_teachers.'.SELF::ID]);
    }

    public function hasResult($quiz_id)  {
        return (boolean) $this->results()->where('quiz_id', $quiz_id)->first(['quiz_results.'.SELF::ID]);
    }
    public function getResult($quiz_id)  {
        return $this->results()->where('quiz_id', $quiz_id)->first('score');
    }

    function login() {
        $this->histories()->updateOrCreate(
            [Self::CREATED_AT => Carbon::today()],
            [HistoryLogin::IS_ONLINE => '1']
        );
    }
    function logout() {
        $this->histories()->updateOrCreate(
            [Self::CREATED_AT => Carbon::today()],
            [HistoryLogin::IS_ONLINE => '0']
        );
    }
}
