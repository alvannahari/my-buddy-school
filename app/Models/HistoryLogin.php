<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryLogin extends Model {
    
    const ID = 'id';
    const USER_ID = 'userable_id';
    const USER_TYPE = 'userable_type';
    const IS_ONLINE = 'is_online';

    protected $guarded = [];

    public function userable() {
        return $this->morphTo();
    }
}
