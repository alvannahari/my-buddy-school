<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizResult extends Model {
    
    const ID = 'id';
    const QUIZ_ID = 'quiz_id';
    const STUDENT_ID = 'student_id';
    const SCORE = 'score';

    protected $guarded = [];

    function quiz() {
        return $this->belongsTo(Quiz::class);
    }

    function student() {
        return $this->belongsTo(Student::class);
    }

    // public function setUpdatedAtAttribute($value) {
        // to Disable updated_at
    // }
}
