<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class StudentDetail extends Model {

    const PHOTO_PATH = 'student/';
    
    const ID = 'id';
    const STUDENT_ID = 'student_id';
    const FULLNAME = 'fullname';
    const GENDER = 'gender';
    const PLACE_BIRTH = 'place_of_birth';
    const DATE_BIRTH = 'date_of_birth';
    const GRADE = 'grade';
    const PHOTO = 'photo';
    const ADDRESS = 'address';
    const COUNTRY = 'country';
    const PROVINCE = 'province';
    const CITY = 'city';
    const LONG_LOC = 'long_loc';
    const LATI_LOC = 'lati_loc';
    const FACEBOOK = 'facebook';
    const TWITTER = 'twitter';
    const INSTAGRAM = 'instagram';
    const PHONE = 'phone';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    public function getPhotoAttribute($value) {
        if ($value == null) return Storage::url(Self::PHOTO_PATH.'default.png');
        else return Storage::url(Self::PHOTO_PATH.$value);
    }

    public function getFacebookAttribute() {
        if ($this->attributes[SELF::FACEBOOK] == null) return '#';
        else return $this->attributes[SELF::FACEBOOK];
    }
    public function getTwitterAttribute() {
        if ($this->attributes[SELF::TWITTER] == null) return '#';
        else return $this->attributes[SELF::TWITTER];
    }
    public function getInstagramAttribute() {
        if ($this->attributes[SELF::INSTAGRAM] == null) return '#';
        else return $this->attributes[SELF::INSTAGRAM];
    }

    function delete() {
        if ($this->attributes[Self::PHOTO] != null) 
            Storage::disk('public')->delete(Self::PHOTO_PATH.$this->attributes[Self::PHOTO]);

        return parent::delete();
    }

    function student() {
        return $this->belongsTo(Student::class);
    }

    function removePhoto() {
        if ($this->attributes[Self::PHOTO] != null) {
            Storage::disk('public')->delete(Self::PHOTO_PATH.$this->attributes[Self::PHOTO]);

            $this->update([Self::PHOTO => null]);
            return true;
        }
        return false;
    }
}
