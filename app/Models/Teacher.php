<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

class Teacher extends Authenticatable implements MustVerifyEmail {
    
    use Notifiable;
    
    const ID = 'id';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const VERIFIED_AT = 'email_verified_at';
    const REMEMBER_TOKEN = 'remember_token';

    protected $guarded = [];

    protected $hidden = [SELF::PASSWORD, SELF::REMEMBER_TOKEN];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->detail()->delete();

        return parent::delete();
    }

    function detail() {
        return $this->hasOne(TeacherDetail::class);
    }
    function checkDetail() {
        $detail = $this->detail->attributes;
        foreach ($detail as $key => $value) 
            if (($key != 'lati_loc' && $key != 'long_loc') && $value == null) 
                return true;

        return false;
    }

    // only use to polymorphic relation with spesific column
    function someDetail() {
        return $this->detail()->select(TeacherDetail::TEACHER_ID, TeacherDetail::FULLNAME, TeacherDetail::PHOTO);
    }

    function courses() {
        return $this->hasMany(Course::class);
    }

    function inboxes() {
        return $this->hasMany(Inbox::class);
    }

    function unreadInbox() {
        return $this->inboxes()->where(Inbox::READ_T, '0')->count();
    }

    function comments() {
        return $this->morphMany(DiscussionComment::class, 'userable');
    }

    function histories() {
        return $this->morphMany(HistoryLogin::class, 'userable');
    }

    public function isOwner($course_id)  {
        return (boolean) $this->courses()->where('id', $course_id)->first(['courses.'.SELF::ID]);
    }

    function login() {
        $this->histories()->updateOrCreate(
            [Self::CREATED_AT => Carbon::today()],
            [HistoryLogin::IS_ONLINE => '1']
        );
    }
    function logout() {
        $this->histories()->updateOrCreate(
            [Self::CREATED_AT => Carbon::today()],
            [HistoryLogin::IS_ONLINE => '0']
        );
    }
}
