<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discussion extends Model {
    
    const ID = 'id';
    const COURSE_ID = 'course_id';
    const STUDENT_ID = 'student_id';
    const TITLE = 'title';
    const DISCUSSION = 'discussion';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function delete() {
        $this->comments()->delete();

        return parent::delete();
    }

    function comments() {
        return $this->hasMany(DiscussionComment::class);
    }

    function course() {
        return $this->belongsTo(Course::class);
    }

    function student() {
        return $this->belongsTo(Student::class);
    }
}
