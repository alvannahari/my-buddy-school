<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model {
    
    const ID = 'id';
    const CATEGORY_ID = 'category_id';
    const TOPIC = 'topic';

    protected $guarded = [];

    protected $casts = [
        SELF::CREATED_AT => 'datetime:Y-m-d H:i:s',
        SELF::UPDATED_AT => 'datetime:Y-m-d H:i:s'
    ];

    function category() {
        return $this->belongsTo(Category::class);
    }

    function course() {
        return $this->hasMany(Course::class);
    }
}
