<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Superuser extends Authenticatable implements MustVerifyEmail {
    
    use Notifiable;
    
    const ID = 'id';
    const EMAIL = 'email';
    const PASSWORD = 'password';

    protected $fillable = [];

    protected $hidden = [SELF::PASSWORD];
}
