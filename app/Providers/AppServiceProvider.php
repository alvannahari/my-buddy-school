<?php

namespace App\Providers;

use App\Models\CompanyProfile;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'student' => 'App\Models\Student',
            'teacher' => 'App\Models\Teacher',
        ]);

        $data['setting'] = CompanyProfile::first()->toArray();
        View::share($data);
    }
}
