<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Quiz;
use App\Models\Course;
use App\Models\QuizOption;
use App\Models\QuizResult;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class QuizController extends Controller {

    protected $user;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard(session()->get('guard'))->user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course) {
        // $quizzes = Quiz::with(['course.topic', 'course.educational'])->where(Quiz::COURSE_ID, $course->id)->get()->toArray();
        $course_temp = $course;
        $quizzes = $course->load(['topic', 'educational', 'quizzes'])->toArray();

        $course = Course::with('topic.category','educational','teacher.someDetail')->find($course_temp->id)->toArray();
        if (auth()->guard('student')->check()) {
            foreach ($quizzes['quizzes'] as $key => $value) {
                if ($this->user->hasResult($value[Quiz::ID])) $quizzes['quizzes'][$key]['hasResult'] = true;
                else $quizzes['quizzes'][$key]['hasResult'] = false;
            }
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_owner'] = $this->user->isOwner($course[Course::ID]);
        }
        // dd($zooms);
        return view('course.quiz.index', compact('quizzes', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Course $course) {
        // dd($request->toArray());
        $validator = Validator::make($request->all(), [
            Quiz::TITLE     => 'required|string|max:255',
            Quiz::FILE      => 'mimes:pdf|max:16384'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(Quiz::TITLE);
        $credentials[Quiz::COURSE_ID] = $course->id;
        
        if ($request->has(QUIZ::FILE)) {
            $file = Storage::disk('public')->put(Quiz::FILE_PATH, $request->file(Quiz::FILE));
            $credentials[Quiz::FILE] = basename($file);
        };

        Quiz::create($credentials);

        return response(['error' => false]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Quiz $quiz) {
        $quiz_temp = $quiz;

        $course = Course::with('topic.category','educational','teacher.someDetail')->find($quiz_temp->course->id)->toArray();
        if (auth()->guard('student')->check()) {
            if ($this->user->hasResult($quiz_temp->id)) {
                $quiz['score'] = $this->user->getResult($quiz_temp->id)->score;
                $quiz['hasResult'] = true;
            } else {
                $quiz = $quiz->load('questions.options')->loadCount('questions')->toArray();
                $quiz['hasResult'] = false;
            }
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
            // dd($quiz);
            return view('course.quiz.student', compact('quiz', 'course'));
        } else if (auth()->guard('teacher')->check()) {
            $course['is_owner'] = $this->user->isOwner($course[Course::ID]);
            $quiz = $quiz->load('questions.options')->loadCount('questions')->toArray();
            return view('course.quiz.teacher', compact('quiz', 'course'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quiz $quiz) {
        $validator = Validator::make($request->all(), [
            Quiz::TITLE     => 'required|string|max:255',
            Quiz::FILE      => 'mimes:pdf|max:16384'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(Quiz::TITLE);
        
        if ($request->has(QUIZ::FILE)) {
            if ($quiz->file != null) 
                $file = Storage::disk('public')->delete(Quiz::FILE_PATH.$quiz->file);
            
            $file = Storage::disk('public')->put(Quiz::FILE_PATH, $request->file(Quiz::FILE));
            $credentials[Quiz::FILE] = basename($file);
        };

        if ($quiz->update($credentials)) 
            return response(['error' => false]);

        return response(['error' => 'Terjadi Kesalahan !!!']);
    }

    function changeState(Quiz $quiz) {
        $state = $quiz->is_active;
        if ($state == 0) $quiz->update([Quiz::IS_ACTIVE => '1']);
        else $quiz->update([Quiz::IS_ACTIVE => '0']);

        return response(['error' => false]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quiz $quiz) {
        if ($quiz->delete()) 
            return response(['error' => false]);
        
        return response(['error' => 'Konten Gagal Terhapus']);
    }

    function submitResult(Request $request, Quiz $quiz) {
        // $example = 'http://127.0.0.1:8000/quiz/10001/submit?answer0=0&answer1=4&answer2=4&answer3=2';
        if (!$quiz->isActive()) {
            abort(404);
        }
        if(empty($request->question1)) {
            $credentials[QuizResult::QUIZ_ID] = $quiz->id;
            $credentials[QuizResult::STUDENT_ID] = $this->user->id;
            $credentials[QuizResult::SCORE] = 0;
            QuizResult::create($credentials);

            return response(['error' => false]);
        }
        // $questions = Question::where(Question::QUIZ_ID, $quiz->id)->with('options')->get()->toArray();
        $questions = $quiz->questions->load('options')->toArray();

        $results = $request->toArray();
        // return response(['error' => $results]);
        $true = 0;
        foreach ($questions as $key => $value) {
            $iteration = $key+1;
            if (!empty($results['question'.$iteration])) {
                $answer = $results['question'.$iteration];
                $answer -= 1;
                foreach ($value['options'] as $keyOption => $valueOption) {
                    if ($keyOption == $answer && $valueOption[QuizOption::IS_ANSWER] == '1') {
                        $true++;
                        break;
                    }
                }
            }
        }

        $update = QuizResult::where(QuizResult::QUIZ_ID, $quiz->id)->where(QuizResult::STUDENT_ID, $this->user->id)->first();
        $credentials[QuizResult::SCORE] = $true*10;
        if ($update->update($credentials)) {
            return response(['error' => false]);
        }
        return response(['error' => 'Trejadi Kesalahan !!!']);
    }
}
