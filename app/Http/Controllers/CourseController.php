<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Course;
use App\Models\CourseStudentTeacher;
use App\Models\Educational;
use App\Models\Partner;
use App\Models\Topic;
use App\Models\QuizResult;
use Illuminate\Support\Facades\Auth;
use App\Models\Teacher;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller {

    protected $user;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard(session()->get('guard'))->user();
            return $next($request);
        });
    }
    
    function index($educational = null, $topic = null) {
        if (auth()->guard('teacher')->check()) {
            return redirect()->route('myCourse');
        }

        $data = [];
        if ($educational == null && $topic == null) {
            $data = Course::inRandomOrder()->with(['topic', 'teacher.someDetail', 'educational'])->get()->toArray();
            $mitra = $this->_getMitra();
            $tutor = $this->_getTutor();
            $data_latest = Course::with(['topic', 'teacher.someDetail', 'educational'])->orderBy(Course::CREATED_AT, 'DESC')->take(8)->get()->toArray();
            $latest = $this->_setCheckOwner($data_latest);
        } else {
            if ($topic == 'all') $data = $this->_getDataCourse($educational);
            else $data = $this->_getDataCourse($educational, $topic);
            $mitra = $this->_getMitra($educational);
            $tutor = $this->_getTutor($educational);
        }
        $courses = $this->_setCheckOwner($data);
        // dd($latest);
        if ($educational == null && $topic == null) return view('course.all', compact('courses', 'mitra', 'tutor', 'latest'));
        else return view('course.index', compact('courses', 'topic', 'educational', 'mitra', 'tutor'));
    }

    function search(Request $request) {
        $search = $request->input('search');
        if ($request->has('grade') && $request->has('category')) {
            $grade = array_values($request->grade);
            $category = array_values($request->category);
            $topics = Topic::whereIn(Topic::CATEGORY_ID, $category)->pluck(Topic::ID)->toArray();
            $course = Course::with('topic','educational','teacher.someDetail')->where(Course::TITLE, "LIKE", "%" . $search . "%")->whereIn(Course::EDUCATIONAL_ID, $grade)->whereIn(Course::TOPIC_ID, $topics)->get()->toArray();
        }
        else if ($request->has('grade')) {
            $grade = array_values($request->grade);
            $course = Course::with('topic','educational','teacher.someDetail')->where(Course::TITLE, "LIKE", "%" . $search . "%")->whereIn(Course::EDUCATIONAL_ID, $grade)->get()->toArray();
        }
        else if ($request->has('category')) {
            $category = array_values($request->category);
            $topics = Topic::whereIn(Topic::CATEGORY_ID, $category)->pluck(Topic::ID)->toArray();
            $course = Course::with('topic','educational','teacher.someDetail')->where(Course::TITLE, "LIKE", "%" . $search . "%")->whereIn(Course::TOPIC_ID, $topics)->get()->toArray();
        } else {
            $course = Course::with('topic','educational','teacher.someDetail')->where(Course::TITLE, "LIKE", "%" . $search . "%")->get()->toArray();
        };

        $result = $this->_setCheckOwner($course);
        // dd($result);
        return view('course.search', compact('result', 'search'));
    }

    function store(Request $request) {
        $validator = Validator::make($request->all(),[
            Course::TOPIC_ID        => 'required|numeric',
            Course::EDUCATIONAL_ID  => 'required|numeric',
            Course::TITLE           => 'required|string|max:255',
            Course::DESCRIPTION     => 'required|string|max:255',
            Course::COVER           => 'required|image|mimes:jpg,jpeg,png|max:8192'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(Course::TOPIC_ID, Course::EDUCATIONAL_ID, Course::TITLE, Course::DESCRIPTION);
        $credentials[Course::TEACHER_ID] = $this->user->id;

        $course = Course::create($credentials);

        $cover = Storage::disk('public')->put(Course::COVER_PATH, $request->file(Course::COVER));

        if($course->update([Course::COVER => basename($cover)])) 
            return response(['error' => false]);

        return response(['error' => 'Terjadi Kesalahan !!!']);
    }

    function takeCourse(Course $course) {
        if ($this->user->hasTaken($course->id)) 
            return response(['error' => 'Kursus telah diambil!!']);
        
        $this->user->takeCourse($course->id);
        return response(['error' => false]);
    }

    function quitCourse(Course $course) {
        if (!$this->user->hasTaken($course->id)) 
            return response(['error' => 'Kursus belum pernah diambil!!']);
        
        $this->user->quitCourse($course->id);
        return response(['error' => false]);
    }

    function show(Course $course) {
        $course_temp = $course;
        $course = Course::with('topic.category','educational','teacher.someDetail','theories:id,course_id,title,description')->withCount('theories','quizzes','discussions','students')->find($course_temp->id)->toArray();

        if (auth()->guard('student')->check()) {
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_mine'] = $this->user->isOwner($course[Course::ID]);
        }

        return view('course.detail', compact('course'));
    }

    function update(Request $request,Course $course) {
        $validator = Validator::make($request->all(),[
            Course::TITLE           => 'required|string|max:255',
            Course::DESCRIPTION     => 'required|string|max:255',
            Course::COVER           => 'image|mimes:jpg,jpeg,png|max:8192'
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(Course::TITLE, Course::DESCRIPTION);
        $credentials[Course::TEACHER_ID] = $this->user->id;

        if ($request->has(Course::COVER)) { 
            $arr_cover = explode("/",$course->cover);
            Storage::disk('public')->delete(Course::COVER_PATH.end($arr_cover));

            $cover = Storage::disk('public')->put(Course::COVER_PATH, $request->file(Course::COVER));
            $credentials[Course::COVER] = basename($cover);
        };

        if( $course->update($credentials) ) 
            return response(['error' => false]);

        return response(['error' => 'Terjadi Kesalahan !!!']);
    }

    function changeState(Course $course) {
        $state = $course->is_active;
        if ($state == 0) $course->update([Course::IS_ACTIVE => '1']);
        else $course->update([Course::IS_ACTIVE => '0']);

        return response(['error' => false]);
    }

    function zoom(Course $course, Request $request) {
        $validator = Validator::make($request->all(), [
            Course::ZOOM    => 'string',
            Course::SCHEDULE    => 'string',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        };
        $course->update([
            Course::ZOOM => $request->zoom,
            Course::SCHEDULE => $request->schedule
        ]);

        return response(['error' => false]);
    }

    function destroy(Course $course) {
        if ($course->delete()) 
            return response(['error' => false]);

        return response(['error' => 'Something Wrong!!!']);
    }

    private function _getDataCourse($educational, $topic = null) {
        if ($topic == null) {
            $data = Course::with(['educational', 'topic', 'teacher.someDetail'])
            ->whereHas('educational', function ($query) use ($educational) {
                $query->where(Educational::STAGE, $educational);
            })
            ->get()->toArray();
        } else {
            $data = Course::with(['educational', 'topic', 'teacher.someDetail'])
            ->whereHas('educational', function ($query) use ($educational) {
                $query->where(Educational::STAGE, $educational);
            })
            ->whereHas('topic', function ($query) use ($topic) {
                $query->where(Topic::TOPIC, $topic);
            })
            ->get()->toArray();
        }

        return $data;
    }

    private function _setCheckOwner(array $data) {
        if (auth()->guard('student')->check()) {
            $userIds = CourseStudentTeacher::where(CourseStudentTeacher::STUDENT_ID, $this->user->id)->pluck(CourseStudentTeacher::COURSE_ID)->toArray();
            foreach ($data as $key => $value) 
                if (in_array($value[COURSE::ID], $userIds)) $data[$key]['has_taken'] = true;
                else $data[$key]['has_taken'] = false;
        }
        else if (auth()->guard('teacher')->check()) {
            foreach ($data as $key => $value) 
                if ($data[$key][Course::TEACHER_ID] == $this->user->id) $data[$key]['is_mine'] = true;
                else $data[$key]['is_mine'] = false;
        } else {
            foreach ($data as $key => $value) 
                $data[$key]['has_taken'] = false;
        }

        return $data;
    }

    private function _getMitra($educational = null) {
        if ($educational == null) 
            return Partner::inRandomOrder()->limit(4)->get()->toArray();
        else {
            $educational_id = Educational::where(Educational::STAGE, $educational)->first()->id;
            return Partner::whereIn(Partner::EDUCATIONAL_ID, [0,$educational_id])->inRandomOrder()->limit(4)->get()->toArray();
        }
    }
    
    private function _getTutor($educational = null) {
        if ($educational == null) 
            return Teacher::with('someDetail')->inRandomOrder()->limit(5)->get()->toArray();
        else {
            $id_educational = Educational::where(Educational::STAGE, $educational)->first()->id;
            return Teacher::with('someDetail')->withCount('courses')->whereHas('courses', function ($query) use ($id_educational) {
                $query->where(Course::EDUCATIONAL_ID, $id_educational);
            })->inRandomOrder()->limit(5)->get()->toArray();
        }
        
    }

    function getResult(Course $course) {
        $course_temp = $course;
        $user_ids = [];
        $result = Course::with('quizzes.results.student.someDetail')->find($course_temp->id)->toArray();
        if (count($result['quizzes']) > 0) {
            foreach ($result['quizzes'] as $key => $value) {
                if (count($value['results']) > 0) {
                    foreach ($value['results'] as $keyResult => $valResult) {
                        if (in_array($valResult[QuizResult::STUDENT_ID], $user_ids)) {
                            $key = array_search($valResult[QuizResult::STUDENT_ID], $user_ids);
                            $result['results'][$key]['count_quiz'] += 1;
                            $result['results'][$key][QuizResult::SCORE] += $valResult[QuizResult::SCORE];
                        } else {
                            array_push($user_ids, $valResult[QuizResult::STUDENT_ID]);
                            $data[QuizResult::STUDENT_ID] = $valResult[QuizResult::STUDENT_ID];
                            $data['count_quiz'] = 1;
                            $data[QuizResult::SCORE] = $valResult[QuizResult::SCORE];
                            $data['data'] = $valResult['student'];
                            $result['results'][] = $data;
                        };
                    };
                } else {
                    $result['results'] = [];
                    break;
                }
            };
            
            unset($result['quizzes']);
            usort($result['results'], function($a, $b) {
                return $b['score'] <=> $a['score'];
            });
        } else {
            $result['results'] = [];
        }

        $course = Course::with('topic.category','educational','teacher.someDetail')->find($course_temp->id)->toArray();
        if (auth()->guard('student')->check()) {
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_mine'] = $this->user->isOwner($course[Course::ID]);
        }
        // dd($result);
        return view('course.result', compact('result', 'course'));
    }

}
