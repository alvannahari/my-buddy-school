<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Question;
use App\Models\QuizOption;
use Illuminate\Support\Facades\Storage;

class QuestionController extends Controller {

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($quiz, Request $request) {
        $validator = Validator::make($request->all(), [
            Question::QUESTION      => 'required|string',
            "answer"                => "required|numeric",
            "option.*"              => "required|string|distinct",
            Question::IMAGE         => 'image|mimes:jpg,jpeg,png|max:8224',
        ]);

        if ($validator->fails()) {
            return response(['error' => $validator->errors()]);
        };

        // $credentials = $request->option;

        $credentials = $request->only(Question::QUESTION);
        $credentials[Question::QUIZ_ID] = $quiz;
        
        if ($request->has(Question::IMAGE)) {
            $image = Storage::disk('public')->put(Question::IMAGE_PATH, $request->file(Question::IMAGE));
            $credentials[Question::IMAGE] = basename($image);
        };
        $questions = Question::create($credentials);

        foreach ($request->option as $key => $value) {
            $credential[QuizOption::QUESTION_ID] = $questions->id;
            $credential[QuizOption::OPTION] = $value;
            if ($key == $request->answer) $credential[QuizOption::IS_ANSWER] = '1';
            else $credential[QuizOption::IS_ANSWER] = '0';
            QuizOption::create($credential);
        }

        return response(['error' => false]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question) {
        if ($question->delete()) 
            return response(['error' => false]);
        
        return response(['error' => 'Pertanyaan Gagal Terhapus']);
    }
}
