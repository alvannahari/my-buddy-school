<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Models\Teacher;

class TeacherController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = Teacher::with('detail:teacher_id,fullname,photo')->withCount('courses')->get()->toArray();

        return view('tutor.index', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher) {
        $tutor = $teacher->load('detail','courses.topic','courses.educational')->loadCount('courses')->toArray();

        $user = auth()->guard(session()->get('guard'))->user();

        foreach ($tutor['courses'] as $key => $value) {
            if ($user->hasTaken($value[Course::ID])) {
                $tutor['courses'][$key]['has_taken'] = true;
            } else {
                $tutor['courses'][$key]['has_taken'] = false;
            }
        }
        // dd($tutor);

        return view('tutor.profile', compact('tutor'));
    }

}
