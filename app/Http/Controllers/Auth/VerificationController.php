<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Mail;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;

class VerificationController extends Controller {

    // use VerifiesEmails;

    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
        // $this->middleware('auth');
        // $this->middleware('signed')->only('verify');
        // $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verifyEmail() {
        $token = request()->segment(2);
        $user_id = request()->segment(3);
        $role = request()->segment(4);
        $password = request()->segment(5);

        if ($role == 'student' && strlen($password) == 60 && strlen($token) == 25) {
            $user = Student::find($user_id);
            $this->_verify($user, $token);
        } else if ($role == 'teacher' && strlen($password) == 60 && strlen($token) == 25) {
            $user = Teacher::find($user_id);
            $this->_verify($user, $token);
        }

        return response([
            'message' => 'Email User Gagal Terverifikasi!'
        ]);
    }

    public function resendEmail(Request $request) {
        $user = Student::where('email',$request->email)->first();
        if (empty($user)) {
            $user = Teacher::where('email',$request->email)->first();
            $user['type'] = 'Guru';
        } else {
            $user['type'] = 'Siswa';
        }
        return response(['data' => $user]);
        // return response([$user->sendEmailVerificationNotification()]);
    }

    private function _verify($user, $token) {
        if ($user->hasVerifiedEmail()) {
            echo json_encode(['message' => 'Email User Sudah Terverifikasi Sebelumnya!']);
            die();
        }

        if ($user->remember_token == null) {
            echo json_encode(['message' => 'Verifikasi Terjadi Kesalahan Silahkan Meminta Kirim Ulang Verifikasi!']);
            die();
        }

        if ($user->remember_token == $token) {
            $user->markEmailAsVerified();
            $user->update([Student::REMEMBER_TOKEN => null]);
            return redirect()->route('home');
            // echo json_encode(['message' => 'Email User Berhasil Terverifikasi!. Silahkan Login']);
            // die();
        }
    }
}
