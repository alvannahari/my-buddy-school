<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\Student;
use App\Models\Teacher;

class LoginController extends Controller {

    use AuthenticatesUsers;

    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/';

    // public function __construct() {
        // $this->middleware('guest')->except('logout');
    // }

    public function showLoginForm() {
        return view('auth.login');
    }

    public function login(Request $request) {
        $this->validator($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // $this->incrementLoginAttempts($request);
        return redirect()->back()->with('message', 'Email Atau Password Salah !!!!');
    }

    protected function validator(Request $request) {
        $validator = Validator::make($request->all(), [
            Student::EMAIL      => ['required','email','string'],
            Student::PASSWORD   => ['required','string'],
        ]);

        if ($validator->fails()) {
            echo json_encode(['error' => $validator->errors()]);
            die();
        };
    }

    public function attemptLogin(Request $request) {
        foreach(array_keys(config('auth.guards')) as $guard){
            if ($guard != 'api' && $this->guard($guard)->attempt($this->credentials($request))) {
                session()->put('guard', $guard);
                return true;
                break;
            };
        }
        return false;
        // return $this->guard($request->type)->attempt($this->credentials($request));
    }

    protected function sendLoginResponse(Request $request) {
        $request->session()->regenerate();
        
        if (auth()->guard('student')->check() || auth()->guard('teacher')->check()) {
            $user = auth()->guard(session()->get('guard'))->user();

            $state = $user->checkDetail();
            session()->put('check_profile', $state);
            $count = $user->unreadInbox();
            session()->put('inbox_unread', $count);
            $user->login();
        }

        $this->clearLoginAttempts($request);

        return redirect()->intended($this->redirectPath());
    }

    public function credentials(Request $request) {
        return $request->only($this->username(), Student::PASSWORD);
    }

    public function guard($guard) {
        return Auth::guard($guard);
    }

    public function logout(Request $request) {
        if (auth()->guard('student')->check() || auth()->guard('teacher')->check()) {
            $user = auth()->guard(session()->get('guard'))->user();
            $user->logout();
            session()->forget('check_profile');
            session()->forget('inbox_unread');
        }
        $this->guard(session()->get('guard'))->logout();
        session()->forget('guard');
        // $this->guard(Auth::getDefaultDriver())->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('home');
    }
}
