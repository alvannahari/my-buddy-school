<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\ActivationEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\StudentDetail;
use App\Models\Teacher;
use App\Models\TeacherDetail;

class RegisterController extends Controller {

    use RegistersUsers;

    public function __construct() {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request) {
        $this->validator($request);

        // dd($request->toArray());

        if($this->create($request)) {
            // return redirect()->route('login')->with('message', 'Registrasi '.$request->type.' Berhasil. Silahkan Verifikasi Email Terlebih Dahulu !');
            return view('emails.confirm');
        };

        return redirect()->back()->with('message', 'Registrasi User Baru Gagal !!');
    }

    protected function validator(Request $request) {
        $validator = Validator::make($request->all(), [
            StudentDetail::FULLNAME        => 'required|string',
            Student::EMAIL                 => 'required|string|email|max:255|unique:students|unique:teachers',
            Student::PASSWORD              => 'required|string|min:8|confirmed'
        ]);

        if ($validator->fails()) {
            echo json_encode(['error' => $validator->errors()]);
            die();
        };
    }

    protected function create(Request $request) {
        $c_password = $request->input(Student::PASSWORD);
        $type = $request->input('type');

        $credentials = $request->only(Student::EMAIL, Student::PASSWORD);
        $credentials[Student::PASSWORD] = bcrypt($credentials[Student::PASSWORD]);
        $credentials[Student::REMEMBER_TOKEN] = $this->generateRandomString();

        if ($type == 'student') {
            DB::beginTransaction();
            try {
                $user = Student::create($credentials);
                StudentDetail::create([
                    StudentDetail::FULLNAME     => $request->input(StudentDetail::FULLNAME),
                    StudentDetail::STUDENT_ID   => $user->id
                ]);

                $credentials[StudentDetail::FULLNAME] = $request->input(StudentDetail::FULLNAME);
                $credentials[Student::PASSWORD] = $c_password;
                $credentials['type'] = ucfirst($type);
                $credentials['link'] = 'http://incar.krakatio.com/verify/'.$credentials[Student::REMEMBER_TOKEN].'/'.$user->id.'/student/'.$this->generateRandomString(60);
                Mail::to($credentials[Student::EMAIL])->send(new ActivationEmail($credentials));

                DB::commit();
                return true;
            } catch (\Exception $e) {
                DB::rollback();
                echo $e->getMessage();
                die();
            }
        } else if ($type == 'teacher') {
            DB::beginTransaction();
            try {
                $user = Teacher::create($credentials);
                TeacherDetail::create([
                    TeacherDetail::FULLNAME     => $request->input(TeacherDetail::FULLNAME),
                    TeacherDetail::TEACHER_ID   => $user->id
                ]);
                
                $credentials[TeacherDetail::FULLNAME] = $request->input(TeacherDetail::FULLNAME);
                $credentials[Teacher::PASSWORD] = $c_password;
                $credentials['type'] = ucfirst($type);
                $credentials['link'] = 'http://incar.krakatio.com/verify/'.$credentials[Teacher::REMEMBER_TOKEN].'/'.$user->id.'/teacher/'.$this->generateRandomString(60);
                Mail::to($credentials[Teacher::EMAIL])->send(new ActivationEmail($credentials));

                DB::commit();
                return true;
            } catch (\Exception $e) {
                DB::rollback();
                return false;
            }
        }
    }

    function generateRandomString($length = 25) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }
}
