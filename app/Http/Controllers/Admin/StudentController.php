<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            $data = Student::with('detail')->get();
    
            return response(['data' => $data]);
        }

        return view('admin.student.index');
        
    }

    function show(Student $student) {
        $student->load('detail','courses.teacher.someDetail','courses.topic','courses.educational');

        // dd($student->toArray());
        return view('admin.student.detail', compact('student'));
    }

    function state(Student $student) {
        $state = $student->email_verified_at;
        if ($state == null )
            $student->update([Student::VERIFIED_AT => now()]);
        else 
            $student->update([Student::VERIFIED_AT => null]);

        return response(['error' => false]);
    }

    function update(Student $student, Request $request) {
        $validator = Validator::make($request->all(), [
            StudentDetail::FULLNAME => 'required|string',
            StudentDetail::GENDER => 'required',
            StudentDetail::PLACE_BIRTH => 'required|string',
            StudentDetail::DATE_BIRTH => 'required',
            StudentDetail::GRADE => 'required',
            StudentDetail::ADDRESS => 'required',
            StudentDetail::COUNTRY => 'required',
            StudentDetail::PROVINCE => 'required',
            StudentDetail::CITY => 'required',
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(StudentDetail::FULLNAME, StudentDetail::GENDER, StudentDetail::PLACE_BIRTH, StudentDetail::DATE_BIRTH, StudentDetail::GRADE, StudentDetail::ADDRESS, StudentDetail::COUNTRY, StudentDetail::PROVINCE, StudentDetail::CITY);

        if ($student->detail->update($credentials))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function updatePhoto(Request $request, Student $student) {
        $validator = Validator::make($request->all(), [
            StudentDetail::PHOTO => 'required|image|mimes:png,jpg,jpeg|max:8096'
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);
            
        $student = $student->detail;

        $arrPhoto = explode("/",$student->photo);
        Storage::disk('public')->delete(StudentDetail::PHOTO_PATH, end($arrPhoto));

        $photo = Storage::disk('public')->put(StudentDetail::PHOTO_PATH, $request->file(StudentDetail::PHOTO));

        if ($student->update([StudentDetail::PHOTO => basename($photo)]))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);

    }

    function destroy(Student $student) {
        $student->delete();
        return redirect()->back();
    }

    function deletePhoto(Student $student) {
        $student->detail->removePhoto();

        return response(['error' => false]);
    }
    
}
