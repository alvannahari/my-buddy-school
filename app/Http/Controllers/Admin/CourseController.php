<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Educational;
use App\Models\Topic;
use COM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            $data = Course::with('topic', 'educational', 'teacher.someDetail')->get();
            return response(['data' => $data]);
        }

        return view('admin.course.index');
    }
    
    function show(Course $course) {
        $course->load('topic','educational','teacher.someDetail')->loadCount('students','theories','discussions','quizzes');
        $topics = Topic::get();
        $educationals = Educational::get();
        return view('admin.course.detail', compact('course','topics','educationals'));
    }

    function update(Request $request, Course $course) {
        $validator = Validator::make($request->all(), [
            Course::TITLE   => 'required|string',
            Course::DESCRIPTION   => 'required|string',
            Course::COVER   => 'image|mimes:png,jpg,jpeg|max:8096',
            Course::ZOOM   => 'string',
            Course::SCHEDULE   => 'string',
            Course::TOPIC_ID   => 'required|numeric',
            Course::EDUCATIONAL_ID   => 'required|numeric',
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        $credentials = $request->except(Course::COVER);

        if ($request->has(Course::COVER)) {
            $arr_cover = explode("/",$course->cover);
            Storage::disk('public')->delete(Course::COVER_PATH.end($arr_cover));
                
            $cover = Storage::disk('public')->put(Course::COVER_PATH, $request->file(Course::COVER));
            $credentials[Course::COVER] = basename($cover);
        }

        if ($course->update($credentials))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function state(Course $course) {
        $state = $course->is_active;
        if ($state == '1')
            $course->update([Course::IS_ACTIVE => '0']);
        else 
            $course->update([Course::IS_ACTIVE => '1']);

        return response(['error' => false]);
    }

    function destroy(Course $course) {
        $course->delete();

        return redirect()->back();
    }
}
