<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CompanyProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller {
    
    function socmed() {
        $setting = CompanyProfile::first();
        // dd($setting->toArray());
        return view('admin.setting.socmed', compact('setting'));
    }

    function updateSocmed(Request $request) {
        $validator = Validator::make($request->all(), [
            CompanyProfile::YOUTUBE => 'required|string',
            CompanyProfile::FACEBOOK => 'required|string',
            CompanyProfile::TWITTER => 'required|string',
            CompanyProfile::INSTAGRAM => 'required|string',
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        $credentials = $request->all();
        
        CompanyProfile::first()->update($credentials);

        return response(['error' => false]);
    }
}
