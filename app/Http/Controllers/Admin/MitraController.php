<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Educational;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MitraController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            $data = Partner::with('educational')->get();

            return response(['data' => $data]);
        }

        return view('admin.mitra.index');
    }

    function create() {
        $educationals = Educational::get();
        return view('admin.mitra.create', compact('educationals'));
    }

    function store(Request $request) {
        $validator = Validator::make($request->all(), [
            Partner::NAME   => 'required|string',
            Partner::EMAIL   => 'required|email',
            Partner::IMAGE   => 'required|image|mimes:png,jpg,jpeg|max:8096',
            Partner::EDUCATIONAL_ID   => 'required|numeric',
            Partner::ADDRESS   => 'required|string',
            Partner::CITY   => 'required|string',
            Partner::PHONE   => 'required|string',
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        $credentials = $request->except(Partner::IMAGE);
        $image = Storage::disk('public')->put(Partner::IMAGE_PATH, $request->file(Partner::IMAGE));
        $credentials[Partner::IMAGE] = basename($image);
        
        if (Partner::create($credentials))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function show (Partner $mitra) {
        $mitra->load('educational');
        $educationals = Educational::get();

        return view('admin.mitra.detail', compact('mitra', 'educationals'));
    }

    function update (Request $request, Partner $mitra) {
        $validator = Validator::make($request->all(), [
            Partner::NAME   => 'required|string',
            Partner::IMAGE   => 'image|mimes:png,jpg,jpeg|max:8096',
            Partner::EDUCATIONAL_ID   => 'required|numeric',
            Partner::EMAIL   => 'required|email',
            Partner::ADDRESS   => 'required|string',
            Partner::CITY   => 'required|string',
            Partner::PHONE   => 'required|string',
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);

        $credentials = $request->except(Partner::IMAGE);

        if ($request->has(Partner::IMAGE)) {
            $arr_image = explode("/",$mitra->image);
            Storage::disk('public')->delete(Partner::IMAGE_PATH.end($arr_image));
                
            $image = Storage::disk('public')->put(Partner::IMAGE_PATH, $request->file(Partner::IMAGE));
            $credentials[Partner::IMAGE] = basename($image);
        }

        if ($mitra->update($credentials))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function destroy(Partner $mitra) {
        $mitra->delete();

        return redirect()->route('admin-mitra');
    }
}
