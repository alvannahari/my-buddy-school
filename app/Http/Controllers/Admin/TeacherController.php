<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher;
use App\Models\TeacherDetail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class TeacherController extends Controller {
    
    function index() {
        if (request()->ajax()) {
            $data = Teacher::with('detail')->get();

            return response(['data' => $data]);
        }

        return view('admin.teacher.index');
        
    }

    function show(Teacher $teacher) {
        $teacher->load(['detail','courses' => function($q) {
            $q->with('topic', 'educational')->withCount('students');
        }]);

        // dd($teacher->toArray());
        return view('admin.teacher.detail', compact('teacher'));
    }

    function state(Teacher $teacher) {
        $state = $teacher->email_verified_at;
        if ($state == null )
            $teacher->update([teacher::VERIFIED_AT => now()]);
        else 
            $teacher->update([teacher::VERIFIED_AT => null]);

        return response(['error' => false]);
    }

    function update(Teacher $teacher, Request $request) {
        $validator = Validator::make($request->all(), [
            TeacherDetail::FULLNAME => 'required|string',
            TeacherDetail::GENDER => 'required',
            TeacherDetail::PLACE_BIRTH => 'required|string',
            TeacherDetail::DATE_BIRTH => 'required',
            TeacherDetail::DEGREE => 'required',
            TeacherDetail::ADDRESS => 'required',
            TeacherDetail::COUNTRY => 'required',
            TeacherDetail::PROVINCE => 'required',
            TeacherDetail::CITY => 'required',
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(TeacherDetail::FULLNAME, TeacherDetail::GENDER, TeacherDetail::PLACE_BIRTH, TeacherDetail::DATE_BIRTH, TeacherDetail::DEGREE, TeacherDetail::ADDRESS, TeacherDetail::COUNTRY, TeacherDetail::PROVINCE, TeacherDetail::CITY);

        if ($teacher->detail->update($credentials))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);
    }

    function updatePhoto(Request $request, Teacher $teacher) {
        $validator = Validator::make($request->all(), [
            TeacherDetail::PHOTO => 'required|image|mimes:png,jpg,jpeg|max:8096'
        ]);

        if ($validator->fails())
            return response(['error' => $validator->errors()]);
            
        $teacher = $teacher->detail;

        $arrPhoto = explode("/",$teacher->photo);
        Storage::disk('public')->delete(TeacherDetail::PHOTO_PATH, end($arrPhoto));

        $photo = Storage::disk('public')->put(TeacherDetail::PHOTO_PATH, $request->file(TeacherDetail::PHOTO));

        if ($teacher->update([TeacherDetail::PHOTO => basename($photo)]))
            return response(['error' => false]);

        return response(['error' => 'Something Wrong']);

    }

    function destroy(Teacher $teacher) {
        $teacher->delete();
        return redirect()->route('admin-student');
    }

    function deletePhoto(Teacher $teacher) {
        $teacher->detail->removePhoto();

        return response(['error' => false]);
    }
    
}
