<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\StudentDetail;
use App\Models\TeacherDetail;
use Mapper;
use App\Models\Course;
use App\Models\HistoryLogin;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller {
    
    function index() {
        // Get Data for diagram line user week
        $result_activity = HistoryLogin::whereBetween(HistoryLogin::CREATED_AT, [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()])->groupBy('date')->select(DB::raw('DATE_FORMAT(created_at, "%d") as date'), DB::raw('count(*) as total'))->get()->toArray();
        $activity = $this->_graphActivity($result_activity);

        // Get Data Pie Chart Eduactional Course
        $result_course = Course::groupBy(Course::EDUCATIONAL_ID)->orderBy(Course::EDUCATIONAL_ID)->select(DB::raw('count(id) as total'))->get()->toArray();
        $chart['educational_label'] = ['Primary School', 'Junior School', 'Senior School', 'Higher School', 'Others'];
        $chart['educational_value'] = array_column($result_course, 'total');
        
        // Get Data Statistic for user 
        $user['student_count'] = Student::count();
        $user['new_student'] = Student::whereBetween(Student::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $user['active_student'] = HistoryLogin::where(HistoryLogin::USER_TYPE, 'student')->whereBetween(HistoryLogin::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $user['student_percent'] = 100 - ($user['student_count'] - $user['new_student']) / $user['student_count'] * 100;

        $user['teacher_count'] = Teacher::count();
        $user['new_teacher'] = Teacher::whereBetween(Teacher::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $user['active_teacher'] = HistoryLogin::where(HistoryLogin::USER_TYPE, 'teacher')->whereBetween(HistoryLogin::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $user['teacher_percent'] = 100 - ($user['teacher_count'] - $user['new_teacher']) / $user['teacher_count'] * 100;

        // Maps with package from bradcornford github
        $maps_students = StudentDetail::whereNotNull([StudentDetail::LATI_LOC, StudentDetail::LONG_LOC, StudentDetail::CITY])->get([StudentDetail::LATI_LOC, StudentDetail::LONG_LOC, StudentDetail::CITY])->toArray();
        $maps_teachers = TeacherDetail::whereNotNull([StudentDetail::LATI_LOC, StudentDetail::LONG_LOC, StudentDetail::CITY])->get([TeacherDetail::LATI_LOC, TeacherDetail::LONG_LOC, TeacherDetail::CITY])->toArray();
        $maps_total = array_merge($maps_students, $maps_teachers);
        // return dd($maps_total);
        Mapper::map(-0.789275, 113.921327, ['center' => true, 'marker' => false, 'zoom' => 5]);
        foreach ($maps_total as $key => $value) {
            Mapper::marker($value[StudentDetail::LONG_LOC], $value[StudentDetail::LATI_LOC], ['title' => $value[StudentDetail::CITY]]);
        }

        // Statistic Visit but only user when login
        $visit['today'] = HistoryLogin::where(HistoryLogin::CREATED_AT, Carbon::today())->count();
        $visit['week'] = HistoryLogin::whereBetween(HistoryLogin::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
        $visit['year'] = HistoryLogin::WhereYear(HistoryLogin::CREATED_AT, Carbon::now())->count();
        $visit['total'] = HistoryLogin::count();
        $visit['online'] = HistoryLogin::where(HistoryLogin::CREATED_AT, Carbon::today())->where(HistoryLogin::IS_ONLINE, '1')->count();

        $recents = Course::with('teacher.someDetail','topic','educational')->orderBy(Course::CREATED_AT, 'desc')->limit(10)->get();
        
        return view('admin.dashboard', compact('activity','chart','user','visit','recents'));
    }
    
    private function _graphActivity(array $activity) {
        $data['value'] = [];

        $data['label'] = [
            'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu','Minggu'
        ];

        // foreach($nameDay as $index => $name) {
        for ($i=0; $i<7; $i++){
            $temp = false;
            $thisDate = Carbon::now()->startOfWeek()->addDay($i)->format('d');
            foreach($activity as $key => $value) {
                if($value['date'] == $thisDate){
                    $data['value'][$i] = $value['total'];
                    $temp = true;
                    break;
                };
            };
            if(!$temp) {
                $data['value'][$i] = 0;
            }
        };

        return $data;
    }
}
