<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Theory;
use Illuminate\Support\Facades\Auth;

class TheoryController extends Controller {

    protected $user;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard(session()->get('guard'))->user();
            return $next($request);
        });
    }

    function index(Course $course) {
        dd($course->theories);
    }

    function form(Course $course, $theory = null) {
        $course_id = $course->id;
        $state = 0;
        if ($theory != null) {
            $theory = Theory::find($theory)->toArray();
            $state = 1;
        }

        $course = Course::with('topic.category','educational','teacher.someDetail')->find($course_id)->toArray();
        if (auth()->guard('student')->check()) {
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_mine'] = $this->user->isOwner($course[Course::ID]);
        }
        return view('course.form', compact('course', 'theory', 'state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $course) {
        $validator = Validator::make($request->all(), [
            Theory::TITLE           => ['required', 'string', 'max:225'],
            Theory::DESCRIPTION     => ['required', 'string', 'max:225'],
            Theory::CONTENT         => ['required'],
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only([Theory::TITLE, Theory::DESCRIPTION, Theory::CONTENT]);
        $credentials[Theory::COURSE_ID] = $course;

        Theory::create($credentials);

        return response($credentials);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Theory $theory) {
        $course_id = $theory->course->id;
        $theory->load('course.educational','course.topic.category','course.theories:id,course_id');

        $course = Course::with('topic.category','educational','teacher.someDetail')->find($course_id)->toArray();
        if (auth()->guard('student')->check()) {
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_mine'] = $this->user->isOwner($course[Course::ID]);
        }
        
        $allTheory = Theory::where(Theory::COURSE_ID, $course_id)->get()->toArray();
        $data = [];
        foreach ($allTheory as $key => $value) {
            if ($value[Theory::ID] == $theory->id) {
                $data['previous'] = $allTheory[$key-1]['id'] ?? null;
                $data['next'] = $allTheory[$key+1]['id'] ?? null;
            }
        }
        // dd($data);

        return view('course.theory.show', compact('theory', 'course', 'allTheory', 'data'));
    }

    function update (Request $request,Theory $theory) {
        $validator = Validator::make($request->all(), [
            Theory::TITLE           => ['required', 'string', 'max:225'],
            Theory::DESCRIPTION     => ['required', 'string', 'max:225'],
            Theory::CONTENT         => ['required'],
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);
        
        $credentials = $request->only(Theory::TITLE, Theory::DESCRIPTION, Theory::CONTENT);

        if ($theory->update($credentials)) 
            return response(['error' => false]);

        return response(['error' => 'Terjadi Kesalahan !!!']);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Theory $theory) {
        if ($theory->delete()) 
            return response(['error' => false]);
            
        return response(['error' => 'Materi Gagal Dihapus!']);
    }
}
