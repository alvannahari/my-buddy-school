<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Course;
use App\Models\Discussion;
use App\Models\DiscussionComment;
use Illuminate\Support\Facades\Auth;

class DiscussionController extends Controller {

    protected $user;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard(session()->get('guard'))->user();
            return $next($request);
        });
    }

    function index(Course $course) {
        $course_temp = $course;
        $course = Course::with(['discussions.student.someDetail','topic.category','educational','teacher.someDetail', 'discussions' => function($query){
            $query->withCount('comments');
        }])->find($course_temp->id)->toArray();

        if (auth()->guard('student')->check()) {
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_mine'] = $this->user->isOwner($course[Course::ID]);
        }
        return view('course.discussion.index', compact('course'));
    }

    function store(Course $course, Request $request) {
        $validator = Validator::make($request->all(), [
            Discussion::TITLE           => 'required|string|max:255',
            Discussion::DISCUSSION      => 'required|string|max:255'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(Discussion::TITLE, Discussion::DISCUSSION);
        $credentials[Discussion::COURSE_ID] = $course->id;
        $credentials[Discussion::STUDENT_ID] = $request->user()->id;

        Discussion::create($credentials);

        return response(['error' => false]);
    }

    function show(Discussion $discussion) {
        $discussions = $discussion->load('student.someDetail','comments.userable.someDetail')->loadCount('comments')->toArray();
        $course = Course::with('topic.category','educational','teacher.someDetail')->find($discussion->course->id)->toArray();

        if (auth()->guard('student')->check()) {
            $course['has_taken'] = $this->user->hasTaken($course[Course::ID]);
        } else if (auth()->guard('teacher')->check()) {
            $course['is_mine'] = $this->user->isOwner($course[Course::ID]);
        }
        $user = $this->user->load('someDetail')->toArray();
        
        return view('course.discussion.detail', compact('discussions', 'user', 'course'));
    }

    function submitComment(Discussion $discussion, Request $request) {
        $validator = Validator::make($request->all(), [
            DiscussionComment::COMMENT  => 'required|string|max:255'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(DiscussionComment::COMMENT);
        $credentials[DiscussionComment::USER_ID] = $request->user()->id;
        $credentials[DiscussionComment::DISCUSSION_ID] = $discussion->id;
        if (auth()->guard('student')->check()) $credentials[DiscussionComment::USER_TYPE] = 'student';
        else $credentials[DiscussionComment::USER_TYPE] = 'teacher';

        DiscussionComment::create($credentials);

        return response(['error' => false]);
    }

    function destroy(Discussion $discussion) {
        if ($discussion->delete())
            return response(['message' => 'Diskusi ini berhasil di hapus']);
        
        return response(['error' => 'Diskusi ini gagal terhapus']);
    }
}
