<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseStudentTeacher;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Topic;

class HomeController extends Controller {

    protected $redirectToRoute = '/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        if (auth()->guard('admin')->check()) {
            return redirect()->route('admin-dashboard');
        }
        $count['students'] = Student::all()->count();
        $count['teachers'] = Teacher::all()->count();
        $count['courses'] = Course::all()->count();
        $tutors = Teacher::with('someDetail')->withCount('courses')->get()->toArray();
        // dd($tutors);
        return view('home', compact('count', 'tutors'));
    }
    
    function test() {
        // $data = Student::where(Student::EMAIL, 'alvannahari@gmail.com')->first();

        // $data->update([
        //     Student::PASSWORD   => bcrypt('cahganteng21')
        // ]);

        // return 'password berhasil diperbarui !!';

        return auth()->guard('student')->check();
    }

    public function dashboard() {
        $user = auth()->guard(session()->get('guard'))->user();
        $count_course = $user->courses->groupBy('educational_id')->toArray();
        $sum = 0;
        foreach ($count_course as $key => $value) {
            $temp = count($count_course[$key]);
            $count_course[$key] = $temp;
            $sum += $temp;
        }
        $count_course['total'] = $sum;

        $courses = $user->courses->load('teacher.someDetail','topic','educational')->take(4)->toArray();
        if (auth()->guard('student')->check()) {
            $discussions = $user->discussions->load('comments.userable.someDetail')->loadCount('comments')->take(3)->toArray();
            return view('dashboard', compact('count_course', 'courses', 'discussions'));
        } 
        return view('dashboard', compact('count_course', 'courses'));
    }

    function myCourse($topic = null) {
        $user = auth()->guard(session()->get('guard'))->user();
        if ($topic == null) {
            $courses = $user->courses->load('topic','educational','teacher.someDetail')->toArray();
        } else {
            if (auth()->guard('student')->check()) {
                $courses = Course::with('topic','educational','students','teacher.someDetail')
                    ->whereHas('students', function ($query) use ($user) {
                        $query->where(CourseStudentTeacher::STUDENT_ID, $user->id);
                    })->whereHas('topic', function ($query) use ($topic) {
                        $query->where(Topic::TOPIC, $topic);
                    })->get()->toArray();
            } else if (auth()->guard('teacher')->check()) {
                $courses = Course::with('topic','educational','teacher.someDetail')
                    ->where(Course::TEACHER_ID, $user->id)->whereHas('topic', function ($query) use ($topic) {
                        $query->where(Topic::TOPIC, $topic);
                    })->get()->toArray();
            }
        }

        // dd($courses);
        return view('user.mycourse', compact('courses'));
    }

    function contactUs() {
        return view('contactus');
    }

    function aboutUs() {
        return view('aboutus');
    }

}
