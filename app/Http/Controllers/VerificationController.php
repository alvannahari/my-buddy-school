<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class VerificationController extends Controller
{
    public function verify($user_id, Request $request) {
        if (! $request->hasValidSignature()) {
            return $this->respondUnAuthorizedRequest(202);
        }

        $user = User::findOrFail($user_id);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }

        return redirect()->to('/');
    }

    /**
     * Resend email verification link
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    // public function resend() {
    //     if (auth()->guard(session()->get('guard'))->user()->hasVerifiedEmail()) {
    //         return $this->respondBadRequest(402);
    //     }

    //     auth()->guard(session()->get('guard'))->user()->sendEmailVerificationNotification();

    //     return $this->respondWithMessage("Email verification link sent on your email id");
    // }
}
