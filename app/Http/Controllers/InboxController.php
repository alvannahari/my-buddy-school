<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Inbox;
use App\Models\Teacher;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class InboxController extends Controller {

    protected $user;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard(session()->get('guard'))->user();
            return $next($request);
        });
    }

    function index() {
        $inboxes = $this->getInboxes($this->user);
        $tutors = $this->getListTutors();
            // foreach ($messages['inboxes'] as $key => $value) {
            //     $date = new Carbon($value[Inbox::CREATED_AT]);
            //     $data = [
            //         Inbox::MESSAGE      => $value[Inbox::MESSAGE],
            //         Inbox::SENDER_S     => $value[Inbox::SENDER_S],
            //         Inbox::CREATED_AT   => $date->toDayDateTimeString()
            //     ];
            //     $messages['messages'][$value['teacher_id']][] = $data;
            // }
        return view('inbox.index', compact('inboxes','tutors'));
    }

    public function store(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'user_id'           => 'required|numeric',
            Inbox::MESSAGE      => 'required|string|max:255'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);
        
        $credentials = $request->only(Inbox::MESSAGE);
        if (auth()->guard('student')->check()) {
            $credentials[Inbox::STUDENT_ID] = $this->user->id;
            $credentials[Inbox::TEACHER_ID] = $request->input('user_id');
            $credentials[Inbox::READ_S] = '1';
        } else if (auth()->guard('teacher')->check()) {
            $credentials[Inbox::STUDENT_ID] = $request->input('user_id');
            $credentials[Inbox::TEACHER_ID] = $this->user->id;
            $credentials[Inbox::SENDER_S] = '0';
            $credentials[Inbox::READ_T] = '1';
        }

        Inbox::create($credentials);

        return response(['error' => false]);
    }
    
    public function show($user_id) {
        $inboxes = $this->getInboxes($this->user);
        $tutors = $this->getListTutors();
        if (auth()->guard('super')->check() || auth()->guard('admin')->check()) {
            $messages = Inbox::with('student', 'teacher')->get()->toArray();
        } else if (auth()->guard('student')->check()) {
            $messages_temp = Inbox::where(Inbox::STUDENT_ID, $this->user->id)->where(Inbox::TEACHER_ID, $user_id)->get()->toArray();
            $sender_user = Teacher::with('someDetail')->find($user_id)->toArray();
            Inbox::where(Inbox::STUDENT_ID, $this->user->id)->where(Inbox::TEACHER_ID, $user_id)->update([Inbox::READ_S => '1']);
        } else if (auth()->guard('teacher')->check()) {
            $messages_temp = Inbox::with('student', 'teacher')->where(Inbox::TEACHER_ID, $this->user->id)->get()->toArray();
            $sender_user = Student::with('someDetail')->find($user_id)->toArray();
            Inbox::where(Inbox::TEACHER_ID, $this->user->id)->where(Inbox::STUDENT_ID, $user_id)->update([Inbox::READ_T =>'1']);
        }

        foreach ($messages_temp as $key => $value) {
            $date = new Carbon($value[Inbox::CREATED_AT]);
            $date_group = substr($date->toDayDateTimeString(), 0, 17);
            $messages_temp[$key][Inbox::CREATED_AT] = $date->toDayDateTimeString();
            $messages[$date_group][] = $messages_temp[$key];
        }

        $count = $this->user->unreadInbox();
        session()->put('inbox_unread', $count);

        // dd($messages);
        return view('inbox.show', compact('messages','sender_user', 'inboxes', 'tutors'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inbox $inbox) {
        if ($inbox->delete()) 
            return response('Pesan Berhasil Dihapus!');
            
        return response('Pesan Gagal terhapus!');
    }

    function getInboxes($user) {
        if (auth()->guard('student')->check()) {
            $data = $user->inboxes->sortByDesc(Inbox::CREATED_AT)->unique('teacher_id')->load('teacher.someDetail')->toArray();
        } else if (auth()->guard('teacher')->check()) {
            $data = $user->inboxes->sortByDesc(Inbox::CREATED_AT)->unique('student_id')->load('student.someDetail')->toArray();
        }
        return $data;
    }

    function getListTutors() {
        if (auth()->guard('student')->check()) {
            $data = Teacher::with('someDetail')->get()->toArray();
        } else {
            $data = [];
        }
        return $data;
    }

    // function getCountUnread($user_id = null) {
    //     if ($user_id == null) {
    //         if (auth()->guard('student')->check()) {
    //             $count = Inbox::where(Inbox::STUDENT_ID, $this->user->id)->where(Inbox::READ_S, '0')->count();
    //         } else if (auth()->guard('teacher')->check()) {
    //             $count = Inbox::where(Inbox::TEACHER_ID, $this->user->id)->where(Inbox::READ_T, '0')->count();
    //         }
    //     } else {
    //         if (auth()->guard('student')->check()) {
    //             $count = Inbox::where(Inbox::STUDENT_ID, $this->user->id)->where(Inbox::TEACHER_ID, $user_id)->where(Inbox::READ_S, '0')->count();
    //         } else if (auth()->guard('teacher')->check()) {
    //             $count = Inbox::where(Inbox::TEACHER_ID, $this->user->id)->where(Inbox::STUDENT_ID, $user_id)->where(Inbox::READ_T, '0')->count();
    //         }
    //     }

    //     return $count;
    // }
}
