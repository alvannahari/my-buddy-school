<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use Illuminate\Http\Request;

class MitraController extends Controller {

    function index() {
        $mitras = Partner::get()->toArray();

        return view('mitra.index', compact('mitras'));
    }

    function show(Partner $mitra) {
        // dd($mitra->toArray());

        return view('mitra.profile', compact('mitra'));
    }
}
