<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\StudentDetail;
use App\Models\TeacherDetail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller {

    protected $user;

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard(session()->get('guard'))->user();
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = null) {
        if (auth()->guard('super')->check() || auth()->guard('admin')->check()) null;
        else $user = $this->user->load('detail')->loadCount('courses')->toArray();

        // dd($user);
        switch ($page) {
            case 'location':
                return view('profile.location', compact('user'));
                break;
            case 'socmed':
                return view('profile.socmed', compact('user'));
                break;
            case 'password':
                return view('profile.password', compact('user'));
                break;
            default:
                return view('profile.personal', compact('user'));
                break;
        }
    }

    function removePhoto() {
        $user = $this->user->detail;

        if (!$user->removePhoto()) 
            return response(['error' => 'Photo anda sudah tidak ada']);

        session()->put('check_profile', false);
        return response(['error' => false]);
    }

    function updatePhoto(Request $request) {
        $validator = Validator::make($request->all(), [
            StudentDetail::PHOTO            => 'required|image|mimes:png,jpg,jpeg|max:8192',
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $user = $this->user->detail;
        $user->removePhoto();

        if (auth()->guard('student')->check()) 
            $photo = Storage::disk('public')->put(StudentDetail::PHOTO_PATH, $request->file(StudentDetail::PHOTO));
        else 
            $photo = Storage::disk('public')->put(TeacherDetail::PHOTO_PATH, $request->file(TeacherDetail::PHOTO));

        if ($user->update([StudentDetail::PHOTO => basename($photo)])) {
            session()->put('check_profile', $this->user->checkDetail());
            return response(['error' => false]);
        }
        
        return response(['error' => 'Terjadi Kesalahan !!!']);
    }

    function updatePersonal(Request $request) {
        $validator = Validator::make($request->all(), [
            StudentDetail::FULLNAME         => 'required|string|max:255',
            StudentDetail::GENDER           => 'required|string',
            StudentDetail::PLACE_BIRTH      => 'required|string',
            'date'                          => 'required',
            'month'                         => 'required',
            'year'                          => 'required',
            StudentDetail::GRADE            => 'string|max:255',
            TeacherDetail::DEGREE           => 'string|max:255',
            StudentDetail::PHONE            => 'required|numeric'
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(StudentDetail::FULLNAME, StudentDetail::GENDER, StudentDetail::PLACE_BIRTH, StudentDetail::DATE_BIRTH, StudentDetail::PHONE);
        $credentials[StudentDetail::DATE_BIRTH] = $request->input('year').'-'.$request->input('month').'-'.$request->input('date');
        if (auth()->guard('student')->check()) {
            $credentials[StudentDetail::GRADE] = $request->input(StudentDetail::GRADE);
        } else {
            $credentials[TeacherDetail::DEGREE] = $request->input(TeacherDetail::DEGREE);
        }

        $user = $this->user->detail;

        if ($user->update($credentials)) {
            session()->put('check_profile', $this->user->checkDetail());
            return response(['error' => false]);
        }
        return response(['error'=>'Data Perubahan User Gagal Tersimpan!']);
    }

    function updateLocation(Request $request) {
        $validator = Validator::make($request->all(), [
            StudentDetail::ADDRESS      => 'required|string',
            StudentDetail::CITY         => 'required|string',
            StudentDetail::PROVINCE     => 'required|string',
            StudentDetail::COUNTRY      => 'required|string',
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(StudentDetail::ADDRESS,StudentDetail::CITY, StudentDetail::PROVINCE,StudentDetail::COUNTRY);
        $fullAddress = implode ("+", $credentials);
        $getLatLong = $this->get_lat_long($fullAddress);
        $credentials[StudentDetail::LATI_LOC] = $getLatLong[1];
        $credentials[StudentDetail::LONG_LOC] = $getLatLong[0];

        $user = $this->user->detail;

        if($user->update($credentials)) {
            session()->put('check_profile', $this->user->checkDetail());
            return response(['error' => false]);
        }

        return response(['error' => 'Terjadi Kesalahan!!!']);
    }

    function updateSocmed(Request $request) {
        $validator = Validator::make($request->all(), [
            StudentDetail::FACEBOOK     => 'required|string|regex:/^\S*$/u',
            StudentDetail::TWITTER      => 'required|string|regex:/^\S*$/u',
            StudentDetail::INSTAGRAM    => 'required|string|regex:/^\S*$/u',
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        $credentials = $request->only(StudentDetail::FACEBOOK, StudentDetail::TWITTER, StudentDetail::INSTAGRAM);

        $user = $this->user->detail;

        if($user->update($credentials)) {
            session()->put('check_profile', $this->user->checkDetail());
            return response(['error' => false]);
        }

        return response(['error' => 'Terjadi Kesalahan!!!']);
    }

    function updatePassword(Request $request) {
        $validator = Validator::make($request->all(), [
            Student::PASSWORD           => 'required',
            'new_password'              => 'required|min:6|confirmed',
            'new_password_confirmation' => 'required|min:6',
        ]);

        if ($validator->fails()) 
            return response(['error' => $validator->errors()]);

        if(!password_verify($request->input(Student::PASSWORD), $this->user->password))
            return response(['error' => 'Password lama tidak sesuai']);

        if($this->user->update([Student::PASSWORD => bcrypt($request->input('new_password'))])) 
            return response(['error' => false]);

        return response(['error' => 'Terjadi Kesalahan!!!']);
    }

    function get_lat_long($address){

        $apiKey = env('GOOGLE_API_KEY', 'AIzaSyAtqWsq5Ai3GYv6dSa6311tZiYKlbYT4mw'); // Google maps now requires an API key.

        $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&key='.$apiKey);

        $output= json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
        return [$latitude,$longitude];
    }
}
