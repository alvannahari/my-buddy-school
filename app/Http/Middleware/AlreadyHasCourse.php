<?php

namespace App\Http\Middleware;

use App\Models\Quiz;
use App\Models\Course;
use Closure;

class AlreadyHasCourse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = $request->user();
        $course = $request->route('course');
        $theory = $request->route('theory');
        $discussion = $request->route('discussion');
        $quiz = $request->route('quiz');
        $question = $request->route('question');
        // return dd($course);
        if (!empty($course) && $this->_haveAcces($user, $course instanceof Course ? $course : Course::find($course))) return $next($request);
        else if (!empty($theory) && $this->_haveAcces($user, $theory->course)) return $next($request);
        else if (!empty($discussion) && $this->_haveAcces($user, $discussion->course)) return $next($request);
        else if (!empty($quiz) && $this->_haveAcces($user, $quiz instanceof Quiz ? $quiz->course : Quiz::find($quiz)->course)) return $next($request);
        else if (!empty($question) && $this->_haveAcces($user, $question->quiz->course)) return $next($request);

        abort(404);
    }

    private function _haveAcces($user, $course) {
        if (auth()->guard('admin')->check() || auth()->guard('super')->check()) 
            return true;
        else if (auth()->guard('student')->check() && $user->hasTaken($course->id) && $course->isActive()) 
            return true;
        else if (auth()->guard('teacher')->check() && $user->isOwner($course->id)) 
            return true;

        return false;
    }
}
