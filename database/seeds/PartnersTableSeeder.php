<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Partner;

class PartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 15; $i++) {
            DB::table('partners')->insert([
                Partner::ID                 => 5018800+$i,
                Partner::EDUCATIONAL_ID     => rand(1,5),
                Partner::NAME               => 'Nama Perusahaan Mitra 0'.$i,
                Partner::EMAIL              => 'perusahaaan0'.$i.'@gmail.com',
                Partner::ADDRESS            => 'Alamat Lengkap Perusahaan Mitra 0'.$i,
                Partner::CITY               => 'Kota dan Negara Perusahaan Mitra 0'.$i,
                Partner::PHONE              => '+6285'.rand(536543671,893859389),
                Partner::CREATED_AT         => now(),
                Partner::UPDATED_AT         => now(),
            ]);
        }
    }
}
