<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Theory;

class TheoriesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i=0; $i < 50; $i++) {
            DB::table('theories')->insert([
                Theory::COURSE_ID                => rand(1000001,1000020),
                Theory::TITLE                    => 'Judul Materi 0'.$i,
                Theory::DESCRIPTION              => 'Deskripsi Overview Judul Materi 0'.$i,
                Theory::CONTENT                  => 'Konten Berupa Text dan video maupun gambar Judul Materi 0'.$i,
                Theory::CREATED_AT               => now(),
                Theory::UPDATED_AT               => now(),
            ]);
        }
    }
}
