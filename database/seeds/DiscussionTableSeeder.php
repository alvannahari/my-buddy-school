<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Discussion;
use App\Models\DiscussionComment;

class DiscussionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) {
            DB::table('discussions')->insert([
                Discussion::COURSE_ID           => rand(1000001,1000020),
                Discussion::STUDENT_ID          => rand(1000001,1000020),
                Discussion::TITLE               => 'Judul Diskusi 0'.$i,
                Discussion::DISCUSSION          => 'Isi Kontent Diskusi 0'.$i,
                Discussion::CREATED_AT          => now(),
                Discussion::UPDATED_AT          => now(),
            ]);
        }
        for ($i=0; $i < 40; $i++) {
            DB::table('discussion_comments')->insert([
                DiscussionComment::DISCUSSION_ID        => rand(10001,10001),
                DiscussionComment::USER_ID              => rand(1000001,1000005),
                DiscussionComment::USER_TYPE            => 'student',
                DiscussionComment::COMMENT              => 'Koementar ke '.$i.' dari siswa',
                DiscussionComment::CREATED_AT           => now(),
                DiscussionComment::UPDATED_AT           => now(),
            ]);
        }
    }
}
