<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Topic;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            [1, 'Natural Science'],
            [1, 'Social Science'],
            [1, 'Vernacular'],
            [3, 'Craft'],
            [3, 'Cultivation'],
            [3, 'Engineering'],
            [4, 'Sports'],
            [2, 'Communication'],
            [2, 'Social Skill'],
            [1, 'Mathematic'],
            [1, 'Indonesian Languange'],
            [1, 'English Language'],
            [1, 'Civilization'],
            [3, 'Cooking'],
            [2, 'Personal Skill'],
            [2, 'Leadership'],
            [2, 'Enterpreneurship'],
            [4, 'Art'],
            [4, 'Music'],
            [5, 'Islam'],
        ];
        for ($i=0; $i < count($category); $i++) {
            DB::table('topics')->insert([
                Topic::ID                       => $i+1,
                Topic::CATEGORY_ID              => $category[$i][0],
                Topic::TOPIC                    => $category[$i][1],
                Topic::CREATED_AT               => now(),
                Topic::UPDATED_AT               => now(),
            ]);
        }
    }
}
