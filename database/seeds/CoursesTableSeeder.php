<?php

use Illuminate\Database\Seeder;
use App\Models\Course;
use App\Models\CourseStudentTeacher;
use Illuminate\Support\Facades\DB;

class CoursesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i=1; $i < 25; $i++) { 
            DB::table('courses')->insert([
                Course::TEACHER_ID              => rand(1100001,1100003),
                Course::TOPIC_ID                => rand(1,10),
                Course::EDUCATIONAL_ID          => rand(1,5),
                Course::TITLE                   => 'Kursus Kategori '.$i,
                Course::DESCRIPTION             => 'Deskripsi Singkat untuk Kursus Kategori '.$i,
                Course::COVER                   => 'default.png',
                Course::CREATED_AT              => now(),
                Course::UPDATED_AT              => now(),
            ]);
        }
        for ($i=1; $i < 56; $i++) { 
            DB::table('course_student_teachers')->insert([
                CourseStudentTeacher::STUDENT_ID            => rand(1000001,1000004),
                CourseStudentTeacher::COURSE_ID             => rand(1000001,1000020),
                CourseStudentTeacher::CREATED_AT            => now(),
            ]);
        }
    }
}
