<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Quiz;
use App\Models\Question;
use App\Models\QuizOption;
use App\Models\QuizResult;

class QuizzesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i=1; $i<=20; $i++) {
            DB::table('quizzes')->insert([
                Quiz::COURSE_ID         => rand(1000001,1000020),
                Quiz::TITLE             => 'Judul Quiz 0'.$i,
                Quiz::CREATED_AT        => now(),
                Quiz::UPDATED_AT        => now(),
            ]);
        }
        for ($i=1; $i <= 80; $i++) {
            DB::table('questions')->insert([
                Question::QUIZ_ID           => rand(10001,10020),
                Question::QUESTION          => 'Pertanyaan Untuk Quiz '.$i,
                Question::CREATED_AT        => now(),
                Question::UPDATED_AT        => now(),
            ]);
        }
        $indexOption = 1;
        for ($i=1; $i <= 400; $i++) {
            DB::table('quiz_options')->insert([
                QuizOption::QUESTION_ID     => $i%5 == 0 ? $indexOption++ : $indexOption,
                QuizOption::OPTION          => 'Pilihan Pertanyaan '.$indexOption,
                QuizOption::IS_ANSWER       => $i%5 == 0 ? '1' : '0',
                QuizOption::CREATED_AT      => now(),
                QuizOption::UPDATED_AT      => now(),
            ]);
        }
        for ($i=1; $i <= 120; $i++) {
            DB::table('quiz_results')->insert([
                QuizResult::QUIZ_ID         => rand(10001,10020),
                QuizResult::STUDENT_ID      => rand(1000001,1000004),
                QuizResult::SCORE           => mt_rand(ceil(40/10) , floor(200/10))*10,
                QuizResult::CREATED_AT      => now()
            ]);
        }
    }
}
