<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use App\Models\Student;
use App\Models\StudentDetail;
use App\Models\Teacher;
use App\Models\TeacherDetail;
use App\Models\Admin;
use App\Models\AdminDetail;
use App\Models\Superuser;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create('id_ID');

        // Insert Data Student Table
        for ($i=1; $i<6; $i++) {
            DB::table('students')->insert([
                // Student::ID                     => $i,
                Student::EMAIL                  => 'student0'.$i.'@user.com',
                Student::PASSWORD               => bcrypt('student0'.$i),
                Student::VERIFIED_AT            => now(),
                Student::CREATED_AT             => now(),
                Student::UPDATED_AT             => now(),
            ]);

            DB::table('student_details')->insert([
                StudentDetail::ID               => $i,
                StudentDetail::STUDENT_ID       => 1000000+$i,
                StudentDetail::FULLNAME         => $faker->name,
                StudentDetail::GENDER           => $faker->randomElement(['Laki-laki','Perempuan']),
                StudentDetail::PLACE_BIRTH      => $faker->city,
                StudentDetail::DATE_BIRTH       => $faker->date(),
                StudentDetail::GRADE            => $faker->randomElement(['Primary School', 'Yunior High School', 'Senior High School', 'Higher School', 'Other']),
                StudentDetail::ADDRESS          => $faker->address,
                StudentDetail::COUNTRY          => $faker->country,
                StudentDetail::PROVINCE         => "East Java",
                StudentDetail::CITY             => $faker->city,
                StudentDetail::LONG_LOC         => $faker->longitude,
                StudentDetail::LATI_LOC         => $faker->latitude,
                StudentDetail::FACEBOOK         => "facebook.com/student0".$i,
                StudentDetail::TWITTER          => "twitter.com/student0".$i,
                StudentDetail::INSTAGRAM        => "instagram.com/student0".$i,
                StudentDetail::PHONE            => $faker->phoneNumber,
                StudentDetail::CREATED_AT       => now(),
                StudentDetail::UPDATED_AT       => now(),
            ]);
        }

        // Insert Data Teacher Table
        for ($i=1; $i<4; $i++) {
            DB::table('teachers')->insert([
                // Teacher::ID                     => $i,
                Teacher::EMAIL                  => 'teacher0'.$i.'@user.com',
                Teacher::PASSWORD               => bcrypt('teacher0'.$i),
                Teacher::VERIFIED_AT            => now(),
                Teacher::CREATED_AT             => now(),
                Teacher::UPDATED_AT             => now(),
            ]);

            DB::table('teacher_details')->insert([
                TeacherDetail::ID               => $i,
                TeacherDetail::TEACHER_ID       => 1100000+$i,
                TeacherDetail::FULLNAME         => $faker->name,
                TeacherDetail::GENDER           => $faker->randomElement(['Laki-laki','Perempuan']),
                TeacherDetail::PLACE_BIRTH      => $faker->city,
                TeacherDetail::DATE_BIRTH       => $faker->date(),
                TeacherDetail::DEGREE           => $faker->randomElement(['Sarjana Teknik', 'Sarjana Pendidikan', 'Magister', 'Freelancer', 'Other']),
                TeacherDetail::ADDRESS          => $faker->address,
                TeacherDetail::COUNTRY          => $faker->country,
                TeacherDetail::PROVINCE         => "East Java",
                TeacherDetail::CITY             => $faker->city,
                TeacherDetail::LONG_LOC         => $faker->longitude,
                TeacherDetail::LATI_LOC         => $faker->latitude,
                TeacherDetail::FACEBOOK         => "facebook.com/teacher0".$i,
                TeacherDetail::TWITTER          => "twitter.com/teacher0".$i,
                TeacherDetail::INSTAGRAM        => "instagram.com/teacher0".$i,
                TeacherDetail::PHONE            => $faker->phoneNumber,
                TeacherDetail::CREATED_AT       => now(),
                TeacherDetail::UPDATED_AT       => now(),
            ]);
        }

        // Insert Data Admin Table
        for ($i=1; $i<2; $i++) {
            DB::table('admins')->insert([
                // Admin::ID                     => $i,
                Admin::EMAIL                  => 'admin0'.$i.'@admin.com',
                Admin::PASSWORD               => bcrypt('admin0'.$i),
                Admin::CREATED_AT             => now(),
                Admin::UPDATED_AT             => now(),
            ]);

            DB::table('admin_details')->insert([
                AdminDetail::ID               => 1110000+$i,
                AdminDetail::ADMIN_ID         => $i,
                AdminDetail::FULLNAME         => $faker->name,
                AdminDetail::GENDER           => $faker->randomElement(['Laki-laki','Perempuan']),
                AdminDetail::PLACE_BIRTH      => $faker->city,
                AdminDetail::DATE_BIRTH       => $faker->date(),
                AdminDetail::ADDRESS          => $faker->address,
                AdminDetail::COUNTRY          => $faker->country,
                AdminDetail::PROVINCE         => "East Java",
                AdminDetail::CITY             => $faker->city,
                AdminDetail::PHONE            => $faker->phoneNumber,
                AdminDetail::CREATED_AT       => now(),
                AdminDetail::UPDATED_AT       => now(),
            ]);
        }

        DB::table('superusers')->insert([
            Admin::EMAIL                  => 'superuser@super.com',
            Admin::PASSWORD               => bcrypt('superuser'),
            Admin::CREATED_AT             => now(),
            Admin::UPDATED_AT             => now(),
        ]);
    }
}
