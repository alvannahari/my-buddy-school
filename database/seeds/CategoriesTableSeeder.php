<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = ['Lesson','Softskill', 'Pra-Vocational', 'Others', 'Religion'];
        for ($i=0; $i < count($data); $i++) {
            DB::table('categories')->insert([
                Category::ID                        => $i+1,
                Category::CATEGORY                  => $data[$i],
                Category::CREATED_AT                => now(),
                Category::UPDATED_AT                => now(),
            ]);
        }
    }
}
