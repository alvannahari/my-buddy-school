<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Educational;

class EducationalsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $data = ['primary_school', 'junior_high_school', 'senior_high_school', 'higher_school', 'others'];
        for ($i=0; $i < count($data); $i++) {
            DB::table('educationals')->insert([
                Educational::ID                 => $i+1,
                Educational::STAGE              => $data[$i],
                Educational::CREATED_AT         => now(),
                Educational::UPDATED_AT         => now(),
            ]);
        }
    }
}
