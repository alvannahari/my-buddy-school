<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Inbox;
use Illuminate\Support\Facades\DB;

class CreateInboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inboxes', function (Blueprint $table) {
            $table->bigIncrements(Inbox::ID);
            $table->integer(Inbox::STUDENT_ID);
            $table->integer(Inbox::TEACHER_ID);
            $table->enum(Inbox::SENDER_S, [0,1])->default(1);
            $table->string(Inbox::MESSAGE);
            $table->enum(Inbox::READ_S, [0,1])->default(0);
            $table->enum(Inbox::READ_T, [0,1])->default(0);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE inboxes AUTO_INCREMENT = 1000001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inboxes');
    }
}
