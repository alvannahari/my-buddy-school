<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\Discussion;
use App\Models\DiscussionComment;

class CreateDiscussionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discussions', function (Blueprint $table) {
            $table->bigIncrements(Discussion::ID);
            $table->integer(Discussion::COURSE_ID);
            $table->integer(Discussion::STUDENT_ID);
            $table->string(Discussion::TITLE);
            $table->string(Discussion::DISCUSSION);
            $table->timestamps();
        });
        
        DB::statement("ALTER TABLE discussions AUTO_INCREMENT = 10001;");

        Schema::create('discussion_comments', function (Blueprint $table) {
            $table->bigIncrements(DiscussionComment::ID);
            $table->integer(DiscussionComment::DISCUSSION_ID);
            $table->integer(DiscussionComment::USER_ID);
            $table->string(DiscussionComment::USER_TYPE);
            $table->string(DiscussionComment::COMMENT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discussions');
    }
}
