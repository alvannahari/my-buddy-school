<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Superuser;
use Illuminate\Support\Facades\DB;

class CreateSuperusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('superusers', function (Blueprint $table) {
            $table->bigIncrements(Superuser::ID);
            $table->string(Superuser::EMAIL)->unique();
            $table->string(Superuser::PASSWORD);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE superusers AUTO_INCREMENT = 999427799;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('superusers');
    }
}
