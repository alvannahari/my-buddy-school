<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\HistoryLogin;

class CreateHistoryLoginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_logins', function (Blueprint $table) {
            $table->bigIncrements(HistoryLogin::ID);
            $table->integer(HistoryLogin::USER_ID);
            $table->enum(HistoryLogin::USER_TYPE, ['student', 'teacher']);
            $table->enum(HistoryLogin::IS_ONLINE, [0, 1])->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_logins');
    }
}
