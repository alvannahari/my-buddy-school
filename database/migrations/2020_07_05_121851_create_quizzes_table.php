<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Quiz;
use App\Models\QuizOption;
use App\Models\Question;
use App\Models\QuizResult;
use Illuminate\Support\Facades\DB;

class CreateQuizzesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->bigIncrements(Quiz::ID);
            $table->integer(Quiz::COURSE_ID);
            $table->string(Quiz::TITLE);
            // $table->integer(Quiz::ANSWER);
            $table->string(Quiz::FILE)->nullable();
            $table->string(Quiz::IS_ACTIVE)->default(0);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE quizzes AUTO_INCREMENT = 10001;");

        Schema::create('questions', function (Blueprint $table) {
            $table->increments(Question::ID);
            $table->integer(Question::QUIZ_ID);
            $table->string(Question::QUESTION);
            $table->string(Question::IMAGE)->nullable();
            $table->timestamps();
        });

        Schema::create('quiz_options', function (Blueprint $table) {
            $table->increments(QuizOption::ID);
            $table->integer(QuizOption::QUESTION_ID);
            $table->string(QuizOption::OPTION);
            $table->enum(QuizOption::IS_ANSWER, [0,1])->default(0);
            $table->timestamps();
        });

        Schema::create('quiz_results', function (Blueprint $table) {
            $table->increments(QuizResult::ID);
            $table->integer(QuizResult::QUIZ_ID);
            $table->integer(QuizResult::STUDENT_ID);
            $table->integer(QuizResult::SCORE);
            $table->timestamps();
            // $table->timestamp(QuizResult::CREATED_AT)->useCurrent();
        });

        // DB::statement("ALTER TABLE quiz_results AUTO_INCREMENT = 101;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('quiz_options');
        Schema::dropIfExists('quiz_results');
    }
}
