<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Teacher;
use App\Models\TeacherDetail;
use Illuminate\Support\Facades\DB;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements(Teacher::ID);
            $table->string(Teacher::EMAIL)->unique();
            $table->string(Teacher::PASSWORD);
            $table->timestamp(Teacher::VERIFIED_AT)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE teachers AUTO_INCREMENT = 1100001;");

        Schema::create('teacher_details', function (Blueprint $table) {
            $table->increments(TeacherDetail::ID);
            $table->integer(TeacherDetail::TEACHER_ID)->unique();
            $table->string(TeacherDetail::FULLNAME);
            $table->enum(TeacherDetail::GENDER, ['Laki-laki', 'Perempuan'])->nullable();;
            $table->string(TeacherDetail::PLACE_BIRTH)->nullable();
            $table->date(TeacherDetail::DATE_BIRTH)->nullable();;
            $table->string(TeacherDetail::DEGREE)->nullable();;
            $table->string(TeacherDetail::PHOTO)->nullable();
            $table->string(TeacherDetail::ADDRESS)->nullable();;
            $table->string(TeacherDetail::COUNTRY)->nullable();;
            $table->string(TeacherDetail::PROVINCE)->nullable();;
            $table->string(TeacherDetail::CITY)->nullable();;
            $table->string(TeacherDetail::LONG_LOC)->nullable();;
            $table->string(TeacherDetail::LATI_LOC)->nullable();;
            $table->string(TeacherDetail::FACEBOOK)->nullable();
            $table->string(TeacherDetail::TWITTER)->nullable();
            $table->string(TeacherDetail::INSTAGRAM)->nullable();
            $table->string(TeacherDetail::PHONE)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
        Schema::dropIfExists('teacher_details');
    }
}
