<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Course;
use App\Models\CourseStudentTeacher;
use Illuminate\Support\Facades\DB;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements(Course::ID);
            $table->integer(Course::TEACHER_ID);
            $table->integer(Course::TOPIC_ID);
            $table->integer(Course::EDUCATIONAL_ID);
            $table->string(Course::TITLE);
            $table->string(Course::DESCRIPTION);
            $table->string(Course::COVER)->default('default.png');
            $table->string(Course::ZOOM)->nullable();
            $table->enum(Course::IS_ACTIVE, [0,1])->default(1);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE courses AUTO_INCREMENT = 1000001;");

        Schema::create('course_student_teachers', function (Blueprint $table) {
            $table->bigIncrements(CourseStudentTeacher::ID);
            $table->integer(CourseStudentTeacher::STUDENT_ID);
            $table->integer(CourseStudentTeacher::COURSE_ID);
            $table->timestamp(CourseStudentTeacher::CREATED_AT)->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
        Schema::dropIfExists('course_student_teachers');
    }
}
