<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Partner;
use Illuminate\Support\Facades\DB;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments(Partner::ID);
            $table->integer(Partner::EDUCATIONAL_ID);
            $table->string(Partner::NAME);
            $table->string(Partner::IMAGE)->nullable();
            $table->string(Partner::EMAIL);
            $table->string(Partner::ADDRESS);
            $table->string(Partner::CITY);
            $table->string(Partner::VIDEO)->nullable();
            $table->string(Partner::PHONE);
            $table->string(Partner::WEBSITE);
            $table->string(Partner::FACEBOOK);
            $table->string(Partner::TWITTER);
            $table->string(Partner::INSTAGRAM);
            $table->string(Partner::YOUTUBE);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE partners AUTO_INCREMENT = 5018801;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
