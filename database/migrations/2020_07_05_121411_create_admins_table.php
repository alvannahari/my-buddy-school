<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Admin;
use App\Models\AdminDetail;
use Illuminate\Support\Facades\DB;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements(Admin::ID);
            $table->string(Admin::EMAIL);
            $table->string(Admin::PASSWORD);
            $table->string(AdminDetail::FULLNAME);
            $table->string(AdminDetail::PHONE);
            $table->rememberToken();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE admins AUTO_INCREMENT = 1110001;");

        // Schema::create('admin_details', function (Blueprint $table) {
        //     $table->increments(AdminDetail::ID);
        //     $table->integer(AdminDetail::ADMIN_ID)->unique();
        //     $table->string(AdminDetail::FULLNAME);
        //     $table->enum(AdminDetail::GENDER, ['Laki-laki', 'Perempuan']);
        //     $table->string(AdminDetail::PLACE_BIRTH);
        //     $table->date(AdminDetail::DATE_BIRTH);
        //     $table->string(AdminDetail::ADDRESS);
        //     $table->string(AdminDetail::COUNTRY);
        //     $table->string(AdminDetail::PROVINCE);
        //     $table->string(AdminDetail::CITY);
        //     $table->string(AdminDetail::PHONE);
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
        Schema::dropIfExists('admin_details');
    }
}
