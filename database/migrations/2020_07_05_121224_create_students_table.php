<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Student;
use App\Models\StudentDetail;
use Illuminate\Support\Facades\DB;

class CreateStudentsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements(Student::ID);
            $table->string(Student::EMAIL)->unique();
            $table->string(Student::PASSWORD);
            $table->timestamp(Student::VERIFIED_AT)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE students AUTO_INCREMENT = 1000001;");
        
        Schema::create('student_details', function (Blueprint $table) {
            $table->increments(StudentDetail::ID);
            $table->integer(StudentDetail::STUDENT_ID)->unique();
            $table->string(StudentDetail::FULLNAME);
            $table->enum(StudentDetail::GENDER, ['Laki-laki', 'Perempuan'])->nullable();;
            $table->string(StudentDetail::PLACE_BIRTH)->nullable();;
            $table->date(StudentDetail::DATE_BIRTH)->nullable();;
            $table->string(StudentDetail::GRADE)->nullable();;
            $table->string(StudentDetail::PHOTO)->nullable();
            $table->string(StudentDetail::ADDRESS)->nullable();;
            $table->string(StudentDetail::COUNTRY)->nullable();;
            $table->string(StudentDetail::PROVINCE)->nullable();;
            $table->string(StudentDetail::CITY)->nullable();;
            $table->string(StudentDetail::LONG_LOC)->nullable();;
            $table->string(StudentDetail::LATI_LOC)->nullable();;
            $table->string(StudentDetail::FACEBOOK)->nullable();
            $table->string(StudentDetail::TWITTER)->nullable();
            $table->string(StudentDetail::INSTAGRAM)->nullable();
            $table->string(StudentDetail::PHONE)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
        Schema::dropIfExists('student_details');
    }
}
