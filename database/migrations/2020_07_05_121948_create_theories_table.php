<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Theory;
use Illuminate\Support\Facades\DB;

class CreateTheoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theories', function (Blueprint $table) {
            $table->bigIncrements(Theory::ID);
            $table->integer(Theory::COURSE_ID);
            $table->string(Theory::TITLE);
            $table->string(Theory::DESCRIPTION);
            $table->longText(Theory::CONTENT);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE theories AUTO_INCREMENT = 10001;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theories');
    }
}
